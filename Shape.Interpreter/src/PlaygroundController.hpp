#pragma once

#include <iostream>
#include <iomanip>
#include <sstream>

#include <JsonBox.h>

#include <Shape.hpp>

namespace Shape
{
	/// \brief Test Shape's data handler.
	class PlaygroundController : 
		public CompilerController
	{
	private:
		class VarFWDummyStorage :
			public IStorageHandler
		{
			Value * operator () (CompilerController *controller, const std::string &key)
			{
				Value *value = nullptr;
				controller->EmitStorageHandler("@", "__varfw_" + key, value);
				return value;
			}
		}; // class VarFWDummyStorage

		class ErrorHandler :
			public IErrorHandler
		{
			void operator () (CompilerController *controller, const Shape::location &location, const std::string &message)
			{
				fprintf(stderr, "[%d.%d-%d.%d] %s\n", location.begin.line, location.begin.column, location.end.line, location.end.column, message.c_str());
			}
		}; // class ErrorHandler

	private:
		std::string mod, dataFileName;

	public:
		PlaygroundController(const std::string &mod, std::string &dataFileName) :
			mod(mod), dataFileName(dataFileName)
		{ 
			this->SetStorageHandler<VarFWDummyStorage>("$");
			this->SetErrorHandler<ErrorHandler>();
		}

		/// \brief Destroys data handler object.
		virtual ~PlaygroundController()
		{ }
	
		void PrepareDummy()
		{ 
			Value *value = nullptr;

			if (this->EmitStorageHandler("@", "", value) == CompilerController::Ok)
			{
				value->clear();

				JsonBox::Value src;

				if (auto h = fopen(dataFileName.c_str(), "r"))
				{
					fclose(h);
					src.loadFromFile(dataFileName);
					Iterate(src, *value);
				}
		
				if (value->IsVoid())
					*value = Shape::Value(Shape::Value::TArray);

				(*value)["date"] = FormatTime("%d-%m-%y");
				(*value)["time"] = FormatTime("%H:%M:%S");		
			}

		}

		std::string FormatTime(const std::string &Format)
		{
			std::string rc;
			tm timeData = { 0 };
			time_t currentTime = time(nullptr);

			rc.resize(16);
		
			errno_t err = localtime_s(&timeData, &currentTime);

			if (err == 0)
			{
				for (int i = 0; i < 100; ++i)
				{
					size_t len = strftime((char *)rc.c_str(), rc.size(), Format.c_str(), &timeData);

					if (len == 0)
						rc.resize(rc.size() * 4);
					else
					{
						rc.resize(len);
						break;
					}
				}
			}

			return rc;
		}

		/// \brief ������������ ������������ ��� � ������.
		/// \param Src ��������.
		template <typename T>
		std::string ToString(T src)
		{
			std::stringstream ss;
			ss << src;
			return ss.str();
		}

		void Iterate(const JsonBox::Value &value, Shape::Value &dst)
		{
			if (value.getType() == JsonBox::Value::OBJECT)
			{
				JsonBox::Object object = value.getObject();
				dst = Shape::Value(Shape::Value::TArray);

				for (const auto &i : object)
					Iterate(i.second, dst[i.first]);
			}
			else if (value.getType() == JsonBox::Value::ARRAY)
			{
				JsonBox::Array array = value.getArray();
				dst = Shape::Value(Shape::Value::TArray);

				for (size_t i = 0u; i < array.size(); ++i)
				{					
					Iterate(array[i], dst[(int)i]);
				}
			}
			else if (value.getType() == JsonBox::Value::STRING) 
				dst = value.getString();
			else if (value.getType() == JsonBox::Value::INTEGER)
				dst = value.getInt();
			else if (value.getType() == JsonBox::Value::DOUBLE)
				dst = (float)value.getDouble();
			else if (value.getType() == JsonBox::Value::BOOLEAN)
				dst = value.getBoolean();
			else dst = Shape::Value();
		}
	}; // class PlaygroundDataHandler
} // namespace Shape
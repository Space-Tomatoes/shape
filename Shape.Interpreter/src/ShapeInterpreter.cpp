/// \file Startup.cpp Shape Interpreter entry point definition.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#include <cstdlib>
#include <cstdio>
#include <vector>
#include <conio.h>
#include <map>
#include <sstream>
#include <iomanip>

#include <getopt.h>

#include <Shape.hpp>
#include "PlaygroundController.hpp"

#define DEF_TRACE_SCAN			(false)
#define DEF_TRACE_PARSE			(false)
#define DEF_FORM_NAME			("default.frm")
#define DEF_DATA_NAME			("default.json")

static char szMod[] = "$MOD$ 210414 1001t ShapeInterpreter.exe [Build: " __TIMESTAMP__ "]";

void ParseOptions(int argc, char *argv[]);
void Usage();

typedef struct SInterpreterOptions
{
	bool traceScanning;
	bool traceParsing;
	std::string formFileName;
	std::string dataFileName;
} InterpreterOptions;

InterpreterOptions options = { DEF_TRACE_SCAN, DEF_TRACE_PARSE, DEF_FORM_NAME, DEF_DATA_NAME };

/// \brief Executable entry point.
/// \param [in] argc Arguments count.
/// \param [in] argv Arguments array.
int main(int argc, char *argv[])
{ 
	setlocale(LC_ALL, "");

	ParseOptions(argc, argv);
	
	Shape::ShapeCompiler compiler;
	Shape::PlaygroundController controller(szMod, options.dataFileName);

	compiler.SetController(&controller);

	compiler.SetTraceScanning(options.traceScanning);
	compiler.SetTraceParsing(options.traceScanning);

	controller.PrepareDummy();

	/*char *szFormPath = new char[options.formFileName.size() + 1];
	strcpy(szFormPath, options.formFileName.c_str());
	::PathRemoveFileSpec(szFormPath);
	::SetCurrentDirectory(szFormPath);
	delete [] szFormPath;*/

	try
	{		
		std::cout << compiler.ParseFile(options.formFileName);
	}
	catch (const Shape::InvalidOperationException &ex)
	{
		fprintf(stderr, "[%d.%d - %d.%d] %s", 
			ex.GetLocation().begin.line, ex.GetLocation().begin.column, ex.GetLocation().end.line, ex.GetLocation().end.column, 
			ex.GetMessage().c_str());
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what();
	}

	return EXIT_SUCCESS;
}

void ParseOptions(int argc, char *argv[])
{
	struct option params[] =
	{
		{ "help",			no_argument,		NULL, 'h' },
		{ "traceScanning",	no_argument,		NULL, 's' },
		{ "traceParsing",	no_argument,		NULL, 'p' },
		{ "form",			required_argument,	NULL, 'f' },
		{ "data",			required_argument,	NULL, 'd' },
		{ NULL, NULL, NULL, NULL }
	};
	
	char cmd = 0;
	int optionIndex = 0;
	extern char *optarg;

	while (cmd != -1)
	{
		switch (cmd = getopt_long(argc, argv, "hspf:d:", params, &optionIndex))
		{
		case 's': // --traceScanning
			options.traceScanning = true;
		break;
		case 'p': // --traceParsing
			options.traceParsing = true;
		break;
		case 'f': // --form
			options.formFileName = optarg;
		break;
		case 'd': // --data
			options.dataFileName = optarg;
		break;
		case 'h': // --help
			Usage();
			exit(EXIT_FAILURE);
		break;
		}
	}
}

void Usage()
{
	printf
	(
		"Usage: ShapeInterpreter[.exe] [-h] [-s] [-p] [-f <file>] [-d <file>]\n"
		"\n"
		"-h, --help\t\t - Display this help.\n"
		"-s, --traceScanning\t - Enables scanning process traces.\n"
		"-p, --traceParsing\t - Enables parsing process traces.\n"
		"-f, --form <file>\t - Form file for processing.\n"
		"-d, --data <file>\t - Data file for processing.\n"
		"\n"
	);
}
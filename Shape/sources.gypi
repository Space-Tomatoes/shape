{
	'variables' : {
		'source_files' : [
			# Shape Controller Handlers.
			'src/Handlers/CompilerController.cpp',
			'src/Handlers/DefaultFunctions.cpp',
			'src/Handlers/DefaultStorages.cpp',
			
			# Shape Compiler.
			'src/ShapeCompiler/ShapeCompiler.cpp',
			
			# Shape Parser.
			'src/ShapeParser/ShapeParser.cpp',
			'src/ShapeParser/ShapeParser.yy',
			
			# Shape Scanner.
			'src/ShapeScanner/ShapeScanner.cpp',
			'src/ShapeScanner/ShapeScanner.ll',
			
			# Shape Value
			'src/Value/Value.cpp',
		],
		'include_files' : [
			# General.
			'include/Shape.hpp',
			'include/Tokens.hpp', # todo: remove
			
			# Exceptions.
			'include/Shape/Exceptions/GenericException.hpp',
			'include/Shape/Exceptions/InvalidOperationException.hpp',
			'include/Shape/Exceptions/IOException.hpp',
			'include/Shape/Exceptions/SyntaxException.hpp',
			
			# Shape Controller Handlers.
			'include/Shape/Handlers/CompilerController.hpp',
			'include/Shape/Handlers/DefaultFunctions.hpp',
			'include/Shape/Handlers/DefaultStorages.hpp',
			'include/Shape/Handlers/IErrorHandler.hpp',
			'include/Shape/Handlers/IFunctionHandler.hpp',
			'include/Shape/Handlers/IStorageHandler.hpp',
			
			# Operators.
			'include/Shape/Operators/AbstractExpression.hpp',
			'include/Shape/Operators/AbstractOperator.hpp',
			'include/Shape/Operators/AccessExpression.hpp',
			'include/Shape/Operators/AssignExpression.hpp',
			'include/Shape/Operators/BinaryExpression.hpp',
			'include/Shape/Operators/BlockOperator.hpp',
			'include/Shape/Operators/ConditionOperator.hpp',
			'include/Shape/Operators/EchoOperator.hpp',
			'include/Shape/Operators/ForeachLoopOperator.hpp',
			'include/Shape/Operators/FunctionCallOperator.hpp',
			'include/Shape/Operators/LoopOperator.hpp',
			'include/Shape/Operators/UnaryExpression.hpp',
			'include/Shape/Operators/VariableExpression.hpp',
			
			# Shape Compiler.
			'include/Shape/ShapeCompiler/ShapeCompiler.hpp',
			
			# Shape Parser.
			'include/Shape/ShapeParser/Location.hpp',
			'include/Shape/ShapeParser/Position.hpp',
			'include/Shape/ShapeParser/ShapeParser.hpp',
			'include/Shape/ShapeParser/Stack.hpp',
			
			# Shape Value.
			'include/Shape/Value/Value.hpp',
		],		
	},
}
#pragma once

#pragma warning(disable: 4146)
#include <Shape/ShapeParser/ShapeParser.hpp>
#include <Shape/ShapeCompiler/ShapeCompiler.hpp>
#include <Shape/Handlers/IFunctionHandler.hpp>
#include <Shape/Handlers/IStorageHandler.hpp>
#include <Shape/Handlers/IErrorHandler.hpp>
#include <Shape/Handlers/CompilerController.hpp>

#if !defined(SHAPE_SEG)
	#define SHAPE_SEG
	static char szSeg[] = "$SEG$ 240614 1004 Shape.lib [Build: " __TIMESTAMP__ "]";
#endif
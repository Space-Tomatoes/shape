#pragma once

#include <iostream>
#include <list>
#include <string>
#include <algorithm>

#include <Shape/Value/Value.hpp>
#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>
#include <Shape/Operators/EchoOperator.hpp>
#include <Shape/Operators/BinaryExpression.hpp>
#include <Shape/Operators/BlockOperator.hpp>
#include <Shape/Operators/ConditionOperator.hpp>
#include <Shape/Operators/UnaryExpression.hpp>
#include <Shape/Operators/FunctionCallOperator.hpp>
#include <Shape/Operators/LoopOperator.hpp>
#include <Shape/Operators/ForeachLoopOperator.hpp>
#include <Shape/Operators/VariableExpression.hpp>
#include <Shape/Operators/AssignExpression.hpp>
#include <Shape/Operators/AccessExpression.hpp>
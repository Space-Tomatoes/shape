/// \file GenericException.hpp
/// \brief Shape's syntax error exception declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <Shape/Exceptions/GenericException.hpp>

namespace Shape
{
	/// \brief Syntax error exception.
	class SyntaxException : public GenericException
	{

	public:
		SyntaxException(const ErrCode &code, const std::string &message) :
			GenericException(code, message)
		{ }

		SyntaxException(const std::string &message) :
			GenericException(message)
		{ }

		SyntaxException(const ErrCode &code) :
			GenericException(code)
		{ }

		SyntaxException() :
			GenericException()
		{ }
	}; // class SyntaxException
} // namespace Shape
/// \file IOException.hpp
/// \brief Shape's I/O exception declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <Shape/Exceptions/GenericException.hpp>

namespace Shape
{
	/// \brief I/O exception.
	class IOException : public GenericException
	{

	public:
		IOException(const ErrCode &code, const std::string &message) :
			GenericException(code, message)
		{ }

		IOException(const std::string &message) :
			GenericException(message)
		{ }

		IOException(const ErrCode &code) :
			GenericException(code)
		{ }

		IOException() :
			GenericException()
		{ }
	}; // class IOException
} // namespace Shape
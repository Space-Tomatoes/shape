/// \file GenericException.hpp
/// \brief Generic Shape exception declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <string>
#include <Shape/ShapeParser/Location.hpp>

namespace Shape
{
	/// \brief Generic Shape exception.
	class GenericException
	{
	public:
		typedef short ErrCode;

	protected:
		std::string message;
		ErrCode code;
		Shape::location location;

	public:
		GenericException(const ErrCode &code, const std::string &message, const Shape::location &location) :
			code(code), message(message), location(location)
		{ }

		GenericException(const std::string &message, const Shape::location &location) :
			code(0), message(message), location(location)
		{ }

		GenericException(const ErrCode &code, const std::string &message) :
			code(code), message(message)
		{ }

		GenericException(const std::string &message) :
			code(0), message(message)
		{ }

		GenericException(const ErrCode &code) :
			code(code)
		{ }

		GenericException()
		{ }

	public:
		std::string GetMessage() const
		{
			return message;
		}

		ErrCode GetErrorCode() const
		{
			return code;
		}

		Shape::location GetLocation() const
		{
			return location;
		}
	}; // class GenericException
} // namespace Shape
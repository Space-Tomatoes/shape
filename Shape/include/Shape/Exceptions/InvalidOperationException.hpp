/// \file InvalidOperationException.hpp
/// \brief Shape's invalid operation exception declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <Shape/Exceptions/GenericException.hpp>
#include <Shape/ShapeParser/Location.hpp>

namespace Shape
{
	/// \brief Invalid operation exception.
	class InvalidOperationException : public GenericException
	{

	public:
		InvalidOperationException(const ErrCode &code, const std::string &message, const Shape::location &location) :
			GenericException(code, message, location)
		{ }

		InvalidOperationException(const std::string &message, const Shape::location &location) :
			GenericException(message, location)
		{ }

		InvalidOperationException(const ErrCode &code, const std::string &message) :
			GenericException(code, message)
		{ }

		InvalidOperationException(const std::string &message) :
			GenericException(message)
		{ }

		InvalidOperationException(const ErrCode &code) :
			GenericException(code)
		{ }

		InvalidOperationException() :
			GenericException()
		{ }
	}; // class InvalidOperationException
} // namespace Shape
/// \file ShapeCompiler.hpp Shape compiler declaration.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#pragma once

#include <iostream>
#include <string>
#include <map>
#include <memory>

#include <Shape/ShapeParser/ShapeParser.hpp>
#include <Shape/Handlers/CompilerController.hpp>
#include <Shape/Value/Value.hpp>

#ifndef YY_DECL
#define YY_DECL Shape::ShapeParser::symbol_type yylex(Shape::ShapeParser::semantic_type *yylval, Shape::ShapeCompiler &compiler)
YY_DECL;
#endif

namespace Shape
{
	class ShapeCompiler;
	class CompilerController;

	typedef std::shared_ptr<ShapeCompiler> ShapeCompilerPtr;
	//typedef std::unique_ptr<ShapeCompiler> ShapeCompilerPtr;

	// \brief Shape compiler class declaration.
	class ShapeCompiler
	{
		friend ShapeParser;
		friend VariableExpression;

	private:
		typedef enum EParserMode
		{
			File, 
			Buffer
		} ParserMode;
		ParserMode mode;
		std::string buffer;

		bool traceScanning;
		bool traceParsing;

		std::string fileName;
		std::string lastLine;
		AbstractOperatorPtr treeRoot;

		Shape::ShapeParser::semantic_type yylval;
		
		//BaseCompilerHandler *dataHandler;
		CompilerController *controller;

	public:
		/// \brief Constructor.
		ShapeCompiler();

		/// \brief Destructor.
		virtual ~ShapeCompiler();

		/// \brief OnScanBegin callback.
		void ScanBegin();

		/// \brief OnScanEnd callback.
		void ScanEnd();

		/// \brief Parses Shape file.
		/// \param [in] fileName File name.
		std::string ParseFile(const std::string &fileName);

		/// \brief Parses Shape script.
		/// \param [in] data Shape script data.
		std::string Parse(const std::string &data);

		/// \brief OnError callback.
		/// \param [in] location Location at the Shape script where error occurs.
		/// \param [in] message Error message text.
		void Error(const Shape::location &location, const std::string &message);

		/// \brief OnError callback.
		/// \param [in] message Error message text.
		void Error(const std::string &message);

		/// \brief Gets current file name.
		/// \return Current file name.
		std::string GetFileName() const;

		/// \brief Gets trace scanning option value.
		/// \return Trace scanning option value.
		bool GetTraceScanning() const;

		/// \brief Enables or Disables traces of scanning.
		/// \param traceScanning 
		void SetTraceScanning(bool traceScanning);

		/// \brief Gets trace parsing option value.
		/// \return Trace parsing option value.
		bool GetTraceParsing() const;

		/// \brief Enables or Disables traces of parsing.
		/// \param traceParsing 
		void SetTraceParsing(bool traceParsing);
		
		/*BaseCompilerHandler * GetDataHandler()
		{
			return this->dataHandler;
		}*/

		CompilerController * GetController()
		{
			return this->controller;
		}

		/*void SetDataHandler(BaseCompilerHandler *handler)
		{
			this->dataHandler = handler;
		}*/

		void SetController(CompilerController *controller)
		{
			this->controller = controller;
		}
	}; // class ShapeCompiler
} // namespace Shape
/// \file DefaultStorages.hpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#pragma once

#include <Shape/Handlers/IStorageHandler.hpp>

namespace Shape { namespace DefaultStorages
{
	class LocalStorage : 
		public IStorageHandler
	{
	private:
		Value memory;

	public:
		LocalStorage() :
			memory(Value::TArray)
		{ }

		Value * operator () (CompilerController *controller, const std::string &key) override;
	}; // class LocalStorage
} /* namespace DefaultStorages */ } // namespace Shape
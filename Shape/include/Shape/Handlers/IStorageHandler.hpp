/// \file IStorageHandler.hpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#pragma once

#include <Shape/Value/Value.hpp>

namespace Shape
{
	class CompilerController;

	/* virtual */ class IStorageHandler
	{
	public:
		virtual Value * operator () (CompilerController *controller, const std::string &key) = 0;
	}; // class IStorageHandler
} // namespace Shape
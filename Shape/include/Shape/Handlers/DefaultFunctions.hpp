/// \file DefaultFunctions.hpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#pragma once

#include <string>
#include <algorithm>
#include <iomanip>

#include <Shape/Handlers/IFunctionHandler.hpp>

namespace Shape { namespace DefaultFunctions
{
	class ExistFunction : 
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= !arg->Execute().IsVoid();
			}
			else value = false;

			return value;
		}
	}; // class ExistFunction

	class SizeFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			return (args.size() ? (Value::Integer)args[0]->Execute().GetSize() : Value(0));
		}
	}; // class SizeFunction

	class LeftFunction : 
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			Value value;

			if (args.size() >= 2)
			{
				std::string text = args[0]->Execute().ToString();
				std::streamsize width = args[1]->Execute().ToInteger();
				char fillCharacter = (args.size() == 3) ? args[2]->Execute().ToString()[0] : ' ';

				std::stringstream ss;
				ss	<< std::setiosflags(std::ios::left)
					<< std::setw(width)
					<< std::setfill(fillCharacter)
					<< text;

				value = ss.str();
			}

			return value;
		}
	}; // class LeftFunction

	class CenterFunction : 
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			Value value;

			if (args.size() >= 2)
			{
				std::string text = args[0]->Execute().ToString();
				std::streamsize width = args[1]->Execute().ToInteger();
				char fillCharacter = (args.size() == 3) ? args[2]->Execute().ToString()[0] : ' ';

				std::stringstream ss;
				ss	<< std::setiosflags(std::ios::left)
					<< std::setw(width)
					<< std::setfill(fillCharacter)
					<< (std::string((((size_t)width - text.size()) / 2), fillCharacter) + text);

				value = ss.str();
			}

			return value;
		}
	}; // class CenterFunction

	class RightFunction : 
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			Value value;

			if (args.size() >= 2)
			{
				std::string text = args[0]->Execute().ToString();
				std::streamsize width = args[1]->Execute().ToInteger();
				char fillCharacter = (args.size() == 3) ? args[2]->Execute().ToString()[0] : ' ';

				std::stringstream ss;
				ss	<< std::setiosflags(std::ios::right)
					<< std::setw(width)
					<< std::setfill(fillCharacter)
					<< text;

				value = ss.str();
			}

			return value;
		}
	}; // class RightFunction

	class ModFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			return (args.size() == 2) 
						? (args[0]->Execute().ToInteger() % args[1]->Execute().ToInteger()) 
						: Value();
		}
	}; // class ModFunction

	class ExecuteFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			for (const auto &arg : args)
				arg->Execute();

			return Value();
		}
	}; // class ExecuteFunction

	class ArrayFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			Value value(Value::TArray);

			for (const auto &arg : args)
				value.AppendChild(arg->Execute());

			return value;
		}
	}; // class ArrayFunction

	class IncludeFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override;
	}; // class IncludeFunction

	class IsIntegerFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= arg->Execute().IsInteger();
			}
			else value = false;

			return value;
		}
	}; // class IsIntegerFunction

	class IsVoidFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= arg->Execute().IsVoid();
			}
			else value = false;

			return value;
		}
	}; // class IsVoidFunction

	class IsBooleanFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= arg->Execute().IsBoolean();
			}
			else value = false;

			return value;
		}
	}; // class IsBooleanFunction

	class IsRealFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= arg->Execute().IsReal();
			}
			else value = false;

			return value;
		}
	}; // class IsRealFunction

	class IsStringFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= arg->Execute().IsString();
			}
			else value = false;

			return value;
		}
	}; // class IsStringFunction

	/*class IsObjectFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= arg->Execute().IsObject();
			}
			else value = false;

			return value;
		}
	}; // class IsObjectFunction*/

	class IsArrayFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			bool value = true;

			if (args.size())
			{
				for (const auto &arg : args)
					value &= arg->Execute().IsArray();
			}
			else value = false;

			return value;
		}
	}; // class IsArrayFunction

	class TypeOfFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			return args.size() ? (Value::Integer)args[0]->Execute().GetType() : Value();
		}
	}; // class TypeOfFunction

	class TimeFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			Value value;

			if (args.size() == 1)
			{
				std::string timestamp = args[0]->Execute().ToString();

				if (timestamp.length() == 12)
				{
					std::string year = timestamp.substr(0, 4);
					std::string mon = timestamp.substr(4, 2);
					std::string day = timestamp.substr(6, 2);
					std::string hour = timestamp.substr(8, 2);
					std::string min = timestamp.substr(10, 2);
			
					timestamp = day + "/" + mon + "/" + year + " " + hour + ":" + min;
					value = timestamp;
				}
			}
			
			return value;
		}
	}; // class TimeFunction

	class NowFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args)
		{
			time_t x;
			return (int)time(&x);
		}
	}; // class NowFunction

	class SortFunction :
		public IFunctionHandler
	{
		Value operator () (CompilerController *controller, ArgumentList &args) override
		{
			Value array = args[0]->Execute();

			if (args.size() > 1)
			{
				Value field = args[1]->Execute();

				for (Value i = 0; i < (Value::Integer)array.GetSize(); ++i)
				{
					for (Value j = 0; j < (Value::Integer)array.GetSize() - 1; ++j)
						if (array[j].GetChild(field) > array[j + 1].GetChild(field))
						{
							Value tmp = array[j + 1];
							array[j + 1] = array[j];
							array[j] = tmp;
						}
				}
			}
			else
			{
				for (Value i = 0; i < (Value::Integer)array.GetSize(); ++i)
				{
					for (Value j = 0; j < (Value::Integer)array.GetSize() - 1; ++j)
						if (array[j] > array[j + 1])
						{
							Value tmp = array[j + 1];
							array[j + 1] = array[j];
							array[j] = tmp;
						}
				}
			}

			return array;
		}
	}; // class SortFunction
} /* namespace DefaultFunctions */ } /* namespace Shape */
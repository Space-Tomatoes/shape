/// \file IFunctionHandler.hpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#pragma once

#include <Shape/Value/Value.hpp>

namespace Shape
{
	class CompilerController;

	/* virtual */ class IFunctionHandler
	{
	public:
		virtual Value operator () (CompilerController *controller, ArgumentList &args) = 0;
	}; // class IFunctionHandler
} // namespace Shape
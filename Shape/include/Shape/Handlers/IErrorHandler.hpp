/// \file IErrorHandler.hpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#pragma once

#include <Shape/Value/Value.hpp>

namespace Shape
{
	class CompilerController;

	/* virtual */ class IErrorHandler
	{
	public:
		virtual void operator () (CompilerController *controller, const Shape::location &location, const std::string &message) = 0;
	}; // class IFunctionHandler
} // namespace Shape
/// \file CompilerController.hpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#pragma once

#include <algorithm>
#include <string>
#include <unordered_map>

#include <Shape/Handlers/IFunctionHandler.hpp>
#include <Shape/Handlers/IStorageHandler.hpp>
#include <Shape/Handlers/IErrorHandler.hpp>
#include <Shape/Handlers/DefaultFunctions.hpp>
#include <Shape/Handlers/DefaultStorages.hpp>

namespace Shape
{
	class ShapeCompiler;

	class CompilerController
	{
	public:
		friend class DefaultStorages::LocalStorage;

	public:
		static ArgumentList EMPTY_ARGS;

	public:
		typedef enum EEmitResult
		{
			Ok,
			FunctionNotFound,
			ArgumentError,
			StorageNotFount,
			VariableNotFount,
			Unknown
		} EmitResult;

	private:
		std::unordered_map<std::string, IFunctionHandler *> _functionHandlers;
		std::unordered_map<std::string, IStorageHandler *> _storageHandlers;
		IErrorHandler *_errorHandler;

		CompilerController(const CompilerController &handler);

	public:
		CompilerController() :
			_errorHandler(nullptr)
		{
			SetDefaultHandlers();
		}		

		/// \brief Destructs Compiler Handler object.
		virtual ~CompilerController()
		{
			// Clearning up handlers of functions.
			for (const auto &handler : _functionHandlers)
				delete handler.second;			
			
			// Clearning up handlers of storages.
			for (const auto &handler : _storageHandlers)
				delete handler.second;

			if (_errorHandler != nullptr)
				delete _errorHandler;
		}

	public:
		void SetDefaultHandlers()
		{
			/* 
				Function Handlers. 
			*/			
			SetFunctionHandler<DefaultFunctions::ExistFunction>("exist");
			SetFunctionHandler<DefaultFunctions::SizeFunction>("size");
			
			SetFunctionHandler<DefaultFunctions::LeftFunction>("left");
			SetFunctionHandler<DefaultFunctions::LeftFunction>("l");
			SetFunctionHandler<DefaultFunctions::CenterFunction>("center");
			SetFunctionHandler<DefaultFunctions::CenterFunction>("c");
			SetFunctionHandler<DefaultFunctions::RightFunction>("right");
			SetFunctionHandler<DefaultFunctions::RightFunction>("r");

			SetFunctionHandler<DefaultFunctions::ModFunction>("mod");
			
			SetFunctionHandler<DefaultFunctions::ExecuteFunction>("execute");

			SetFunctionHandler<DefaultFunctions::ArrayFunction>("array");

			SetFunctionHandler<DefaultFunctions::IncludeFunction>("include");

			SetFunctionHandler<DefaultFunctions::IsIntegerFunction>("isInt");
			SetFunctionHandler<DefaultFunctions::IsVoidFunction>("isVoid");
			SetFunctionHandler<DefaultFunctions::IsBooleanFunction>("isBool");
			SetFunctionHandler<DefaultFunctions::IsRealFunction>("isReal");
			SetFunctionHandler<DefaultFunctions::IsStringFunction>("isString");
			//SetFunctionHandler<DefaultFunctions::IsObjectFunction>("isObject");
			SetFunctionHandler<DefaultFunctions::IsArrayFunction>("isArray");
			SetFunctionHandler<DefaultFunctions::TypeOfFunction>("typeOf");
			SetFunctionHandler<DefaultFunctions::TimeFunction>("time");
			SetFunctionHandler<DefaultFunctions::NowFunction>("now");
			SetFunctionHandler<DefaultFunctions::SortFunction>("sort");

			/* Storage Handlers. */
			SetStorageHandler<DefaultStorages::LocalStorage>("@");

			/* Error handler. */
		}

		/// \brief Sets the handler of function.
		/// \param [in] HandlerType Type of handler for setup.
		/// \param [in] functionName Name of function for handling.
		/// \return Returns true if the handler was established, or false if already established.
		template <typename HandlerType>
		bool SetFunctionHandler(const std::string &functionName)
		{
			if (_functionHandlers.find(functionName) == _functionHandlers.end())
			{
				_functionHandlers[functionName] = new HandlerType;
				return true;
			}
			else return false;
		}

		/// \brief Sets the handler of function.
		/// \param [in] HandlerType Type of handler for setup.
		/// \param [in] Arg1 Type of first constructor's argument.
		/// \param [in] functionName Name of function for handling.
		/// \return Returns true if the handler was established, or false if already established.
		template <typename HandlerType, typename Arg1>
		bool SetFunctionHandler(const std::string &functionName, Arg1 &argument)
		{
			if (_functionHandlers.find(functionName) == _functionHandlers.end())
			{
				_functionHandlers[functionName] = new HandlerType(argument);
				return true;
			}
			else return false;
		}
		
		/// \brief Unsets the handler of function.
		/// \param [in] functionName Name of function for handling.
		/// \return Returns true if the handler was unset, or false if already unset.
		bool UnsetFunctionHandler(const std::string &functionName)
		{
			auto el = _functionHandlers.find(functionName);
			
			if (el != _functionHandlers.end())
			{
				delete el->second;
				_functionHandlers.erase(el);
				return true;
			}
			else return false;
		}
		
		/// \brief Sets the handler of storage.
		/// \param [in] HandlerType Type of handler for setup.
		/// \param [in] functionName Name of storage for handling.
		/// \return Returns true if the handler was established, or false if already established.
		template <typename HandlerType>
		bool SetStorageHandler(const std::string &storageName)
		{
			if (_storageHandlers.find(storageName) == _storageHandlers.end())
			{
				_storageHandlers[storageName] = new HandlerType;
				return true;
			}
			else return false;
		}
		
		/// \brief Unsets the handler of storage.
		/// \param [in] functionName Name of storage for handling.
		/// \return Returns true if the handler was unset, or false if already unset.
		bool UnsetStorageHandler(const std::string &storageName)
		{
			auto el = _storageHandlers.find(storageName);

			if (el != _storageHandlers.end())
			{
				delete el->second;
				_storageHandlers.erase(el);
				return true;
			}
			else return false;
		}

		template <typename HandlerType>
		bool SetErrorHandler()
		{
			if (_errorHandler == nullptr)
			{
				_errorHandler = new HandlerType;
				return true;
			}
			else return false;
		}

		bool UnsetErrorHandler()
		{
			if (_errorHandler != nullptr)
			{
				delete _errorHandler;
				_errorHandler = nullptr;
				return true;
			}
			else return false;
		}
	
		EmitResult EmitFunctionHandler(const std::string &functionName, ArgumentList &args, Value &value)
		{
			auto el = _functionHandlers.find(functionName);
			
			if (el != _functionHandlers.end())
			{
				value = (*el->second)(this, args);
				return Ok;
			}
			else return FunctionNotFound;
		}
		
		EmitResult EmitStorageHandler(const std::string &storageName, const std::string &key, Value *&value)
		{
			auto el = _storageHandlers.find(storageName);

			if (el != _storageHandlers.end())
			{
				value = (*el->second)(this, key);
				return Ok;
			}
			else return StorageNotFount;
		}

		EmitResult EmitErrorHandler(const Shape::location &location, const std::string &message)
		{
			if (_errorHandler != nullptr)
			{
				(*_errorHandler)(this, location, message);
				return Ok;
			}
			else return FunctionNotFound;
		}
	}; // class CompilerController
} // namespace Shape
/// \file Value.hpp
/// \brief Shape-value declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <string>
#include <iostream>
#include <sstream>
#include <memory>
#include <unordered_map>

#include <Shape/Operators/AbstractExpression.hpp>
#include <Shape/Exceptions/InvalidOperationException.hpp>

namespace Shape
{
	class Value;
	typedef std::shared_ptr<Value> ValuePtr;

	/// \brief Shape value object.
	class Value : public AbstractExpression
	{
	public:
		static size_t GetHash(const Value &value)
		{	
			return std::hash<ValueType>()(value.type) ^
				   std::hash<Value::Integer>()(value.intValue) ^
				   std::hash<Value::Real>()(value.realValue) ^
				   std::hash<Value::String>()(value.stringValue) ^
				   std::hash<Value::Boolean>()(value.booleanValue) ^
				   std::hash<Value::ArrayPtr>()(value.arrayPtr);
		}

		static bool EqualTo(const Value &left, const Value &right)
		{
			return left.type == right.type &&
				   left.intValue == right.intValue &&
				   left.realValue == right.realValue &&
				   left.stringValue == right.stringValue &&
				   left.booleanValue == right.booleanValue &&
				   left.arrayPtr == right.arrayPtr;
		}

	public:
		/*
			Type definitions.
		*/	
		typedef void Void;
		typedef int Integer;
		typedef float Real;
		typedef std::string String;
		typedef bool Boolean;
		typedef std::unordered_map<Value, Value, decltype(&Value::GetHash), decltype(&Value::EqualTo)> Array;
		typedef std::shared_ptr<Array> ArrayPtr;
		
		/// \brief Shape value object types.
		typedef enum EValueType : unsigned __int8
		{
			TVoid,
			TInteger,
			TReal,
			TString,
			TBoolean,
			TArray,
			TObject,
			TReference
		} ValueType;

	private:
		ValueType type;
		Integer intValue;
		Real realValue;
		String stringValue;
		Boolean booleanValue;
		ArrayPtr arrayPtr;
		Value * ref;
		bool isSystem;

	public:
		/* 
			Constructors. 
		*/
		/// \brief Constructs a Void value object.
		Value(Void);

		Value(const Value &value);

		/// \brief Constructs an Integer value object.
		/// \param [in] value The initial value for the object.
		Value(const Integer &value);

		/// \brief Constructs a Real value object.
		/// \param [in] value The initial value for the object.
		Value(const Real &value);

		/// \brief Constructs a String value object.
		/// \param [in] value The initial value for the object.
		Value(const char *value);

		/// \brief Constructs a String value object.
		/// \param [in] value The initial value for the object.
		Value(const String &value);

		/// \brief Constructs a Boolean value object.
		/// \param [in] value The initial value for the object.
		Value(const Boolean &value);

		/// \brief Constructs a Array value object.
		/// \param [in] value The initial value for the object.
		Value(const ArrayPtr &value);

		Value(const ValueType &type);

	public:
		/*
			Assignment operators.
		*/
		/// \brief Copying assignment operator.
		/// \param [in] value The new value for the object
		/// \return Reference to the left operand.
		Value & operator = (const Value &value);

		/// \brief Integer assignment operator.
		/// \param [in] value The new value for the object
		/// \return Reference to the left operand.
		Value & operator = (const Integer &value);

		/// \brief Real assignment operator.
		/// \param [in] value The new value for the object
		/// \return Reference to the left operand.
		Value & operator = (const Real &value);

		/// \brief String assignment operator.
		/// \param [in] value The new value for the object
		/// \return Reference to the left operand.
		Value & operator = (const char *value);

		/// \brief String assignment operator.
		/// \param [in] value The new value for the object
		/// \return Reference to the left operand.
		Value & operator = (const String &value);

		/// \brief Boolean assignment operator.
		/// \param [in] value The new value for the object
		/// \return Reference to the left operand.
		Value & operator = (const Boolean &value);

		/// \brief Array assignment operator.
		/// \param [in] value The new value for the object
		/// \return Reference to the left operand.
		Value & operator = (const ArrayPtr &value);

		/*
			Binary arithmetic operators.
		*/
		/// \brief The addition operator.
		/// \param [in] value The right operand.
		/// \return A new object with the result of operation.
		const Value operator + (const Value &value) const;

		/// \brief The difference operator.
		/// \param [in] value The right operand.
		/// \return A new object with the result of operation.
		const Value operator - (const Value &value) const;

		/// \brief The multiplication operator.
		/// \param [in] value The right operand.
		/// \return A new object with the result of operation.
		const Value operator * (const Value &value) const;

		/// \brief The division operator.
		/// \param [in] value The right operand.
		/// \return A new object with the result of operation.
		const Value operator / (const Value &value) const;

		/*
			Binary arithmetic operators with assignment.
		*/
		/// \brief The addition operator with assignment.
		/// \param [in] value The right operand.
		/// \return Reference to the left operand.
		Value & operator += (const Value &value);

		/// \brief The difference operator with assignment.
		/// \param [in] value The right operand.
		/// \return Reference to the left operand.
		Value & operator -= (const Value &value);

		/// \brief The multiplication operator with assignment.
		/// \param [in] value The right operand.
		/// \return Reference to the left operand.
		Value & operator *= (const Value &value);

		/// \brief The division operator with assignment.
		/// \param [in] The right operand.
		/// \return Reference to the left operand.
		Value & operator /= (const Value &value);

		/*
			Unary operators.
		*/
		/// \brief The positive operator.
		/// \return A new object with the result of operation.
		const Value operator + () const;

		/// \brief The negation operator.
		/// \return A new object with the result of operation.
		const Value operator - () const;

		const Value operator ! () const;

		Value & operator -- ();
		Value & operator ++ ();

		/*
			Logical operators.
		*/
		/// \brief The equality operator.
		/// \param [in] left The left operand.
		/// \param [in] right The right operand.
		/// \return Result of a logical operation.
		friend bool operator == (const Value &left, const Value &right);

		friend bool operator != (const Value &left, const Value &right);

		/// \brief The less than operator
		/// \param [in] left The left operand.
		/// \param [in] right The right operand.
		/// \return Result of a logical operation.
		friend bool operator < (const Value &left, const Value &right);

		/// \brief The greater than operator.
		/// \param [in] left The left operand.
		/// \param [in] right The right operand.
		/// \return Result of a logical operation.
		friend bool operator > (const Value &left, const Value &right);

		/// \brief The less than or equal operator.
		/// \param [in] left The left operand.
		/// \param [in] right The right operand.
		/// \return Result of a logical operation.
		friend bool operator <= (const Value &left, const Value &right);

		/// \brief The greater than or equal operator.
		/// \param [in] left The left operand.
		/// \param [in] right The right operand.
		/// \return Result of a logical operation.
		friend bool operator >= (const Value &left, const Value &right);

		friend bool operator || (const Value &left, const Value &right);
		friend bool operator && (const Value &left, const Value &right);

		/*
			Cast operations.
		*/		
		/// \brief Casts a Void value to the Shape-value.
		void FromVoid(Void);

		/// \brief Casts an Integer value to the Shape-value.
		/// \param [in] value An Integer value.
		void FromInteger(const Integer &value);

		/// \brief Casts a Real value to the Shape-value.
		/// \param [in] value A Real value.
		void FromReal(const Real &value);

		/// \brief Casts a String value to the Shape-value.
		/// \param [in] value A String value.
		void FromString(const String &value);

		/// \brief Casts a Boolean value to the Shape-value.
		/// \param [in] value A Boolean value.
		void FromBoolean(const Boolean &value);

		void FromValue(const Value &value);

		void MakeRef(Value *ptr)
		{
			FromValue(*ptr);
			this->ref = ptr;
		}

		void ClearRef() { this->ref = nullptr; }

		/// \brief Casts the current value to an Integer value.
		/// \return The Integer representation of the current value.
		Integer ToInteger() const;

		/// \brief Casts the current value to a Real value.
		/// \return The Real representation of the current value.
		Real ToReal() const;

		/// \brief Casts the current value to a String value.
		/// \return The String representation of the current value.
		String ToString() const;

		/// \brief Casts the current value to a Boolean value.
		/// \return The Boolean representation of the current value.
		Boolean ToBoolean() const;

		bool IsVoid() const;
		bool IsInteger() const;
		bool IsReal() const;
		bool IsString() const;
		bool IsBoolean() const;
		bool IsArray() const;

		bool IsSystem() const
		{
			return this->isSystem;
		}

		void SetSystem(bool isSystem = true)
		{
			this->isSystem = isSystem;
		}

		/*
			Stream I/O.
		*/
		/// \brief The stream input operator.
		/// \param [in] stream Reference to the input stream.
		/// \param [out] value Value object into which the data will be read.
		/// \return Reference to the input stream.
		friend std::istream & operator >> (std::istream &stream, Value &value);

		/// \brief The stream output operator.
		/// \param [in] stream Reference to the output stream.
		/// \param [in] value Value object from which the data will be read.
		/// \return Reference to the output stream.
		friend std::ostream & operator << (std::ostream &stream, const Value &value);

		/// \brief Prints value.
		virtual void Print() const;

		virtual Value Execute() const;

		/// \brief Gets type of current value.
		/// \return Type of current value.
		ValueType GetType() const;

		ValueType GetImplicitConvertionType(const Value &value) const;

		// todo: move to string utility.
		/// \brief Replaces
		/// \param [in] where
		/// \param [in] what
		/// \param [in] withWhat
		void ReplaceAll(std::string &where, const std::string &what, const std::string &withWhat) const
		{
			int pos = -1;

			while (true) 
			{
				pos = where.find(what, ++pos);
				
				if (pos == -1)
					return;
				
				where.replace(pos, what.size(), withWhat);
				pos += what.size();
			}
		}

		/*
			Array operations.
		*/
		size_t GetSize() const
		{
			return (IsArray() && arrayPtr) ? arrayPtr->size() : 0u;
		}

		Value & GetChild(const Value &index) const
		{
			return (*arrayPtr)[index];
		}

		void SetChild(const Value &index, const Value &value)
		{
			if (IsArray() && arrayPtr)
				(*arrayPtr)[index] = value;
		}

		void AppendChild(const Value &value)
		{
			if (IsArray() && arrayPtr)
				(*arrayPtr)[(Value::Integer)arrayPtr->size()] = value;
		}

		Value & operator [] (const Value &index)
		{
			if (arrayPtr)
				return arrayPtr->operator[](index);
			else throw Shape::InvalidOperationException("Variable is not an object or array.");
		}

		ArrayPtr GetMap()
		{
			if (arrayPtr)
				return arrayPtr;
			else throw Shape::InvalidOperationException("Variable is not an object or array.");
		}

		void clear()
		{
			arrayPtr.reset();
			*this = Shape::Value(TVoid);
		}
	}; // class Value

	/*
		Logical operators.
	*/
	/// \brief The equality operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator == (const Value &left, const Value &right);

	bool operator == (const Value &left, const Value &right);

	/// \brief The less than operator
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator < (const Value &left, const Value &right);

	/// \brief The greater than operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator > (const Value &left, const Value &right);

	/// \brief The less than or equal operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator <= (const Value &left, const Value &right);

	/// \brief The greater than or equal operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator >= (const Value &left, const Value &right);

	bool operator || (const Value &left, const Value &right);
	bool operator && (const Value &left, const Value &right);

	/*
		Stream I/O.
	*/
	/// \brief The stream input operator.
	/// \param [in] stream Reference to the input stream.
	/// \param [out] value Value object into which the data will be read.
	/// \return Reference to the input stream.
	std::istream & operator >> (std::istream &stream, Value &value);

	/// \brief The stream output operator.
	/// \param [in] stream Reference to the output stream.
	/// \param [in] value Value object from which the data will be read.
	/// \return Reference to the output stream.
	std::ostream & operator << (std::ostream &stream, const Value &value);
} // namespace Shape
/// \file AssignExpression.hpp
/// \brief Assign expression declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <iostream>
#include <string>

#include <Shape/Operators/AbstractExpression.hpp>
#include <Shape/Operators/VariableExpression.hpp>
#include <Shape/Handlers/CompilerController.hpp>

namespace Shape
{
	class AssignExpression : public AbstractExpression
	{
	private:
		CompilerController *controller;
		AbstractExpressionPtr lvalue, value;
		std::string oper;

	public:
		AssignExpression(CompilerController *controller, const std::string &oper, const AbstractExpressionPtr &lvalue, const AbstractExpressionPtr &value) :
			controller(controller), lvalue(lvalue), value(value), oper(oper)
		{ }

		AssignExpression(CompilerController *controller, const std::string &oper, const AbstractExpressionPtr &lvalue) :
			controller(controller), lvalue(lvalue), oper(oper)
		{ }

		virtual void Print() const
		{
			lvalue->Print();
			std::cout << " = ";
			value->Print();
		}

		virtual Value Execute() const
		{
			Value result;

			if (controller)
			{
				Value *ptr = nullptr;

				auto var = std::dynamic_pointer_cast<VariableExpression>(lvalue);
			
				if (var)
				{
					auto rc = controller->EmitStorageHandler(var->GetScope(), var->GetName(), ptr);

					if (rc != CompilerController::Ok)
						ptr = &lvalue->Execute();
				}
				else
					ptr = &lvalue->Execute();

				if		(oper == "=")
				{
					(*ptr) = value->Execute();
				}
				else if (oper == "++")
				{
					++(*ptr);
				}
				else if (oper == "--")
				{
					--(*ptr);
				}
				else if (oper == "+=")
				{
					(*ptr) += value->Execute();
				}
				else if (oper == "-=")
				{
					(*ptr) -= value->Execute();
				} 
				else if (oper == "*=")
				{
					(*ptr) *= value->Execute();
				} 
				else if (oper == "/=")
				{
					(*ptr) /= value->Execute();
				} 
			}
			
			result.SetLocation(this->location);
			return result;
		}
	}; // class AssignOperator
} // namespace Shape
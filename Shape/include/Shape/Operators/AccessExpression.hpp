#pragma once

#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>

namespace Shape
{
	class AccessOperator : public AbstractExpression
	{
	private:
		AbstractExpressionPtr object, index;

	public:
		AccessOperator()
		{ }

		AccessOperator(const AbstractExpressionPtr &object, const AbstractExpressionPtr &index) :
			object(object), index(index)
		{ }
		
		virtual ~AccessOperator()
		{ }

		virtual void Print() const
		{ }

		virtual Value Execute() const
		{
			Value result;

			if (object && index)
			{
				auto val = object->Execute();

				if (val.IsArray())
				{
					auto idx = index->Execute();

					idx.ClearRef();

					if (idx.ToString() == "lenght")
					{
						result = (Shape::Value::Integer)val.GetSize();
					}
					else
					{
						auto zzz = &val.GetChild(idx);
						result.MakeRef(zzz);
					}
				}
			}

			result.SetLocation(this->location);
			return result;
		}

		void SetRoot(const AbstractExpressionPtr &root)
		{
			if (object && index)
			{
				auto acc = std::dynamic_pointer_cast<AccessOperator>(object);

				if (acc && root)
					acc->SetRoot(root);
			}
			else if (root && index)
				object = root;
		}

		void SetObject(const AbstractExpressionPtr &object)
		{
			this->object = object;
		}
	}; // class AccessOperator
} // namespace Shape
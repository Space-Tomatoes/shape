/// \file LoopOperator.hpp
/// \brief 
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>

namespace Shape
{
	class ForeachLoopOperator : public AbstractOperator
	{
	private:
		AbstractExpressionPtr arr, value, key;
		BlockOperatorPtr body;
		CompilerController *controller;

	public:
		ForeachLoopOperator()
		{ }

		ForeachLoopOperator(CompilerController *controller, const AbstractExpressionPtr &arr, const AbstractExpressionPtr &value, const AbstractExpressionPtr &key, const AbstractOperatorPtr &body) :
			controller(controller), arr(arr), value(value), key(key)
		{ 
			this->body = std::dynamic_pointer_cast<BlockOperator>(body);
		}

		virtual void Print(int indent = 0) const
		{
		}

		virtual Value Execute() const
		{
			Value result("");

			auto x = arr->Execute();

			if (x.IsArray())
			{
				auto &map = *x.GetMap();

				for (auto i = std::begin(map); i != std::end(map); ++i)
				{
					if (!i->second.IsSystem())
					{
						{
							Value *ptr = nullptr;

							auto lvalue = std::dynamic_pointer_cast<VariableExpression>(value);

							if (lvalue)
								controller->EmitStorageHandler(lvalue->GetScope(), lvalue->GetName(), ptr);
							else
								ptr = &value->Execute();

							*ptr = i->second;
						}

						{
							Value *ptr = nullptr;

							auto lvalue = std::dynamic_pointer_cast<VariableExpression>(key);

							if (lvalue)
								controller->EmitStorageHandler(lvalue->GetScope(), lvalue->GetName(), ptr);
							else
								ptr = &key->Execute();

							*ptr = i->first;
						}

						result += body->Execute().ToString();
					}
				}
			}

			result.SetLocation(this->location);
			return result;
		}
	}; // class ForeachLoopOperator
} // namespace Shape
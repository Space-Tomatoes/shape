/// \file BinaryExpression.hpp
/// \brief Binary Expression declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <iostream>
#include <string>

#include <Shape/Operators/AbstractExpression.hpp>

namespace Shape
{
	class UnaryExpression : public AbstractExpression
	{
	private:
		std::string oper;
		AbstractExpressionPtr arg;

	public:
		UnaryExpression(const std::string &oper, const AbstractExpressionPtr &arg) :
			oper(oper), arg(arg)
		{ }

		virtual void Print() const
		{
			std::cout << "(";
			arg->Print();
			std::cout << ")";
		}

		virtual Value Execute() const
		{
			Value tmp;

			if (oper == "+")
				tmp = +arg->Execute();
			else if (oper == "-")
				tmp = -arg->Execute();
			else if (oper == "!")			
				tmp = !arg->Execute();
			else if (oper == "--")
				tmp = --arg->Execute();
			else if (oper == "++")
				tmp = ++arg->Execute();
			else if (oper == "Integer")
				tmp = arg->Execute().ToInteger();
			else if (oper == "Real")
				tmp = arg->Execute().ToReal();
			else if (oper == "Boolean")
				tmp = arg->Execute().ToBoolean();
			else if (oper == "String")
				tmp = arg->Execute().ToString();

			tmp.SetLocation(this->location);
			return tmp;
		}

		virtual ~UnaryExpression()
		{ }
	};
} // namespace Shape
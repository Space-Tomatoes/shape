/// \file ConditionOperator.hpp
/// \brief Condition operator declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <memory>

#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>
#include <Shape/Operators/BlockOperator.hpp>

namespace Shape
{
	class ConditionOperator : public AbstractOperator
	{
	private:
		AbstractExpressionPtr cond;
		BlockOperatorPtr thenops, elseops;

	public: 
		ConditionOperator()
		{ 
			this->cond = std::make_shared<Value>(false);
			this->thenops = std::make_shared<BlockOperator>();
			this->elseops = std::make_shared<BlockOperator>();
		}

		ConditionOperator(const AbstractExpressionPtr &cond, const AbstractOperatorPtr &thenops, const AbstractOperatorPtr &elseops)
		{ 
			this->cond = cond;
			this->thenops = std::dynamic_pointer_cast<BlockOperator>(thenops);
			this->elseops = std::dynamic_pointer_cast<BlockOperator>(elseops);
		}

		ConditionOperator(const AbstractExpressionPtr &cond, const AbstractOperatorPtr &thenops)
		{
			this->cond = cond;
			this->thenops = std::dynamic_pointer_cast<BlockOperator>(thenops);
			this->elseops = std::make_shared<BlockOperator>();
		}

		virtual void Print(int indent = 0) const
		{
			std::cout << "{if "; 
			cond->Print();
			std::cout << "}" << std::endl;

			thenops->Print(indent + 1);

			if (elseops->Size() > 0)
			{
				std::cout << std::string(indent, '\t') << "{else}" << std::endl;
				elseops->Print(indent + 1);
			}

			std::cout << std::string(indent, '\t') << "{/if}" << std::endl;
		}

		virtual Value Execute() const
		{
			Value result;

			if (cond->Execute().ToBoolean())
				result = thenops->Execute();
			else if (elseops)
				result = elseops->Execute();

			result.SetLocation(this->location);
			return result;
		}

		void SetElse(const AbstractOperatorPtr &elseops)
		{
			this->elseops = std::make_shared<BlockOperator>(elseops);//std::dynamic_pointer_cast<BlockOperator>(elseops);
		}

		AbstractOperatorPtr GetElse()
		{
			return this->elseops;
		}

		virtual ~ConditionOperator() 
		{ }
	}; // class ConditionOperator
} // namespace Shape
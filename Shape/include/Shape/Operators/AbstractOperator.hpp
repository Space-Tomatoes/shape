/// \file AbstractOperator.hpp
/// \brief Abstract operator declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <list>
#include <memory>

#include <Shape/Value/Value.hpp>
#include <Shape/ShapeParser/Location.hpp>

namespace Shape
{
	class AbstractOperator;
	typedef std::shared_ptr<AbstractOperator> AbstractOperatorPtr;
	typedef std::list<AbstractOperatorPtr> OperatorList;	

	class AbstractOperator
	{
	protected:
		Shape::location location;

	protected:
		AbstractOperator()
		{ }
		
	public:
		virtual ~AbstractOperator()
		{ }

		virtual void Print(int indent = 0) const = 0;

		virtual Value Execute() const = 0;

		virtual void SetLocation(const Shape::location &location)
		{
			this->location = location;
		}

		virtual Shape::location GetLocation() const
		{
			return this->location;
		}
	}; // class AbstractOperator
} // namespace Shape
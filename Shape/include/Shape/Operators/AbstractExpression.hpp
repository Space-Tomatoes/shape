/// \file AbstractExpression.hpp
/// \brief Abstract expression declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <list>
#include <vector>
#include <memory>

#include <Shape/Value/Value.hpp>
#include <Shape/ShapeParser/Location.hpp>

namespace Shape
{
	class AbstractExpression;
	class Value;
	
	typedef std::shared_ptr<AbstractExpression> AbstractExpressionPtr;
	typedef std::vector<AbstractExpressionPtr> ExpressionList;	
	typedef ExpressionList ArgumentList;

	/// \brief Abstract expression.
	class AbstractExpression
	{
	protected:
		Shape::location location;

	protected: 
		/// \brief Constructs an abstract expression.
		AbstractExpression()
		{ }

	public:
		/// \brief Destructs an abstract expression.
		virtual ~AbstractExpression()
		{ }

		virtual Value Execute() const = 0;

		/// \brief Prints expression source code.
		virtual void Print() const = 0;

		virtual void SetLocation(const Shape::location &location)
		{
			this->location = location;
		}

		virtual Shape::location GetLocation() const
		{
			return this->location;
		}
	}; // class AbstractExpression
} // namespace Shape
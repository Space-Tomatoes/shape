/// \file FunctionCallOperator.hpp
/// \brief Function call operator declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <string>
#include <iostream>
#include <memory>

#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>
#include <Shape/Handlers/CompilerController.hpp>

namespace Shape
{
	class FunctionCallExpression : public AbstractExpression
	{
	private:
		std::string name;
		ArgumentList args;
		CompilerController *controller;

	public:
		FunctionCallExpression(CompilerController *controller, const std::string &name, ArgumentList &args) :
			controller(controller), name(name), args(args)
		{ }

		virtual void Print() const
		{
			std::cout << name << "(";

			for (auto i = std::begin(args); i != std::end(args); ++i)
			{
				if (i != args.begin())
					std::cout<<", ";

				(*i)->Print();
			}

			std::cout << ")";
		}

		virtual Value Execute() const
		{
			Value result;

			if (controller != nullptr)
				controller->EmitFunctionHandler(name, (ArgumentList &)args, result);

			result.SetLocation(this->location);
			return result;
		}

		virtual ~FunctionCallExpression() 
		{ }
	}; // class FunctionCallOperator
} // namespace Shape
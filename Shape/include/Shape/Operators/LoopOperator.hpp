/// \file LoopOperator.hpp
/// \brief 
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>

namespace Shape
{
	class LoopOperator : public AbstractOperator
	{
	private:
		AbstractExpressionPtr expr1, expr2, expr3;
		BlockOperatorPtr body;

	public:
		LoopOperator()
		{ }
		
		LoopOperator(const AbstractExpressionPtr &expr2, const AbstractOperatorPtr &body)
		{
			this->expr1 = std::shared_ptr<Value>();
			this->expr2 = expr2;
			this->expr1 = std::shared_ptr<Value>();
			this->body = std::dynamic_pointer_cast<BlockOperator>(body);
		}

		LoopOperator(const AbstractExpressionPtr &expr1, const AbstractExpressionPtr &expr2, const AbstractExpressionPtr &expr3,  const AbstractOperatorPtr &body)
		{
			this->expr1 = expr1;
			this->expr2 = expr2;
			this->expr3 = expr3;
			this->body = std::dynamic_pointer_cast<BlockOperator>(body);
		}

		virtual void Print(int indent = 0) const
		{
		}

		virtual Value Execute() const
		{
			Value result("");

			for (	expr1 ? expr1->Execute() : true; 
					expr2->Execute().ToBoolean(); 
					expr3 ? expr3->Execute() : true		)
			{
				result += body->Execute().ToString();
			}

			result.SetLocation(this->location);
			return result;
		}
	}; // class LoopOperator
} // namespace Shape
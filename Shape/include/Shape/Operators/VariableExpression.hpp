/// \file VariableExpression.hpp
/// \brief Variable expression operator declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <string>
#include <iostream>
#include <memory>

#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>
#include <Shape/Handlers/CompilerController.hpp>

namespace Shape
{
	class VariableExpression;

	typedef std::shared_ptr<VariableExpression> VariableExpressionPtr;

	class VariableExpression : 
		public AbstractExpression
	{
	private:
		std::string name, scope;

		CompilerController *controller;

	public:
		VariableExpression(CompilerController *controller, const std::string scope, std::string &name) :
			controller(controller), scope(scope), name(name)
		{ }

		virtual void Print() const
		{
			std::cout << scope << name;
		}

		virtual Value Execute() const
		{
			Value value;

			if (controller)
			{
				Value *result = nullptr;
				if (controller->EmitStorageHandler(scope, name, result) == CompilerController::Ok)
				{
					value = *result;
					value.SetLocation(this->location);
				}
			}

			return value;
		}

		std::string GetScope()
		{
			return scope;
		}

		std::string GetName()
		{
			return name;
		}
	}; // class VariableExpression
} // namespace Shape
/// \file BlockOperator.hpp
/// \brief Block operator declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <string>
#include <iostream>
#include <memory>

#include <Shape/Operators/AbstractOperator.hpp>

namespace Shape
{
	class BlockOperator;

	typedef std::shared_ptr<BlockOperator> BlockOperatorPtr;

	class BlockOperator : public AbstractOperator
	{
	private:
		OperatorList ops;

	public:
		void Append(const AbstractOperatorPtr &op) 
		{
			auto block = std::dynamic_pointer_cast<BlockOperator>(op);

			if (block.get())
				ops.splice(ops.end(), block->ops, block->ops.begin(), block->ops.end());
			else ops.push_back(op);
		}

	public:
		BlockOperator() 
		{ }

		BlockOperator(const AbstractOperatorPtr &op)
		{
			Append(op);
		}

		BlockOperator(const AbstractOperatorPtr &op1, const AbstractOperatorPtr &op2)
		{ 
			Append(op1); 
			Append(op2); 
		}

		int Size() const
		{ 
			return ops.size(); 
		}

		virtual void Print(int indent = 0) const
		{
			for (auto i = std::begin(ops); i != std::end(ops); ++i)
			{
				std::cout << std::string(indent, '\t');
				(*i)->Print(indent);
			}
		}

		virtual Value Execute() const
		{
			Value tmp("");

			for (auto i = std::begin(ops); i != std::end(ops); ++i)
			{
				auto x = (*i)->Execute();

				tmp += x.ToString();
			}

			tmp.SetLocation(this->location);
			return tmp;
		}

		bool IsEmpty()
		{
			return ops.empty();
		}

		virtual ~BlockOperator() 
		{ }
	}; // class BlockOperator
} // namespace Shape
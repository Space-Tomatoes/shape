/// \file BlockOperator.hpp
/// \brief Block operator declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <Shape/Operators/AbstractOperator.hpp>
#include <Shape/Operators/AbstractExpression.hpp>

namespace Shape
{
	class EchoOperator : public AbstractOperator
	{
	private:
		AbstractExpressionPtr expr;

	public: 
		EchoOperator(const AbstractExpressionPtr &expr) :
			expr(expr) 
		{ }
	
		virtual void Print(int indent = 0) const
		{
			std::cout << "{echo ";
			expr->Print();
			std::cout << "}" << std::endl;
		}

		virtual Value Execute() const
		{
			Value tmp = expr->Execute();

			tmp.SetLocation(this->location);

			return tmp;
		}

		virtual ~EchoOperator() 
		{ }
	}; // class EchoOperator
} // namespace Shape
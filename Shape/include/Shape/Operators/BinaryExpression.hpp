/// \file BinaryExpression.hpp
/// \brief Binary Expression declarations.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <iostream>
#include <string>

#include <Shape/Operators/AbstractExpression.hpp>
#include <Shape/Operators/VariableExpression.hpp>

namespace Shape
{
	class BinaryExpression : public AbstractExpression
	{
	private:
		std::string oper;
		AbstractExpressionPtr arg1, arg2;

	public: 
		BinaryExpression(const std::string &oper, const AbstractExpressionPtr &arg1, const AbstractExpressionPtr &arg2) :
			oper(oper), arg1(arg1), arg2(arg2)
		{ }

		virtual void Print() const
		{
			std::cout << "(";
			arg1->Print();
			std::cout << " " << oper << " ";
			arg2->Print();
			std::cout << ")";
		}

		virtual Value Execute() const
		{
			Value tmp;
			
			if		(oper == "+")
				tmp = arg1->Execute() + arg2->Execute();
			else if (oper == "-")
				tmp = arg1->Execute() - arg2->Execute();
			else if (oper == "*")
				tmp = arg1->Execute() * arg2->Execute();
			else if (oper == "/")
			{
				auto x = arg1->Execute();
				auto y = arg2->Execute();

				tmp = arg1->Execute() / arg2->Execute();
			}
			else if (oper == "||")
				tmp = arg1->Execute() || arg2->Execute();
			else if (oper == "&&")
				tmp = arg1->Execute() && arg2->Execute();			
			else if (oper == "==")
				tmp = arg1->Execute() == arg2->Execute();
			else if (oper == "!=")
				tmp = arg1->Execute() != arg2->Execute();
			else if (oper == "<")
				tmp = arg1->Execute() < arg2->Execute();
			else if (oper == "<=")
				tmp = arg1->Execute() <= arg2->Execute();
			else if (oper == ">")
				tmp = arg1->Execute() > arg2->Execute();
			else if (oper == ">=")
				tmp = arg1->Execute() >= arg2->Execute();
			
			tmp.SetLocation(this->location);
			return tmp;
		}

		virtual ~BinaryExpression()
		{ }
	}; // class BinaryExpression
} // namespace Shape
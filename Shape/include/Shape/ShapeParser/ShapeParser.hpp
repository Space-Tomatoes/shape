// A Bison parser, made by GNU Bison 3.0.

// Skeleton interface for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2013 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

/**
 ** \file Shape/include/Shape/ShapeParser/ShapeParser.hpp
 ** Define the Shape::parser class.
 */

// C++ LALR(1) parser skeleton written by Akim Demaille.

#ifndef YY_YY_SHAPE_INCLUDE_SHAPE_SHAPEPARSER_SHAPEPARSER_HPP_INCLUDED
# define YY_YY_SHAPE_INCLUDE_SHAPE_SHAPEPARSER_SHAPEPARSER_HPP_INCLUDED
// //                    "%code requires" blocks.
#line 14 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:378

	#pragma warning(push)
	#pragma warning(disable: 4005 4065)

	#include <string>
	#include <memory>

	#include <Tokens.hpp>

	#define YYERROR_VERBOSE		(1)
	#define YYDEBUG				(1)
	
	namespace Shape
	{
		class ShapeCompiler;
	} // namespace Shape

#line 62 "Shape/include/Shape/ShapeParser/ShapeParser.hpp" // lalr1.m4:378


# include <vector>
# include <iostream>
# include <stdexcept>
# include <string>
# include "Stack.hpp"
# include "Location.hpp"

#ifndef YYASSERT
# include <cassert>
# define YYASSERT assert
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

#line 6 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:378
namespace Shape {
#line 85 "Shape/include/Shape/ShapeParser/ShapeParser.hpp" // lalr1.m4:378



  /// A char[S] buffer to store and retrieve objects.
  ///
  /// Sort of a variant, but does not keep track of the nature
  /// of the stored data, since that knowledge is available
  /// via the current state.
  template <size_t S>
  struct variant
  {
    /// Type of *this.
    typedef variant<S> self_type;

    /// Empty construction.
    variant ()
    {}

    /// Construct and fill.
    template <typename T>
    variant (const T& t)
    {
      YYASSERT (sizeof (T) <= S);
      new (yyas_<T> ()) T (t);
    }

    /// Destruction, allowed only if empty.
    ~variant ()
    {}

    /// Instantiate an empty \a T in here.
    template <typename T>
    T&
    build ()
    {
      return *new (yyas_<T> ()) T;
    }

    /// Instantiate a \a T in here from \a t.
    template <typename T>
    T&
    build (const T& t)
    {
      return *new (yyas_<T> ()) T (t);
    }

    /// Accessor to a built \a T.
    template <typename T>
    T&
    as ()
    {
      return *yyas_<T> ();
    }

    /// Const accessor to a built \a T (for %printer).
    template <typename T>
    const T&
    as () const
    {
      return *yyas_<T> ();
    }

    /// Swap the content with \a other, of same type.
    ///
    /// Both variants must be built beforehand, because swapping the actual
    /// data requires reading it (with as()), and this is not possible on
    /// unconstructed variants: it would require some dynamic testing, which
    /// should not be the variant's responsability.
    /// Swapping between built and (possibly) non-built is done with
    /// variant::move ().
    template <typename T>
    void
    swap (self_type& other)
    {
      std::swap (as<T> (), other.as<T> ());
    }

    /// Move the content of \a other to this.
    ///
    /// Destroys \a other.
    template <typename T>
    void
    move (self_type& other)
    {
      build<T> ();
      swap<T> (other);
      other.destroy<T> ();
    }

    /// Copy the content of \a other to this.
    template <typename T>
    void
    copy (const self_type& other)
    {
      build<T> (other.as<T> ());
    }

    /// Destroy the stored \a T.
    template <typename T>
    void
    destroy ()
    {
      as<T> ().~T ();
    }

  private:
    /// Prohibit blind copies.
    self_type& operator=(const self_type&);
    variant (const self_type&);

    /// Accessor to raw memory as \a T.
    template <typename T>
    T*
    yyas_ ()
    {
      void *yyp = yybuffer_.yyraw;
      return static_cast<T*> (yyp);
     }

    /// Const accessor to raw memory as \a T.
    template <typename T>
    const T*
    yyas_ () const
    {
      const void *yyp = yybuffer_.yyraw;
      return static_cast<const T*> (yyp);
     }

    union
    {
      /// Strongest alignment constraints.
      long double yyalign_me;
      /// A buffer large enough to store any of the semantic values.
      char yyraw[S];
    } yybuffer_;
  };


  /// A Bison parser.
  class ShapeParser
  {
  public:
#ifndef YYSTYPE
    /// An auxiliary type to compute the largest semantic type.
    union union_type
    {
      // ForeachOptionalArg
      // ForExpr
      // Expr
      // Value
      // Variable
      // AccessOperator
      // Index
      char dummy1[sizeof(AbstractExpressionPtr)];

      // Start
      // StatementList
      // Statement
      // ElseIfList
      // ElseSingle
      char dummy2[sizeof(AbstractOperatorPtr)];

      // FunctionCallParameterList
      char dummy3[sizeof(ArgumentList)];

      // "(Real)"
      char dummy4[sizeof(float)];

      // "(Integer)"
      char dummy5[sizeof(int)];

      // "(String)"
      // "(Identifier)"
      // "(Content)"
      char dummy6[sizeof(std::string)];
};

    /// Symbol semantic values.
    typedef variant<sizeof(union_type)> semantic_type;
#else
    typedef YYSTYPE semantic_type;
#endif
    /// Symbol locations.
    typedef location location_type;

    /// Syntax errors thrown from user actions.
    struct syntax_error : std::runtime_error
    {
      syntax_error (const location_type& l, const std::string& m);
      location_type location;
    };

    /// Tokens.
    struct token
    {
      enum yytokentype
      {
        End = 0,
        Real = 258,
        Integer = 259,
        String = 260,
        Identifier = 261,
        True = 262,
        False = 263,
        OpenCurlyBrace = 264,
        CloseCurlyBrace = 265,
        OpenBrace = 266,
        CloseBrace = 267,
        OpenBracket = 268,
        CloseBracket = 269,
        If = 270,
        ElseIf = 271,
        Else = 272,
        EndIf = 273,
        While = 274,
        EndWhile = 275,
        Do = 276,
        For = 277,
        EndFor = 278,
        Foreach = 279,
        EndForeach = 280,
        Dollar = 281,
        Sharp = 282,
        At = 283,
        LT = 284,
        GT = 285,
        EQ = 286,
        NE = 287,
        LE = 288,
        GE = 289,
        Not = 290,
        Plus = 291,
        PlusPlus = 292,
        Minus = 293,
        MinusMinus = 294,
        Multiply = 295,
        Division = 296,
        PlusAssign = 297,
        MinusAssign = 298,
        MultiplyAssign = 299,
        DivisionAssign = 300,
        Comma = 301,
        Break = 302,
        Continue = 303,
        Echo = 304,
        Comment = 305,
        Content = 306,
        Semicolon = 307,
        Assign = 308,
        Or = 309,
        And = 310,
        Inc = 311,
        IntegerCast = 312,
        RealCast = 313,
        StringCast = 314,
        ArrayCast = 315,
        BooleanCast = 316,
        As = 317,
        Arrow = 318,
        Dot = 319
      };
    };

    /// (External) token type, as returned by yylex.
    typedef token::yytokentype token_type;

    /// Internal symbol number.
    typedef int symbol_number_type;

    /// Internal symbol number for tokens (subsumed by symbol_number_type).
    typedef unsigned char token_number_type;

    /// A complete symbol.
    ///
    /// Expects its Base type to provide access to the symbol type
    /// via type_get().
    ///
    /// Provide access to semantic value and location.
    template <typename Base>
    struct basic_symbol : Base
    {
      /// Alias to Base.
      typedef Base super_type;

      /// Default constructor.
      basic_symbol ();

      /// Copy constructor.
      basic_symbol (const basic_symbol& other);

      /// Constructor for valueless symbols, and symbols from each type.

  basic_symbol (typename Base::kind_type t, const location_type& l);

  basic_symbol (typename Base::kind_type t, const AbstractExpressionPtr v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const AbstractOperatorPtr v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const ArgumentList v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const float v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const int v, const location_type& l);

  basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l);


      /// Constructor for symbols with semantic value.
      basic_symbol (typename Base::kind_type t,
                    const semantic_type& v,
                    const location_type& l);

      ~basic_symbol ();

      /// Destructive move, \a s is emptied into this.
      void move (basic_symbol& s);

      /// The semantic value.
      semantic_type value;

      /// The location.
      location_type location;

    private:
      /// Assignment operator.
      basic_symbol& operator= (const basic_symbol& other);
    };

    /// Type access provider for token (enum) based symbols.
    struct by_type
    {
      /// Default constructor.
      by_type ();

      /// Copy constructor.
      by_type (const by_type& other);

      /// The symbol type as needed by the constructor.
      typedef token_type kind_type;

      /// Constructor from (external) token numbers.
      by_type (kind_type t);

      /// Steal the symbol type from \a that.
      void move (by_type& that);

      /// The (internal) type number (corresponding to \a type).
      /// -1 when this symbol is empty.
      symbol_number_type type_get () const;

      /// The token.
      token_type token () const;

      enum { empty = 0 };

      /// The symbol type.
      /// -1 when this symbol is empty.
      token_number_type type;
    };

    /// "External" symbols: returned by the scanner.
    typedef basic_symbol<by_type> symbol_type;

    // Symbol constructors declarations.
    static inline
    symbol_type
    make_End (const location_type& l);

    static inline
    symbol_type
    make_Real (const float& v, const location_type& l);

    static inline
    symbol_type
    make_Integer (const int& v, const location_type& l);

    static inline
    symbol_type
    make_String (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_Identifier (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_True (const location_type& l);

    static inline
    symbol_type
    make_False (const location_type& l);

    static inline
    symbol_type
    make_OpenCurlyBrace (const location_type& l);

    static inline
    symbol_type
    make_CloseCurlyBrace (const location_type& l);

    static inline
    symbol_type
    make_OpenBrace (const location_type& l);

    static inline
    symbol_type
    make_CloseBrace (const location_type& l);

    static inline
    symbol_type
    make_OpenBracket (const location_type& l);

    static inline
    symbol_type
    make_CloseBracket (const location_type& l);

    static inline
    symbol_type
    make_If (const location_type& l);

    static inline
    symbol_type
    make_ElseIf (const location_type& l);

    static inline
    symbol_type
    make_Else (const location_type& l);

    static inline
    symbol_type
    make_EndIf (const location_type& l);

    static inline
    symbol_type
    make_While (const location_type& l);

    static inline
    symbol_type
    make_EndWhile (const location_type& l);

    static inline
    symbol_type
    make_Do (const location_type& l);

    static inline
    symbol_type
    make_For (const location_type& l);

    static inline
    symbol_type
    make_EndFor (const location_type& l);

    static inline
    symbol_type
    make_Foreach (const location_type& l);

    static inline
    symbol_type
    make_EndForeach (const location_type& l);

    static inline
    symbol_type
    make_Dollar (const location_type& l);

    static inline
    symbol_type
    make_Sharp (const location_type& l);

    static inline
    symbol_type
    make_At (const location_type& l);

    static inline
    symbol_type
    make_LT (const location_type& l);

    static inline
    symbol_type
    make_GT (const location_type& l);

    static inline
    symbol_type
    make_EQ (const location_type& l);

    static inline
    symbol_type
    make_NE (const location_type& l);

    static inline
    symbol_type
    make_LE (const location_type& l);

    static inline
    symbol_type
    make_GE (const location_type& l);

    static inline
    symbol_type
    make_Not (const location_type& l);

    static inline
    symbol_type
    make_Plus (const location_type& l);

    static inline
    symbol_type
    make_PlusPlus (const location_type& l);

    static inline
    symbol_type
    make_Minus (const location_type& l);

    static inline
    symbol_type
    make_MinusMinus (const location_type& l);

    static inline
    symbol_type
    make_Multiply (const location_type& l);

    static inline
    symbol_type
    make_Division (const location_type& l);

    static inline
    symbol_type
    make_PlusAssign (const location_type& l);

    static inline
    symbol_type
    make_MinusAssign (const location_type& l);

    static inline
    symbol_type
    make_MultiplyAssign (const location_type& l);

    static inline
    symbol_type
    make_DivisionAssign (const location_type& l);

    static inline
    symbol_type
    make_Comma (const location_type& l);

    static inline
    symbol_type
    make_Break (const location_type& l);

    static inline
    symbol_type
    make_Continue (const location_type& l);

    static inline
    symbol_type
    make_Echo (const location_type& l);

    static inline
    symbol_type
    make_Comment (const location_type& l);

    static inline
    symbol_type
    make_Content (const std::string& v, const location_type& l);

    static inline
    symbol_type
    make_Semicolon (const location_type& l);

    static inline
    symbol_type
    make_Assign (const location_type& l);

    static inline
    symbol_type
    make_Or (const location_type& l);

    static inline
    symbol_type
    make_And (const location_type& l);

    static inline
    symbol_type
    make_Inc (const location_type& l);

    static inline
    symbol_type
    make_IntegerCast (const location_type& l);

    static inline
    symbol_type
    make_RealCast (const location_type& l);

    static inline
    symbol_type
    make_StringCast (const location_type& l);

    static inline
    symbol_type
    make_ArrayCast (const location_type& l);

    static inline
    symbol_type
    make_BooleanCast (const location_type& l);

    static inline
    symbol_type
    make_As (const location_type& l);

    static inline
    symbol_type
    make_Arrow (const location_type& l);

    static inline
    symbol_type
    make_Dot (const location_type& l);


    /// Build a parser object.
    ShapeParser (Shape::ShapeParser::semantic_type *yylval_yyarg, Shape::ShapeCompiler &compiler_yyarg);
    virtual ~ShapeParser ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

#if YYDEBUG
    /// The current debugging stream.
    std::ostream& debug_stream () const;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);
#endif

    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);

    /// Report a syntax error.
    void error (const syntax_error& err);

  private:
    /// This class is not copyable.
    ShapeParser (const ShapeParser&);
    ShapeParser& operator= (const ShapeParser&);

    /// State numbers.
    typedef int state_type;

    /// Generate an error message.
    /// \param yystate   the state where the error occurred.
    /// \param yytoken   the lookahead token type, or yyempty_.
    virtual std::string yysyntax_error_ (state_type yystate,
                                         symbol_number_type yytoken) const;

    /// Compute post-reduction state.
    /// \param yystate   the current state
    /// \param yylhs     the nonterminal to push on the stack
    state_type yy_lr_goto_state_ (state_type yystate, int yylhs);

    /// Whether the given \c yypact_ value indicates a defaulted state.
    /// \param yyvalue   the value to check
    static bool yy_pact_value_is_default_ (int yyvalue);

    /// Whether the given \c yytable_ value indicates a syntax error.
    /// \param yyvalue   the value to check
    static bool yy_table_value_is_error_ (int yyvalue);

    static const signed char yypact_ninf_;
    static const signed char yytable_ninf_;

    /// Convert a scanner token number \a t to a symbol number.
    static token_number_type yytranslate_ (token_type t);

    // Tables.
  // YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
  // STATE-NUM.
  static const short int yypact_[];

  // YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
  // Performed when YYTABLE does not specify something else to do.  Zero
  // means the default is an error.
  static const unsigned char yydefact_[];

  // YYPGOTO[NTERM-NUM].
  static const signed char yypgoto_[];

  // YYDEFGOTO[NTERM-NUM].
  static const signed char yydefgoto_[];

  // YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
  // positive, shift that token.  If negative, reduce the rule whose
  // number is the opposite.  If YYTABLE_NINF, syntax error.
  static const short int yytable_[];

  static const signed char yycheck_[];

  // YYSTOS[STATE-NUM] -- The (internal number of the) accessing
  // symbol of state STATE-NUM.
  static const unsigned char yystos_[];

  // YYR1[YYN] -- Symbol number of symbol that rule YYN derives.
  static const unsigned char yyr1_[];

  // YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.
  static const unsigned char yyr2_[];


    /// Convert the symbol name \a n to a form suitable for a diagnostic.
    static std::string yytnamerr_ (const char *n);


    /// For a symbol, its name in clear.
    static const char* const yytname_[];
#if YYDEBUG
  // YYRLINE[YYN] -- Source line where rule number YYN was defined.
  static const unsigned short int yyrline_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r);
    /// Print the state stack on the debug stream.
    virtual void yystack_print_ ();

    // Debugging.
    int yydebug_;
    std::ostream* yycdebug_;

    /// \brief Display a symbol type, value and location.
    /// \param yyo    The output stream.
    /// \param yysym  The symbol.
    template <typename Base>
    void yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const;
#endif

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg     Why this token is reclaimed.
    ///                  If null, print nothing.
    /// \param s         The symbol.
    template <typename Base>
    void yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const;

  private:
    /// Type access provider for state based symbols.
    struct by_state
    {
      /// Default constructor.
      by_state ();

      /// The symbol type as needed by the constructor.
      typedef state_type kind_type;

      /// Constructor.
      by_state (kind_type s);

      /// Copy constructor.
      by_state (const by_state& other);

      /// Steal the symbol type from \a that.
      void move (by_state& that);

      /// The (internal) type number (corresponding to \a state).
      /// "empty" when empty.
      symbol_number_type type_get () const;

      enum { empty = 0 };

      /// The state.
      state_type state;
    };

    /// "Internal" symbol: element of the stack.
    struct stack_symbol_type : basic_symbol<by_state>
    {
      /// Superclass.
      typedef basic_symbol<by_state> super_type;
      /// Construct an empty symbol.
      stack_symbol_type ();
      /// Steal the contents from \a sym to build this.
      stack_symbol_type (state_type s, symbol_type& sym);
      /// Assignment, needed by push_back.
      stack_symbol_type& operator= (const stack_symbol_type& that);
    };

    /// Stack type.
    typedef stack<stack_symbol_type> stack_type;

    /// The stack.
    stack_type yystack_;

    /// Push a new state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the symbol
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, stack_symbol_type& s);

    /// Push a new look ahead token on the state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the state
    /// \param sym  the symbol (for its value and location).
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, state_type s, symbol_type& sym);

    /// Pop \a n symbols the three stacks.
    void yypop_ (unsigned int n = 1);

    // Constants.
    enum
    {
      yyeof_ = 0,
      yylast_ = 862,           //< Last index in yytable_.
      yynnts_ = 14,  //< Number of nonterminal symbols.
      yyempty_ = -2,
      yyfinal_ = 4, //< Termination state number.
      yyterror_ = 1,
      yyerrcode_ = 256,
      yyntokens_ = 67    //< Number of tokens.
    };


    // User arguments.
    Shape::ShapeParser::semantic_type *yylval;
    Shape::ShapeCompiler &compiler;
  };

  // Symbol number corresponding to token number t.
  inline
  ShapeParser::token_number_type
  ShapeParser::yytranslate_ (token_type t)
  {
    static
    const token_number_type
    translate_table[] =
    {
     0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    57,     2,     2,
       2,     2,     2,     2,     2,     2,    56,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    58,    59,    60,    61,    62,    63,    64,    65,    66
    };
    const unsigned int user_token_number_max_ = 319;
    const token_number_type undef_token_ = 2;

    if (static_cast<int>(t) <= yyeof_)
      return yyeof_;
    else if (static_cast<unsigned int> (t) <= user_token_number_max_)
      return translate_table[t];
    else
      return undef_token_;
  }

  inline
  ShapeParser::syntax_error::syntax_error (const location_type& l, const std::string& m)
    : std::runtime_error (m)
    , location (l)
  {}

  // basic_symbol.
  template <typename Base>
  inline
  ShapeParser::basic_symbol<Base>::basic_symbol ()
    : value ()
  {}

  template <typename Base>
  inline
  ShapeParser::basic_symbol<Base>::basic_symbol (const basic_symbol& other)
    : Base (other)
    , value ()
    , location (other.location)
  {
      switch (other.type_get ())
    {
      case 71: // ForeachOptionalArg
      case 75: // ForExpr
      case 76: // Expr
      case 77: // Value
      case 78: // Variable
      case 79: // AccessOperator
      case 80: // Index
        value.copy< AbstractExpressionPtr > (other.value);
        break;

      case 68: // Start
      case 69: // StatementList
      case 70: // Statement
      case 72: // ElseIfList
      case 73: // ElseSingle
        value.copy< AbstractOperatorPtr > (other.value);
        break;

      case 74: // FunctionCallParameterList
        value.copy< ArgumentList > (other.value);
        break;

      case 3: // "(Real)"
        value.copy< float > (other.value);
        break;

      case 4: // "(Integer)"
        value.copy< int > (other.value);
        break;

      case 5: // "(String)"
      case 6: // "(Identifier)"
      case 51: // "(Content)"
        value.copy< std::string > (other.value);
        break;

      default:
        break;
    }

  }


  template <typename Base>
  inline
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const semantic_type& v, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {
    (void) v;
      switch (this->type_get ())
    {
      case 71: // ForeachOptionalArg
      case 75: // ForExpr
      case 76: // Expr
      case 77: // Value
      case 78: // Variable
      case 79: // AccessOperator
      case 80: // Index
        value.copy< AbstractExpressionPtr > (v);
        break;

      case 68: // Start
      case 69: // StatementList
      case 70: // Statement
      case 72: // ElseIfList
      case 73: // ElseSingle
        value.copy< AbstractOperatorPtr > (v);
        break;

      case 74: // FunctionCallParameterList
        value.copy< ArgumentList > (v);
        break;

      case 3: // "(Real)"
        value.copy< float > (v);
        break;

      case 4: // "(Integer)"
        value.copy< int > (v);
        break;

      case 5: // "(String)"
      case 6: // "(Identifier)"
      case 51: // "(Content)"
        value.copy< std::string > (v);
        break;

      default:
        break;
    }
}


  // Implementation of basic_symbol constructor for each type.

  template <typename Base>
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const location_type& l)
    : Base (t)
    , value ()
    , location (l)
  {}

  template <typename Base>
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const AbstractExpressionPtr v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const AbstractOperatorPtr v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const ArgumentList v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const float v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const int v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}

  template <typename Base>
  ShapeParser::basic_symbol<Base>::basic_symbol (typename Base::kind_type t, const std::string v, const location_type& l)
    : Base (t)
    , value (v)
    , location (l)
  {}


  template <typename Base>
  inline
  ShapeParser::basic_symbol<Base>::~basic_symbol ()
  {
    // User destructor.
    symbol_number_type yytype = this->type_get ();
    switch (yytype)
    {
   default:
      break;
    }

    // Type destructor.
    switch (yytype)
    {
      case 71: // ForeachOptionalArg
      case 75: // ForExpr
      case 76: // Expr
      case 77: // Value
      case 78: // Variable
      case 79: // AccessOperator
      case 80: // Index
        value.template destroy< AbstractExpressionPtr > ();
        break;

      case 68: // Start
      case 69: // StatementList
      case 70: // Statement
      case 72: // ElseIfList
      case 73: // ElseSingle
        value.template destroy< AbstractOperatorPtr > ();
        break;

      case 74: // FunctionCallParameterList
        value.template destroy< ArgumentList > ();
        break;

      case 3: // "(Real)"
        value.template destroy< float > ();
        break;

      case 4: // "(Integer)"
        value.template destroy< int > ();
        break;

      case 5: // "(String)"
      case 6: // "(Identifier)"
      case 51: // "(Content)"
        value.template destroy< std::string > ();
        break;

      default:
        break;
    }

  }

  template <typename Base>
  inline
  void
  ShapeParser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move(s);
      switch (this->type_get ())
    {
      case 71: // ForeachOptionalArg
      case 75: // ForExpr
      case 76: // Expr
      case 77: // Value
      case 78: // Variable
      case 79: // AccessOperator
      case 80: // Index
        value.move< AbstractExpressionPtr > (s.value);
        break;

      case 68: // Start
      case 69: // StatementList
      case 70: // Statement
      case 72: // ElseIfList
      case 73: // ElseSingle
        value.move< AbstractOperatorPtr > (s.value);
        break;

      case 74: // FunctionCallParameterList
        value.move< ArgumentList > (s.value);
        break;

      case 3: // "(Real)"
        value.move< float > (s.value);
        break;

      case 4: // "(Integer)"
        value.move< int > (s.value);
        break;

      case 5: // "(String)"
      case 6: // "(Identifier)"
      case 51: // "(Content)"
        value.move< std::string > (s.value);
        break;

      default:
        break;
    }

    location = s.location;
  }

  // by_type.
  inline
  ShapeParser::by_type::by_type ()
     : type (empty)
  {}

  inline
  ShapeParser::by_type::by_type (const by_type& other)
    : type (other.type)
  {}

  inline
  ShapeParser::by_type::by_type (token_type t)
    : type (yytranslate_ (t))
  {}

  inline
  void
  ShapeParser::by_type::move (by_type& that)
  {
    type = that.type;
    that.type = empty;
  }

  inline
  int
  ShapeParser::by_type::type_get () const
  {
    return type;
  }

  inline
  ShapeParser::token_type
  ShapeParser::by_type::token () const
  {
    // YYTOKNUM[NUM] -- (External) token number corresponding to the
    // (internal) symbol number NUM (which must be that of a token).  */
    static
    const unsigned short int
    yytoken_number_[] =
    {
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,    46,    37,   311,   312,
     313,   314,   315,   316,   317,   318,   319
    };
    return static_cast<token_type> (yytoken_number_[type]);
  }
  // Implementation of make_symbol for each symbol type.
  ShapeParser::symbol_type
  ShapeParser::make_End (const location_type& l)
  {
    return symbol_type (token::End, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Real (const float& v, const location_type& l)
  {
    return symbol_type (token::Real, v, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Integer (const int& v, const location_type& l)
  {
    return symbol_type (token::Integer, v, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_String (const std::string& v, const location_type& l)
  {
    return symbol_type (token::String, v, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Identifier (const std::string& v, const location_type& l)
  {
    return symbol_type (token::Identifier, v, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_True (const location_type& l)
  {
    return symbol_type (token::True, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_False (const location_type& l)
  {
    return symbol_type (token::False, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_OpenCurlyBrace (const location_type& l)
  {
    return symbol_type (token::OpenCurlyBrace, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_CloseCurlyBrace (const location_type& l)
  {
    return symbol_type (token::CloseCurlyBrace, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_OpenBrace (const location_type& l)
  {
    return symbol_type (token::OpenBrace, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_CloseBrace (const location_type& l)
  {
    return symbol_type (token::CloseBrace, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_OpenBracket (const location_type& l)
  {
    return symbol_type (token::OpenBracket, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_CloseBracket (const location_type& l)
  {
    return symbol_type (token::CloseBracket, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_If (const location_type& l)
  {
    return symbol_type (token::If, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_ElseIf (const location_type& l)
  {
    return symbol_type (token::ElseIf, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Else (const location_type& l)
  {
    return symbol_type (token::Else, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_EndIf (const location_type& l)
  {
    return symbol_type (token::EndIf, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_While (const location_type& l)
  {
    return symbol_type (token::While, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_EndWhile (const location_type& l)
  {
    return symbol_type (token::EndWhile, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Do (const location_type& l)
  {
    return symbol_type (token::Do, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_For (const location_type& l)
  {
    return symbol_type (token::For, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_EndFor (const location_type& l)
  {
    return symbol_type (token::EndFor, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Foreach (const location_type& l)
  {
    return symbol_type (token::Foreach, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_EndForeach (const location_type& l)
  {
    return symbol_type (token::EndForeach, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Dollar (const location_type& l)
  {
    return symbol_type (token::Dollar, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Sharp (const location_type& l)
  {
    return symbol_type (token::Sharp, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_At (const location_type& l)
  {
    return symbol_type (token::At, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_LT (const location_type& l)
  {
    return symbol_type (token::LT, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_GT (const location_type& l)
  {
    return symbol_type (token::GT, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_EQ (const location_type& l)
  {
    return symbol_type (token::EQ, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_NE (const location_type& l)
  {
    return symbol_type (token::NE, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_LE (const location_type& l)
  {
    return symbol_type (token::LE, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_GE (const location_type& l)
  {
    return symbol_type (token::GE, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Not (const location_type& l)
  {
    return symbol_type (token::Not, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Plus (const location_type& l)
  {
    return symbol_type (token::Plus, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_PlusPlus (const location_type& l)
  {
    return symbol_type (token::PlusPlus, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Minus (const location_type& l)
  {
    return symbol_type (token::Minus, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_MinusMinus (const location_type& l)
  {
    return symbol_type (token::MinusMinus, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Multiply (const location_type& l)
  {
    return symbol_type (token::Multiply, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Division (const location_type& l)
  {
    return symbol_type (token::Division, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_PlusAssign (const location_type& l)
  {
    return symbol_type (token::PlusAssign, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_MinusAssign (const location_type& l)
  {
    return symbol_type (token::MinusAssign, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_MultiplyAssign (const location_type& l)
  {
    return symbol_type (token::MultiplyAssign, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_DivisionAssign (const location_type& l)
  {
    return symbol_type (token::DivisionAssign, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Comma (const location_type& l)
  {
    return symbol_type (token::Comma, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Break (const location_type& l)
  {
    return symbol_type (token::Break, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Continue (const location_type& l)
  {
    return symbol_type (token::Continue, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Echo (const location_type& l)
  {
    return symbol_type (token::Echo, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Comment (const location_type& l)
  {
    return symbol_type (token::Comment, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Content (const std::string& v, const location_type& l)
  {
    return symbol_type (token::Content, v, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Semicolon (const location_type& l)
  {
    return symbol_type (token::Semicolon, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Assign (const location_type& l)
  {
    return symbol_type (token::Assign, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Or (const location_type& l)
  {
    return symbol_type (token::Or, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_And (const location_type& l)
  {
    return symbol_type (token::And, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Inc (const location_type& l)
  {
    return symbol_type (token::Inc, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_IntegerCast (const location_type& l)
  {
    return symbol_type (token::IntegerCast, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_RealCast (const location_type& l)
  {
    return symbol_type (token::RealCast, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_StringCast (const location_type& l)
  {
    return symbol_type (token::StringCast, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_ArrayCast (const location_type& l)
  {
    return symbol_type (token::ArrayCast, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_BooleanCast (const location_type& l)
  {
    return symbol_type (token::BooleanCast, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_As (const location_type& l)
  {
    return symbol_type (token::As, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Arrow (const location_type& l)
  {
    return symbol_type (token::Arrow, l);

  }

  ShapeParser::symbol_type
  ShapeParser::make_Dot (const location_type& l)
  {
    return symbol_type (token::Dot, l);

  }


#line 6 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:378
} // Shape
#line 1743 "Shape/include/Shape/ShapeParser/ShapeParser.hpp" // lalr1.m4:378




#endif // !YY_YY_SHAPE_INCLUDE_SHAPE_SHAPEPARSER_SHAPEPARSER_HPP_INCLUDED

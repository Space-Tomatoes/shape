#include <Shape/ShapeCompiler/ShapeCompiler.hpp>

#include <Windows.h>

namespace Shape
{	
	/// \brief Constructor.
	ShapeCompiler::ShapeCompiler() :
		traceScanning(false),
		traceParsing(false)		
	{ }

	/// \brief Destructor.
	ShapeCompiler::~ShapeCompiler()
	{ }

	/// \brief Parses Shape file.
	/// \param [in] fileName File name.
	std::string ShapeCompiler::ParseFile(const std::string &fileName)
	{
		std::string data;

		mode = File;

		this->fileName = fileName;
		
		auto check = fopen(fileName.c_str(), "r");

		if (check)
		{		
			fclose(check);

			ScanBegin();//?
			{
				Shape::ShapeParser parser(&yylval, *this);
				parser.set_debug_level(traceParsing);
				parser.parse();
			}
			ScanEnd();//?

			data = treeRoot->Execute().ToString();
		}

		return data;
	}

	/// \brief Parses Shape script.
	/// \param [in] data Shape script data.
	std::string ShapeCompiler::Parse(const std::string &data)
	{ 
		std::string result;
		mode = Buffer;
		this->buffer = data;
		
		ScanBegin();//?
		{
			Shape::ShapeParser parser(&yylval, *this);
			parser.set_debug_level(traceParsing);
			parser.parse();
		}
		ScanEnd();//?

		result = treeRoot->Execute().ToString();

		return result;
	}

	/// \brief OnError callback.
	/// \param [in] location Location at the Shape script where error occurs.
	/// \param [in] message Error message text.
	void ShapeCompiler::Error(const Shape::location &location, const std::string &message)
	{ 
		std::cerr << location << ": " << message << std::endl;
	}

	/// \brief OnError callback.
	/// \param [in] message Error message text.
	void ShapeCompiler::Error(const std::string &message)
	{ 
		std::cerr << message << std::endl;
	}

	std::string ShapeCompiler::GetFileName() const
	{
		return this->fileName;
	}

	/// \brief Gets trace scanning option value.
	/// \return Trace scanning option value.
	bool ShapeCompiler::GetTraceScanning() const
	{
		return this->traceScanning;
	}

	/// \brief Enables or Disables traces of scanning.
	/// \param traceScanning 
	void ShapeCompiler::SetTraceScanning(bool traceScanning)
	{
		this->traceScanning = traceScanning;
	}

	/// \brief Gets trace parsing option value.
	/// \return Trace parsing option value.
	bool ShapeCompiler::GetTraceParsing() const
	{
		return this->traceParsing;
	}

	/// \brief Enables or Disables traces of parsing.
	/// \param traceParsing 
	void ShapeCompiler::SetTraceParsing(bool traceParsing)
	{
		this->traceParsing = traceParsing;
	}
} // namespace Shape
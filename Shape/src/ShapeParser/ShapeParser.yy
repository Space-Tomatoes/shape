%skeleton "lalr1.cc"
%language "C++"
%require "3.0"

%defines
%define api.namespace {Shape}
%define parser_class_name {ShapeParser}
%define api.token.constructor
%define api.value.type variant

%expect 76

%code requires
{
	#pragma warning(push)
	#pragma warning(disable: 4005 4065)

	#include <string>
	#include <memory>

	#include <Tokens.hpp>

	#define YYERROR_VERBOSE		(1)
	#define YYDEBUG				(1)
	
	namespace Shape
	{
		class ShapeCompiler;
	} // namespace Shape
}

%param { Shape::ShapeParser::semantic_type *yylval }
%param { Shape::ShapeCompiler &compiler }


%locations
%initial-action
{
	auto t = @$;

	@$.begin.filename = @$.end.filename = &compiler.GetFileName();
};

%define parse.trace
%define parse.error verbose

%code
{
	#include <Shape/ShapeCompiler/ShapeCompiler.hpp>
}

%token	<float>					Real				"(Real)"
%token	<int>					Integer				"(Integer)"
%token	<std::string>			String				"(String)"
%token	<std::string>			Identifier			"(Identifier)"
%token							True				"(True)"
%token							False				"(False)"
%token							OpenCurlyBrace		"{ (OpenCurlyBrace)"
%token							CloseCurlyBrace		"} (CloseCurlyBrace)"
%token							OpenBrace			"( (OpenBrace)"
%token							CloseBrace			") (CloseBrace)"
%token							OpenBracket			"] (OpenBracket)"
%token							CloseBracket		"[ (CloseBracket)"
%token							If					"if (If)"
%left							ElseIf
%token							ElseIf				"elseif (ElseIf)"
%left							Else
%token							Else				"else (Else)"
%left							EndIf
%token							EndIf				"/if (EndIf)"
%token							While				"while (While)"
%token							EndWhile			"/while (EndWhile)"
%token							Do					"do (Do)"
%token							For					"for (For)"
%token							EndFor				"/for (EndFor)"
%token							Foreach             "foreach (Foreach)"
%token							EndForeach          "/foreach (EndForeach)"
%token							Dollar				"$ (Dollar)"
%token							Sharp				"# (Sharp)"
%token							At					"@ (At)"
%token							LT					"< (LT)"
%token							GT					"> (GT)"
%token							EQ					"== (EQ)"
%token							NE					"!= (NE)"
%token							LE					"<= (LE)"
%token							GE					">= (GE)"
%token							Not					"! (Not)"
%token							Plus   				"+ (Plus)"
%token							PlusPlus			"++ (PlusPlus)"
%token							Minus				"- (Minus)"
%token							MinusMinus			"-- (MinusMinus)"
%token							Multiply			"* (Multiply)"
%token							Division			"/ (Division)"
%token							PlusAssign   		"+= (PlusAssign)"
%token							MinusAssign			"-= (MinusAssign)"
%token							MultiplyAssign		"*= (MultiplyAssign)"
%token							DivisionAssign		"/= (DivisionAssign)"
%token							Comma				", (Comma)"
%token							Break				"break (Break)"
%token							Continue			"continue (Continue)"
%token							Echo				"echo (Echo)"
%token							Comment				"comment (Comment)"
%token	<std::string>			Content				"(Content)"
%token							End			0		"End of File (End)"
%token							Semicolon			"; (Semicolon)"

%type	<AbstractOperatorPtr>	Start StatementList Statement ElseIfList ElseSingle
%type	<AbstractExpressionPtr>	Variable Value Expr ForExpr Index AccessOperator ForeachOptionalArg
%type	<ArgumentList>			FunctionCallParameterList

%token Assign

%left Comma
%right Echo     
%left Assign
%left Or
%token Or  
%left And 
%token And 
%nonassoc EQ NE
%nonassoc LT LE GT GE
%left Plus Minus '.'
%left Multiply Division '%'
%right Not
%right Inc IntegerCast RealCast StringCast ArrayCast BooleanCast
%token IntegerCast    
%token RealCast 
%token StringCast 
%token BooleanCast
%token As        
%token Arrow
%token Dot

%start Start

/*%printer { yyoutput << $$; } <*>;*/

%%

Start:							StatementList				
								{ 
									$$ = $1;
									compiler.treeRoot = $$;
								}
;

StatementList:					%empty						{ $$ = std::make_shared<BlockOperator>(); $$->SetLocation(yylhs.location); }
|								StatementList Statement		{ $$ = std::make_shared<BlockOperator>($1, $2); $$->SetLocation(yylhs.location); }										
|								error						{ $$ = std::make_shared<BlockOperator>(); $$->SetLocation(yylhs.location); }
;

Statement:						If Expr StatementList ElseIfList ElseSingle EndIf
								{
									auto sl = $StatementList;
									auto eil = $ElseIfList;
									auto es = $ElseSingle;

									$$ = std::make_shared<ConditionOperator>($Expr, $StatementList, $ElseSingle);
									$$->SetLocation(yylhs.location);
								}
|								While Expr StatementList EndWhile
								{
									$$ = std::make_shared<LoopOperator>($Expr, $StatementList);
									$$->SetLocation(yylhs.location);
								}
|								Do Statement While OpenBrace Expr CloseBrace
								{
									$$ = std::make_shared<BlockOperator>();
									$$->SetLocation(yylhs.location);
								}
|								For ForExpr Semicolon ForExpr Semicolon ForExpr StatementList EndFor
								{
									$$ = std::make_shared<LoopOperator>($2, $4, $6, $StatementList);
									$$->SetLocation(yylhs.location);
								}
|								Break			
								{
									$$ = std::make_shared<BlockOperator>();
									$$->SetLocation(yylhs.location);
								}
|								Continue
								{
									$$ = std::make_shared<BlockOperator>();
									$$->SetLocation(yylhs.location);
								}
|								Content
								{
									$$ = std::make_shared<EchoOperator>(std::make_shared<Value>($Content));
									$$->SetLocation(yylhs.location);
								}
|								Echo Expr
								{
									$$ = std::make_shared<EchoOperator>($Expr);
									$$->SetLocation(yylhs.location);
								}
|								Expr
								{
									$$ = std::make_shared<EchoOperator>($Expr);
									$$->SetLocation(yylhs.location);
								}
|								Foreach Variable As Variable ForeachOptionalArg StatementList EndForeach
								{
									$$ = std::make_shared<ForeachLoopOperator>(compiler.GetController(), $2, $4, $ForeachOptionalArg, $StatementList);
									$$->SetLocation(yylhs.location);
								}
;

ForeachOptionalArg:				%empty											{ $$ = std::make_shared<Value>(true); $$->SetLocation(yylhs.location); }
|								Arrow Variable									{ $$ = $Variable; }
;

ElseIfList:						%empty											{ $$ = std::make_shared<ConditionOperator>(); $$->SetLocation(yylhs.location); }
|								ElseIfList ElseIf Expr StatementList
								{
									auto eil = $1;
									auto exp = $Expr;
									auto sl = $StatementList;

									$$ = std::make_shared<ConditionOperator>($Expr, $StatementList, std::make_shared<ConditionOperator>());
									$$->SetLocation(yylhs.location);
								}
;

ElseSingle:						%empty											{ $$ = std::make_shared<ConditionOperator>(); $$->SetLocation(yylhs.location); }
|								Else StatementList								{ $$ = $StatementList; }
;

FunctionCallParameterList:		%empty											{ $$.clear(); }
|								Expr											{ $$.clear(); $$.push_back($Expr); }
|								FunctionCallParameterList Comma Expr			{ $$ = $1; $$.push_back($Expr); }
;

ForExpr:						%empty											{ $$ = std::make_shared<Value>(true); $$->SetLocation(yylhs.location); }
|								Expr											{ $$ = $Expr; }
;

Expr:							Identifier										{ $$ = std::make_shared<VariableExpression>(compiler.GetController(), "const", $Identifier); $$->SetLocation(yylhs.location); }
|								Variable										{ $$ = $Variable; }
|								Variable Assign Expr							{ $$ = std::make_shared<AssignExpression>(compiler.GetController(), "=", $1, $3); $$->SetLocation(yylhs.location); }
|								Variable PlusAssign Expr						{ $$ = std::make_shared<AssignExpression>(compiler.GetController(), "+=", $1, $3); $$->SetLocation(yylhs.location); }
|								Variable MinusAssign Expr						{ $$ = std::make_shared<AssignExpression>(compiler.GetController(), "-=", $1, $3); $$->SetLocation(yylhs.location); }
|								Variable MultiplyAssign Expr					{ $$ = std::make_shared<AssignExpression>(compiler.GetController(), "*=", $1, $3); $$->SetLocation(yylhs.location); }
|								Variable DivisionAssign Expr					{ $$ = std::make_shared<AssignExpression>(compiler.GetController(), "/=", $1, $3); $$->SetLocation(yylhs.location); }
|								Variable PlusPlus %prec Inc						{ $$ = std::make_shared<AssignExpression>(compiler.GetController(), "++", $1); $$->SetLocation(yylhs.location); }
|								Variable MinusMinus %prec Inc					{ $$ = std::make_shared<AssignExpression>(compiler.GetController(), "--", $1); $$->SetLocation(yylhs.location); }
|								Expr Or Expr									{ $$ = std::make_shared<BinaryExpression>("||", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr And Expr									{ $$ = std::make_shared<BinaryExpression>("&&", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr Plus Expr									{ $$ = std::make_shared<BinaryExpression>("+", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr Minus Expr									{ $$ = std::make_shared<BinaryExpression>("-", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr Multiply Expr								{ $$ = std::make_shared<BinaryExpression>("*", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr Division Expr								{ $$ = std::make_shared<BinaryExpression>("/", $1, $3); $$->SetLocation(yylhs.location); }
|								Plus Expr %prec Inc								{ $$ = std::make_shared<UnaryExpression>("+", $2); $$->SetLocation(yylhs.location); }
|								Minus Expr %prec Inc							{ $$ = std::make_shared<UnaryExpression>("-", $2); $$->SetLocation(yylhs.location); }
|								Not Expr										{ $$ = std::make_shared<UnaryExpression>("!", $2); $$->SetLocation(yylhs.location); }
|								Expr EQ Expr									{ $$ = std::make_shared<BinaryExpression>("==", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr NE Expr 									{ $$ = std::make_shared<BinaryExpression>("!=", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr LT Expr 									{ $$ = std::make_shared<BinaryExpression>("<", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr LE Expr									{ $$ = std::make_shared<BinaryExpression>("<=", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr GT Expr 									{ $$ = std::make_shared<BinaryExpression>(">", $1, $3); $$->SetLocation(yylhs.location); }
|								Expr GE Expr 									{ $$ = std::make_shared<BinaryExpression>(">=", $1, $3); $$->SetLocation(yylhs.location); }
|								OpenBrace Expr CloseBrace						{ $$ = $2; }
|								IntegerCast Expr 								{ $$ = std::make_shared<UnaryExpression>("Integer", $2); $$->SetLocation(yylhs.location); }
|								RealCast Expr 									{ $$ = std::make_shared<UnaryExpression>("Real", $2); $$->SetLocation(yylhs.location); }
|								StringCast Expr									{ $$ = std::make_shared<UnaryExpression>("String", $2); $$->SetLocation(yylhs.location); }
|								BooleanCast Expr								{ $$ = std::make_shared<UnaryExpression>("Boolean", $2); $$->SetLocation(yylhs.location); }
|								Value											{ $$ = $Value; }
|								Identifier OpenBrace FunctionCallParameterList CloseBrace	
								{ $$ = std::make_shared<FunctionCallExpression>(compiler.GetController(), $1, $3); $$->SetLocation(yylhs.location); }
;

Value:							Integer											{ $$ = std::make_shared<Value>($1); $$->SetLocation(yylhs.location); }
|								Real											{ $$ = std::make_shared<Value>($1); $$->SetLocation(yylhs.location); }
|								True											{ $$ = std::make_shared<Value>(true); $$->SetLocation(yylhs.location); }
|								False											{ $$ = std::make_shared<Value>(false); $$->SetLocation(yylhs.location); }
|								String											{ $$ = std::make_shared<Value>($1); $$->SetLocation(yylhs.location); }
;

Variable:						Dollar Identifier AccessOperator				{ $$ = std::make_shared<VariableExpression>(compiler.GetController(), "$", $Identifier); $$->SetLocation(yylhs.location); }
|								Sharp Identifier AccessOperator					{ $$ = std::make_shared<VariableExpression>(compiler.GetController(), "#", $Identifier); $$->SetLocation(yylhs.location); }
|								At Identifier AccessOperator 					
								{ 
									$$ = std::make_shared<VariableExpression>(compiler.GetController(), "@", $Identifier);
									$$->SetLocation(yylhs.location);

									auto accOper = std::dynamic_pointer_cast<AccessOperator>($AccessOperator);

									if (accOper)
									{
										accOper->SetRoot($$);
										$$ = accOper;
									}
								}
;

AccessOperator:					%empty											{  }
|								AccessOperator Dot Identifier				
								{ 
									$$ = std::make_shared<AccessOperator>($1, std::make_shared<Value>($3));
									$$->SetLocation(yylhs.location);
								}
|								AccessOperator Index
								{
									$$ = std::make_shared<AccessOperator>($1, $2);
									$$->SetLocation(yylhs.location);
								}
;

Index:							OpenBracket Expr CloseBracket					{ $$ = $Expr; }
;

%%

void Shape::ShapeParser::error(const location_type &location, const std::string &message)
{
	if (compiler.GetController() != nullptr)
		compiler.GetController()->EmitErrorHandler(location, message);
}

#pragma warning(pop)
// A Bison parser, made by GNU Bison 3.0.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2013 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.

#line 37 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:405

# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

#include "ShapeParser.hpp"

// User implementation prologue.

#line 51 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:413
// Unqualified %code blocks.
#line 48 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:414

	#include <Shape/ShapeCompiler/ShapeCompiler.hpp>

#line 57 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:414


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyempty = true)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

#line 6 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:480
namespace Shape {
#line 143 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:480

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  ShapeParser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              // Fall through.
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }


  /// Build a parser object.
  ShapeParser::ShapeParser (Shape::ShapeParser::semantic_type *yylval_yyarg, Shape::ShapeCompiler &compiler_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      yylval (yylval_yyarg),
      compiler (compiler_yyarg)
  {}

  ShapeParser::~ShapeParser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
  ShapeParser::by_state::by_state ()
    : state (empty)
  {}

  inline
  ShapeParser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  ShapeParser::by_state::move (by_state& that)
  {
    state = that.state;
    that.state = empty;
  }

  inline
  ShapeParser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  ShapeParser::symbol_number_type
  ShapeParser::by_state::type_get () const
  {
    return state == empty ? 0 : yystos_[state];
  }

  inline
  ShapeParser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  ShapeParser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 71: // ForeachOptionalArg
      case 75: // ForExpr
      case 76: // Expr
      case 77: // Value
      case 78: // Variable
      case 79: // AccessOperator
      case 80: // Index
        value.move< AbstractExpressionPtr > (that.value);
        break;

      case 68: // Start
      case 69: // StatementList
      case 70: // Statement
      case 72: // ElseIfList
      case 73: // ElseSingle
        value.move< AbstractOperatorPtr > (that.value);
        break;

      case 74: // FunctionCallParameterList
        value.move< ArgumentList > (that.value);
        break;

      case 3: // "(Real)"
        value.move< float > (that.value);
        break;

      case 4: // "(Integer)"
        value.move< int > (that.value);
        break;

      case 5: // "(String)"
      case 6: // "(Identifier)"
      case 51: // "(Content)"
        value.move< std::string > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty;
  }

  inline
  ShapeParser::stack_symbol_type&
  ShapeParser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 71: // ForeachOptionalArg
      case 75: // ForExpr
      case 76: // Expr
      case 77: // Value
      case 78: // Variable
      case 79: // AccessOperator
      case 80: // Index
        value.copy< AbstractExpressionPtr > (that.value);
        break;

      case 68: // Start
      case 69: // StatementList
      case 70: // Statement
      case 72: // ElseIfList
      case 73: // ElseSingle
        value.copy< AbstractOperatorPtr > (that.value);
        break;

      case 74: // FunctionCallParameterList
        value.copy< ArgumentList > (that.value);
        break;

      case 3: // "(Real)"
        value.copy< float > (that.value);
        break;

      case 4: // "(Integer)"
        value.copy< int > (that.value);
        break;

      case 5: // "(String)"
      case 6: // "(Identifier)"
      case 51: // "(Content)"
        value.copy< std::string > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
  ShapeParser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  ShapeParser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  inline
  void
  ShapeParser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  ShapeParser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  ShapeParser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  ShapeParser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  ShapeParser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  ShapeParser::debug_level_type
  ShapeParser::debug_level () const
  {
    return yydebug_;
  }

  void
  ShapeParser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline ShapeParser::state_type
  ShapeParser::yy_lr_goto_state_ (state_type yystate, int yylhs)
  {
    int yyr = yypgoto_[yylhs - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yylhs - yyntokens_];
  }

  inline bool
  ShapeParser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  ShapeParser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  ShapeParser::parse ()
  {
    /// Whether yyla contains a lookahead.
    bool yyempty = true;

    // State.
    int yyn;
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// $$ and @$.
    stack_symbol_type yylhs;

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    // User initialization code.
    #line 38 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:733
{
	auto t = yyla.location;

	yyla.location.begin.filename = yyla.location.end.filename = &compiler.GetFileName();
}

#line 482 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:733

    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULL, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyempty)
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex (yylval, compiler));
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
        yyempty = false;
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Discard the token being shifted.
    yyempty = true;

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
    /* Variants are always initialized to an empty instance of the
       correct type. The default $$=$1 action is NOT applied when using
       variants.  */
      switch (yyr1_[yyn])
    {
      case 71: // ForeachOptionalArg
      case 75: // ForExpr
      case 76: // Expr
      case 77: // Value
      case 78: // Variable
      case 79: // AccessOperator
      case 80: // Index
        yylhs.value.build< AbstractExpressionPtr > ();
        break;

      case 68: // Start
      case 69: // StatementList
      case 70: // Statement
      case 72: // ElseIfList
      case 73: // ElseSingle
        yylhs.value.build< AbstractOperatorPtr > ();
        break;

      case 74: // FunctionCallParameterList
        yylhs.value.build< ArgumentList > ();
        break;

      case 3: // "(Real)"
        yylhs.value.build< float > ();
        break;

      case 4: // "(Integer)"
        yylhs.value.build< int > ();
        break;

      case 5: // "(String)"
      case 6: // "(Identifier)"
      case 51: // "(Content)"
        yylhs.value.build< std::string > ();
        break;

      default:
        break;
    }


    // Compute the default @$.
    {
      slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
      YYLLOC_DEFAULT (yylhs.location, slice, yylen);
    }

    // Perform the reduction.
    YY_REDUCE_PRINT (yyn);
    try
      {
        switch (yyn)
          {
  case 2:
#line 141 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { 
									yylhs.value.as< AbstractOperatorPtr > () = yystack_[0].value.as< AbstractOperatorPtr > ();
									compiler.treeRoot = yylhs.value.as< AbstractOperatorPtr > ();
								}
#line 633 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 3:
#line 147 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<BlockOperator>(); yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location); }
#line 639 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 4:
#line 148 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<BlockOperator>(yystack_[1].value.as< AbstractOperatorPtr > (), yystack_[0].value.as< AbstractOperatorPtr > ()); yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location); }
#line 645 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 5:
#line 149 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<BlockOperator>(); yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location); }
#line 651 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 6:
#line 153 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									auto sl = yystack_[3].value.as< AbstractOperatorPtr > ();
									auto eil = yystack_[2].value.as< AbstractOperatorPtr > ();
									auto es = yystack_[1].value.as< AbstractOperatorPtr > ();

									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<ConditionOperator>(yystack_[4].value.as< AbstractExpressionPtr > (), yystack_[3].value.as< AbstractOperatorPtr > (), yystack_[1].value.as< AbstractOperatorPtr > ());
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 664 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 7:
#line 162 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<LoopOperator>(yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[1].value.as< AbstractOperatorPtr > ());
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 673 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 8:
#line 167 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<BlockOperator>();
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 682 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 9:
#line 172 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<LoopOperator>(yystack_[6].value.as< AbstractExpressionPtr > (), yystack_[4].value.as< AbstractExpressionPtr > (), yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[1].value.as< AbstractOperatorPtr > ());
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 691 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 10:
#line 177 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<BlockOperator>();
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 700 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 11:
#line 182 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<BlockOperator>();
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 709 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 12:
#line 187 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<EchoOperator>(std::make_shared<Value>(yystack_[0].value.as< std::string > ()));
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 718 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 13:
#line 192 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<EchoOperator>(yystack_[0].value.as< AbstractExpressionPtr > ());
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 727 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 14:
#line 197 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<EchoOperator>(yystack_[0].value.as< AbstractExpressionPtr > ());
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 736 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 15:
#line 202 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<ForeachLoopOperator>(compiler.GetController(), yystack_[5].value.as< AbstractExpressionPtr > (), yystack_[3].value.as< AbstractExpressionPtr > (), yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[1].value.as< AbstractOperatorPtr > ());
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 745 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 16:
#line 208 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<Value>(true); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 751 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 17:
#line 209 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = yystack_[0].value.as< AbstractExpressionPtr > (); }
#line 757 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 18:
#line 212 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<ConditionOperator>(); yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location); }
#line 763 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 19:
#line 214 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									auto eil = yystack_[3].value.as< AbstractOperatorPtr > ();
									auto exp = yystack_[1].value.as< AbstractExpressionPtr > ();
									auto sl = yystack_[0].value.as< AbstractOperatorPtr > ();

									yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<ConditionOperator>(yystack_[1].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractOperatorPtr > (), std::make_shared<ConditionOperator>());
									yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location);
								}
#line 776 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 20:
#line 224 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractOperatorPtr > () = std::make_shared<ConditionOperator>(); yylhs.value.as< AbstractOperatorPtr > ()->SetLocation(yylhs.location); }
#line 782 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 21:
#line 225 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractOperatorPtr > () = yystack_[0].value.as< AbstractOperatorPtr > (); }
#line 788 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 22:
#line 228 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< ArgumentList > ().clear(); }
#line 794 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 23:
#line 229 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< ArgumentList > ().clear(); yylhs.value.as< ArgumentList > ().push_back(yystack_[0].value.as< AbstractExpressionPtr > ()); }
#line 800 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 24:
#line 230 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< ArgumentList > () = yystack_[2].value.as< ArgumentList > (); yylhs.value.as< ArgumentList > ().push_back(yystack_[0].value.as< AbstractExpressionPtr > ()); }
#line 806 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 25:
#line 233 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<Value>(true); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 812 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 26:
#line 234 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = yystack_[0].value.as< AbstractExpressionPtr > (); }
#line 818 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 27:
#line 237 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<VariableExpression>(compiler.GetController(), "const", yystack_[0].value.as< std::string > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 824 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 28:
#line 238 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = yystack_[0].value.as< AbstractExpressionPtr > (); }
#line 830 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 29:
#line 239 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AssignExpression>(compiler.GetController(), "=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 836 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 30:
#line 240 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AssignExpression>(compiler.GetController(), "+=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 842 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 31:
#line 241 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AssignExpression>(compiler.GetController(), "-=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 848 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 32:
#line 242 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AssignExpression>(compiler.GetController(), "*=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 854 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 33:
#line 243 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AssignExpression>(compiler.GetController(), "/=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 860 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 34:
#line 244 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AssignExpression>(compiler.GetController(), "++", yystack_[1].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 866 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 35:
#line 245 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AssignExpression>(compiler.GetController(), "--", yystack_[1].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 872 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 36:
#line 246 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("||", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 878 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 37:
#line 247 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("&&", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 884 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 38:
#line 248 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("+", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 890 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 39:
#line 249 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("-", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 896 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 40:
#line 250 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("*", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 902 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 41:
#line 251 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("/", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 908 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 42:
#line 252 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<UnaryExpression>("+", yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 914 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 43:
#line 253 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<UnaryExpression>("-", yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 920 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 44:
#line 254 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<UnaryExpression>("!", yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 926 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 45:
#line 255 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("==", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 932 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 46:
#line 256 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("!=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 938 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 47:
#line 257 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("<", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 944 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 48:
#line 258 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>("<=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 950 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 49:
#line 259 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>(">", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 956 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 50:
#line 260 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<BinaryExpression>(">=", yystack_[2].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 962 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 51:
#line 261 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = yystack_[1].value.as< AbstractExpressionPtr > (); }
#line 968 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 52:
#line 262 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<UnaryExpression>("Integer", yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 974 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 53:
#line 263 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<UnaryExpression>("Real", yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 980 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 54:
#line 264 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<UnaryExpression>("String", yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 986 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 55:
#line 265 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<UnaryExpression>("Boolean", yystack_[0].value.as< AbstractExpressionPtr > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 992 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 56:
#line 266 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = yystack_[0].value.as< AbstractExpressionPtr > (); }
#line 998 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 57:
#line 268 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<FunctionCallExpression>(compiler.GetController(), yystack_[3].value.as< std::string > (), yystack_[1].value.as< ArgumentList > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1004 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 58:
#line 271 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<Value>(yystack_[0].value.as< int > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1010 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 59:
#line 272 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<Value>(yystack_[0].value.as< float > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1016 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 60:
#line 273 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<Value>(true); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1022 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 61:
#line 274 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<Value>(false); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1028 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 62:
#line 275 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<Value>(yystack_[0].value.as< std::string > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1034 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 63:
#line 278 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<VariableExpression>(compiler.GetController(), "$", yystack_[1].value.as< std::string > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1040 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 64:
#line 279 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<VariableExpression>(compiler.GetController(), "#", yystack_[1].value.as< std::string > ()); yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location); }
#line 1046 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 65:
#line 281 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { 
									yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<VariableExpression>(compiler.GetController(), "@", yystack_[1].value.as< std::string > ());
									yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location);

									auto accOper = std::dynamic_pointer_cast<AccessOperator>(yystack_[0].value.as< AbstractExpressionPtr > ());

									if (accOper)
									{
										accOper->SetRoot(yylhs.value.as< AbstractExpressionPtr > ());
										yylhs.value.as< AbstractExpressionPtr > () = accOper;
									}
								}
#line 1063 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 66:
#line 295 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {  }
#line 1069 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 67:
#line 297 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { 
									yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AccessOperator>(yystack_[2].value.as< AbstractExpressionPtr > (), std::make_shared<Value>(yystack_[0].value.as< std::string > ()));
									yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location);
								}
#line 1078 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 68:
#line 302 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    {
									yylhs.value.as< AbstractExpressionPtr > () = std::make_shared<AccessOperator>(yystack_[1].value.as< AbstractExpressionPtr > (), yystack_[0].value.as< AbstractExpressionPtr > ());
									yylhs.value.as< AbstractExpressionPtr > ()->SetLocation(yylhs.location);
								}
#line 1087 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;

  case 69:
#line 308 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:853
    { yylhs.value.as< AbstractExpressionPtr > () = yystack_[1].value.as< AbstractExpressionPtr > (); }
#line 1093 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
    break;


#line 1097 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:853
          default:
            break;
          }
      }
    catch (const syntax_error& yyexc)
      {
        error (yyexc);
        YYERROR;
      }
    YY_SYMBOL_PRINT ("-> $$ =", yylhs);
    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();

    // Shift the result of the reduction.
    yypush_ (YY_NULL, yylhs);
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state,
                                           yyempty ? yyempty_ : yyla.type_get ()));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyempty)
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyempty = true;
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* $$ was initialized before running the user action.  */
    YY_SYMBOL_PRINT ("Error: discarding", yylhs);
    yylhs.~stack_symbol_type();
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyempty)
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyempty)
          yy_destroy_ (YY_NULL, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULL, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  ShapeParser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
  ShapeParser::yysyntax_error_ (state_type yystate, symbol_number_type yytoken) const
  {
    std::string yyres;
    // Number of reported tokens (one for the "unexpected", one per
    // "expected").
    size_t yycount = 0;
    // Its maximum.
    enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
    // Arguments of yyformat.
    char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];

    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yytoken) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state
         merging (from LALR or IELR) and default reductions corrupt the
         expected token list.  However, the list is correct for
         canonical LR with one exception: it will still contain any
         token that will not be accepted due to an error action in a
         later state.
    */
    if (yytoken != yyempty_)
      {
        yyarg[yycount++] = yytname_[yytoken];
        int yyn = yypact_[yystate];
        if (!yy_pact_value_is_default_ (yyn))
          {
            /* Start YYX at -YYN if negative to avoid negative indexes in
               YYCHECK.  In other words, skip the first -YYN actions for
               this state because they are default actions.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;
            // Stay within bounds of both yycheck and yytname.
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
              if (yycheck_[yyx + yyn] == yyx && yyx != yyterror_
                  && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
                {
                  if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                    {
                      yycount = 1;
                      break;
                    }
                  else
                    yyarg[yycount++] = yytname_[yyx];
                }
          }
      }

    char const* yyformat = YY_NULL;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
        YYCASE_(0, YY_("syntax error"));
        YYCASE_(1, YY_("syntax error, unexpected %s"));
        YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    // Argument number.
    size_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += yytnamerr_ (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const signed char ShapeParser::yypact_ninf_ = -55;

  const signed char ShapeParser::yytable_ninf_ = -4;

  const short int
  ShapeParser::yypact_[] =
  {
     106,   -55,    15,   655,   -55,   -55,   -55,   -55,    12,   -55,
     -55,   716,   716,   716,   655,   716,    -6,    22,    30,    32,
     716,   716,   716,   -55,   -55,   716,   -55,   716,   716,   716,
     716,   -55,    44,   -55,   692,   716,     1,   167,   228,    21,
      14,    44,     0,   -55,   -55,   -55,   -55,   -55,   -55,    44,
     -55,   -55,   -55,   -55,   716,   716,   716,   716,   716,   716,
     716,   716,   716,   716,   716,   716,   -55,   -55,   716,   716,
     716,   716,   716,    17,    44,   -55,   655,   472,    60,   716,
      -6,    -1,    -1,    -1,   821,   821,   794,   794,   821,   821,
     -33,   -33,   -55,   -55,   781,   808,    44,    44,    44,    44,
      44,   -55,   716,    10,   -55,   716,    20,    18,   716,    73,
     -55,    44,   716,   289,    69,   726,   716,    -6,   350,   754,
     -55,   167,   655,   -55,   -55,   411,   -55,   533,   -55,   655,
     594,   -55,   -55
  };

  const unsigned char
  ShapeParser::yydefact_[] =
  {
       0,     5,     0,     2,     1,    59,    58,    62,    27,    60,
      61,     0,     0,     0,     0,    25,     0,     0,     0,     0,
       0,     0,     0,    10,    11,     0,    12,     0,     0,     0,
       0,     4,    14,    56,    28,    22,     0,     0,     0,     0,
       0,    26,     0,    66,    66,    66,    44,    42,    43,    13,
      52,    53,    54,    55,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    34,    35,     0,     0,
       0,     0,     0,     0,    23,    51,    18,     0,     0,    25,
       0,    63,    64,    65,    47,    49,    45,    46,    48,    50,
      38,    39,    40,    41,    36,    37,    30,    31,    32,    33,
      29,    57,     0,    20,     7,     0,     0,    16,     0,     0,
      68,    24,     0,     0,     0,     0,    25,     0,     0,     0,
      67,     0,    21,     6,     8,     0,    17,     0,    69,    19,
       0,    15,     9
  };

  const signed char
  ShapeParser::yypgoto_[] =
  {
     -55,   -55,   -32,    74,   -55,   -55,   -55,   -55,   -54,   -11,
     -55,   -13,    25,   -55
  };

  const signed char
  ShapeParser::yydefgoto_[] =
  {
      -1,     2,     3,    31,   118,   103,   114,    73,    40,    32,
      33,    34,    81,   110
  };

  const short int
  ShapeParser::yytable_[] =
  {
      36,    37,    38,    42,    41,    76,    77,    62,    63,    46,
      47,    48,   108,    75,    49,     4,    50,    51,    52,    53,
      17,    18,    19,    35,    74,   106,   112,   113,    43,   101,
      54,    55,    56,    57,    58,    59,    44,    60,    45,    61,
      78,    62,    63,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    64,    65,    96,    97,    98,
      99,   100,   125,   102,    80,   109,    79,   107,    41,    82,
      83,   105,   116,    54,    55,    56,    57,    58,    59,   120,
      60,   122,    61,   117,    62,    63,   127,   123,    39,   129,
       0,   111,     0,   130,   115,     0,     0,   119,    64,    65,
       0,   121,     0,     0,   126,    41,    -3,     1,     0,    -3,
      -3,    -3,    -3,    -3,    -3,     0,     0,    -3,     0,     0,
       0,    -3,     0,     0,     0,    -3,     0,    -3,    -3,     0,
      -3,     0,    -3,    -3,    -3,     0,     0,     0,     0,     0,
       0,    -3,    -3,     0,    -3,     0,     0,     0,     0,     0,
       0,     0,     0,    -3,    -3,    -3,     0,    -3,     0,     0,
       0,     0,     0,     0,     0,    -3,    -3,    -3,     1,    -3,
      -3,    -3,    -3,    -3,    -3,    -3,     0,     0,    -3,     0,
       0,     0,    -3,    -3,    -3,    -3,    -3,     0,    -3,    -3,
       0,    -3,     0,    -3,    -3,    -3,    54,    55,    56,    57,
      58,    59,    -3,    60,     0,    61,     0,    62,    63,     0,
       0,     0,     0,     0,    -3,    -3,    -3,     0,    -3,     0,
       0,    64,    65,     0,     0,     0,    -3,    -3,    -3,     1,
      -3,    -3,    -3,    -3,    -3,    -3,    -3,     0,     0,    -3,
       0,     0,     0,    -3,     0,     0,     0,    -3,    -3,    -3,
      -3,     0,    -3,     0,    -3,    -3,    -3,    54,    55,    56,
      57,    58,    59,    -3,    60,     0,    61,     0,    62,    63,
       0,     0,     0,     0,     0,    -3,    -3,    -3,     0,    -3,
       0,     0,    64,    65,     0,     0,     0,    -3,    -3,    -3,
       1,    -3,    -3,    -3,    -3,    -3,    -3,    -3,     0,     0,
      -3,     0,     0,     0,    -3,     0,     0,    -3,    -3,     0,
      -3,    -3,     0,    -3,     0,    -3,    -3,    -3,     0,     0,
       0,     0,     0,     0,    -3,    -3,     0,    -3,     0,     0,
       0,     0,     0,     0,     0,     0,    -3,    -3,    -3,     0,
      -3,     0,     0,     0,     0,     0,     0,     0,    -3,    -3,
      -3,     1,    -3,    -3,    -3,    -3,    -3,    -3,    -3,     0,
       0,    -3,     0,     0,     0,    -3,     0,     0,     0,    -3,
       0,    -3,    -3,     0,    -3,    -3,    -3,    -3,    -3,     0,
       0,     0,     0,     0,     0,    -3,    -3,     0,    -3,     0,
       0,     0,     0,     0,     0,     0,     0,    -3,    -3,    -3,
       0,    -3,     0,     0,     0,     0,     0,     0,     0,    -3,
      -3,    -3,     1,    -3,    -3,    -3,    -3,    -3,    -3,    -3,
       0,     0,    -3,     0,     0,     0,    -3,     0,     0,     0,
      -3,     0,    -3,    -3,    -3,    -3,     0,    -3,    -3,    -3,
       0,     0,     0,     0,     0,     0,    -3,    -3,     0,    -3,
       0,     0,     0,     0,     0,     0,     0,     0,    -3,    -3,
      -3,     0,    -3,     0,     0,     0,     0,     0,     0,     0,
      -3,    -3,    -3,     0,    -3,     5,     6,     7,     8,     9,
      10,     0,     0,    11,     0,     0,     0,    12,     0,     0,
       0,    13,   104,    14,    15,     0,    16,     0,    17,    18,
      19,     0,     0,     0,     0,     0,     0,    20,    21,     0,
      22,     0,     0,     0,     0,     0,     0,     0,     0,    23,
      24,    25,     0,    26,     0,     0,     0,     0,     0,     0,
       0,    27,    28,    29,     0,    30,     5,     6,     7,     8,
       9,    10,     0,     0,    11,     0,     0,     0,    12,     0,
       0,     0,    13,     0,    14,    15,     0,    16,   131,    17,
      18,    19,     0,     0,     0,     0,     0,     0,    20,    21,
       0,    22,     0,     0,     0,     0,     0,     0,     0,     0,
      23,    24,    25,     0,    26,     0,     0,     0,     0,     0,
       0,     0,    27,    28,    29,     0,    30,     5,     6,     7,
       8,     9,    10,     0,     0,    11,     0,     0,     0,    12,
       0,     0,     0,    13,     0,    14,    15,   132,    16,     0,
      17,    18,    19,     0,     0,     0,     0,     0,     0,    20,
      21,     0,    22,     0,     0,     0,     0,     0,     0,     0,
       0,    23,    24,    25,     0,    26,     0,     0,     0,     0,
       0,     0,     0,    27,    28,    29,     0,    30,     5,     6,
       7,     8,     9,    10,     0,     0,    11,     0,     0,     0,
      12,     0,     0,     0,    13,     0,    14,    15,     0,    16,
       0,    17,    18,    19,     0,     0,     0,     0,     0,     0,
      20,    21,     0,    22,     0,     0,     0,     0,     0,     0,
       0,     0,    23,    24,    25,     0,    26,     0,     0,     0,
       0,     0,     0,     0,    27,    28,    29,     0,    30,     5,
       6,     7,     8,     9,    10,     0,     0,    11,     0,    66,
       0,    67,     0,     0,    68,    69,    70,    71,   124,     0,
       0,     0,    17,    18,    19,    72,     0,     0,     0,     0,
       0,    20,    21,     0,    22,    54,    55,    56,    57,    58,
      59,     0,    60,     0,    61,     0,    62,    63,   128,     0,
       0,     0,     0,     0,     0,    27,    28,    29,     0,    30,
      64,    65,     0,    54,    55,    56,    57,    58,    59,     0,
      60,     0,    61,     0,    62,    63,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    64,    65,
      54,    55,    56,    57,    58,    59,     0,    60,     0,    61,
       0,    62,    63,    54,    55,    -4,    -4,    58,    59,     0,
      60,     0,    61,     0,    62,    63,    65,    54,    55,    56,
      57,    58,    59,     0,    60,     0,    61,     0,    62,    63,
      -4,    -4,     0,     0,    -4,    -4,     0,    60,     0,    61,
       0,    62,    63
  };

  const signed char
  ShapeParser::yycheck_[] =
  {
      11,    12,    13,    16,    15,    37,    38,    40,    41,    20,
      21,    22,    13,    12,    25,     0,    27,    28,    29,    30,
      26,    27,    28,    11,    35,    79,    16,    17,     6,    12,
      29,    30,    31,    32,    33,    34,     6,    36,     6,    38,
      19,    40,    41,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    54,    55,    68,    69,    70,
      71,    72,   116,    46,    64,    66,    52,    80,    79,    44,
      45,    11,    52,    29,    30,    31,    32,    33,    34,     6,
      36,   113,    38,    65,    40,    41,   118,    18,    14,   121,
      -1,   102,    -1,   125,   105,    -1,    -1,   108,    54,    55,
      -1,   112,    -1,    -1,   117,   116,     0,     1,    -1,     3,
       4,     5,     6,     7,     8,    -1,    -1,    11,    -1,    -1,
      -1,    15,    -1,    -1,    -1,    19,    -1,    21,    22,    -1,
      24,    -1,    26,    27,    28,    -1,    -1,    -1,    -1,    -1,
      -1,    35,    36,    -1,    38,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    47,    48,    49,    -1,    51,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    60,    61,     1,    63,
       3,     4,     5,     6,     7,     8,    -1,    -1,    11,    -1,
      -1,    -1,    15,    16,    17,    18,    19,    -1,    21,    22,
      -1,    24,    -1,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    -1,    38,    -1,    40,    41,    -1,
      -1,    -1,    -1,    -1,    47,    48,    49,    -1,    51,    -1,
      -1,    54,    55,    -1,    -1,    -1,    59,    60,    61,     1,
      63,     3,     4,     5,     6,     7,     8,    -1,    -1,    11,
      -1,    -1,    -1,    15,    -1,    -1,    -1,    19,    20,    21,
      22,    -1,    24,    -1,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    -1,    38,    -1,    40,    41,
      -1,    -1,    -1,    -1,    -1,    47,    48,    49,    -1,    51,
      -1,    -1,    54,    55,    -1,    -1,    -1,    59,    60,    61,
       1,    63,     3,     4,     5,     6,     7,     8,    -1,    -1,
      11,    -1,    -1,    -1,    15,    -1,    -1,    18,    19,    -1,
      21,    22,    -1,    24,    -1,    26,    27,    28,    -1,    -1,
      -1,    -1,    -1,    -1,    35,    36,    -1,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    47,    48,    49,    -1,
      51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,    60,
      61,     1,    63,     3,     4,     5,     6,     7,     8,    -1,
      -1,    11,    -1,    -1,    -1,    15,    -1,    -1,    -1,    19,
      -1,    21,    22,    -1,    24,    25,    26,    27,    28,    -1,
      -1,    -1,    -1,    -1,    -1,    35,    36,    -1,    38,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    47,    48,    49,
      -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    59,
      60,    61,     1,    63,     3,     4,     5,     6,     7,     8,
      -1,    -1,    11,    -1,    -1,    -1,    15,    -1,    -1,    -1,
      19,    -1,    21,    22,    23,    24,    -1,    26,    27,    28,
      -1,    -1,    -1,    -1,    -1,    -1,    35,    36,    -1,    38,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    47,    48,
      49,    -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      59,    60,    61,    -1,    63,     3,     4,     5,     6,     7,
       8,    -1,    -1,    11,    -1,    -1,    -1,    15,    -1,    -1,
      -1,    19,    20,    21,    22,    -1,    24,    -1,    26,    27,
      28,    -1,    -1,    -1,    -1,    -1,    -1,    35,    36,    -1,
      38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    47,
      48,    49,    -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    59,    60,    61,    -1,    63,     3,     4,     5,     6,
       7,     8,    -1,    -1,    11,    -1,    -1,    -1,    15,    -1,
      -1,    -1,    19,    -1,    21,    22,    -1,    24,    25,    26,
      27,    28,    -1,    -1,    -1,    -1,    -1,    -1,    35,    36,
      -1,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      47,    48,    49,    -1,    51,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    59,    60,    61,    -1,    63,     3,     4,     5,
       6,     7,     8,    -1,    -1,    11,    -1,    -1,    -1,    15,
      -1,    -1,    -1,    19,    -1,    21,    22,    23,    24,    -1,
      26,    27,    28,    -1,    -1,    -1,    -1,    -1,    -1,    35,
      36,    -1,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    47,    48,    49,    -1,    51,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    59,    60,    61,    -1,    63,     3,     4,
       5,     6,     7,     8,    -1,    -1,    11,    -1,    -1,    -1,
      15,    -1,    -1,    -1,    19,    -1,    21,    22,    -1,    24,
      -1,    26,    27,    28,    -1,    -1,    -1,    -1,    -1,    -1,
      35,    36,    -1,    38,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    47,    48,    49,    -1,    51,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    59,    60,    61,    -1,    63,     3,
       4,     5,     6,     7,     8,    -1,    -1,    11,    -1,    37,
      -1,    39,    -1,    -1,    42,    43,    44,    45,    12,    -1,
      -1,    -1,    26,    27,    28,    53,    -1,    -1,    -1,    -1,
      -1,    35,    36,    -1,    38,    29,    30,    31,    32,    33,
      34,    -1,    36,    -1,    38,    -1,    40,    41,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    59,    60,    61,    -1,    63,
      54,    55,    -1,    29,    30,    31,    32,    33,    34,    -1,
      36,    -1,    38,    -1,    40,    41,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    54,    55,
      29,    30,    31,    32,    33,    34,    -1,    36,    -1,    38,
      -1,    40,    41,    29,    30,    31,    32,    33,    34,    -1,
      36,    -1,    38,    -1,    40,    41,    55,    29,    30,    31,
      32,    33,    34,    -1,    36,    -1,    38,    -1,    40,    41,
      29,    30,    -1,    -1,    33,    34,    -1,    36,    -1,    38,
      -1,    40,    41
  };

  const unsigned char
  ShapeParser::yystos_[] =
  {
       0,     1,    68,    69,     0,     3,     4,     5,     6,     7,
       8,    11,    15,    19,    21,    22,    24,    26,    27,    28,
      35,    36,    38,    47,    48,    49,    51,    59,    60,    61,
      63,    70,    76,    77,    78,    11,    76,    76,    76,    70,
      75,    76,    78,     6,     6,     6,    76,    76,    76,    76,
      76,    76,    76,    76,    29,    30,    31,    32,    33,    34,
      36,    38,    40,    41,    54,    55,    37,    39,    42,    43,
      44,    45,    53,    74,    76,    12,    69,    69,    19,    52,
      64,    79,    79,    79,    76,    76,    76,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    76,    76,
      76,    12,    46,    72,    20,    11,    75,    78,    13,    66,
      80,    76,    16,    17,    73,    76,    52,    65,    71,    76,
       6,    76,    69,    18,    12,    75,    78,    69,    14,    69,
      69,    25,    23
  };

  const unsigned char
  ShapeParser::yyr1_[] =
  {
       0,    67,    68,    69,    69,    69,    70,    70,    70,    70,
      70,    70,    70,    70,    70,    70,    71,    71,    72,    72,
      73,    73,    74,    74,    74,    75,    75,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    76,    76,
      76,    76,    76,    76,    76,    76,    76,    76,    77,    77,
      77,    77,    77,    78,    78,    78,    79,    79,    79,    80
  };

  const unsigned char
  ShapeParser::yyr2_[] =
  {
       0,     2,     1,     0,     2,     1,     6,     4,     6,     8,
       1,     1,     1,     2,     1,     7,     0,     2,     0,     4,
       0,     2,     0,     1,     3,     0,     1,     1,     1,     3,
       3,     3,     3,     3,     2,     2,     3,     3,     3,     3,
       3,     3,     2,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     2,     2,     2,     2,     1,     4,     1,     1,
       1,     1,     1,     3,     3,     3,     0,     3,     2,     3
  };



  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const ShapeParser::yytname_[] =
  {
  "\"End of File (End)\"", "error", "$undefined", "\"(Real)\"",
  "\"(Integer)\"", "\"(String)\"", "\"(Identifier)\"", "\"(True)\"",
  "\"(False)\"", "\"{ (OpenCurlyBrace)\"", "\"} (CloseCurlyBrace)\"",
  "\"( (OpenBrace)\"", "\") (CloseBrace)\"", "\"] (OpenBracket)\"",
  "\"[ (CloseBracket)\"", "\"if (If)\"", "\"elseif (ElseIf)\"",
  "\"else (Else)\"", "\"/if (EndIf)\"", "\"while (While)\"",
  "\"/while (EndWhile)\"", "\"do (Do)\"", "\"for (For)\"",
  "\"/for (EndFor)\"", "\"foreach (Foreach)\"",
  "\"/foreach (EndForeach)\"", "\"$ (Dollar)\"", "\"# (Sharp)\"",
  "\"@ (At)\"", "\"< (LT)\"", "\"> (GT)\"", "\"== (EQ)\"", "\"!= (NE)\"",
  "\"<= (LE)\"", "\">= (GE)\"", "\"! (Not)\"", "\"+ (Plus)\"",
  "\"++ (PlusPlus)\"", "\"- (Minus)\"", "\"-- (MinusMinus)\"",
  "\"* (Multiply)\"", "\"/ (Division)\"", "\"+= (PlusAssign)\"",
  "\"-= (MinusAssign)\"", "\"*= (MultiplyAssign)\"",
  "\"/= (DivisionAssign)\"", "\", (Comma)\"", "\"break (Break)\"",
  "\"continue (Continue)\"", "\"echo (Echo)\"", "\"comment (Comment)\"",
  "\"(Content)\"", "\"; (Semicolon)\"", "Assign", "Or", "And", "'.'",
  "'%'", "Inc", "IntegerCast", "RealCast", "StringCast", "ArrayCast",
  "BooleanCast", "As", "Arrow", "Dot", "$accept", "Start", "StatementList",
  "Statement", "ForeachOptionalArg", "ElseIfList", "ElseSingle",
  "FunctionCallParameterList", "ForExpr", "Expr", "Value", "Variable",
  "AccessOperator", "Index", YY_NULL
  };

#if YYDEBUG
  const unsigned short int
  ShapeParser::yyrline_[] =
  {
       0,   140,   140,   147,   148,   149,   152,   161,   166,   171,
     176,   181,   186,   191,   196,   201,   208,   209,   212,   213,
     224,   225,   228,   229,   230,   233,   234,   237,   238,   239,
     240,   241,   242,   243,   244,   245,   246,   247,   248,   249,
     250,   251,   252,   253,   254,   255,   256,   257,   258,   259,
     260,   261,   262,   263,   264,   265,   266,   267,   271,   272,
     273,   274,   275,   278,   279,   280,   295,   296,   301,   308
  };

  // Print the state stack on the debug stream.
  void
  ShapeParser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  ShapeParser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG


#line 6 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:1163
} // Shape
#line 1713 "Shape/src/ShapeParser/ShapeParser.cpp" // lalr1.m4:1163
#line 311 "Shape\\src\\ShapeParser\\ShapeParser.yy" // lalr1.m4:1164


void Shape::ShapeParser::error(const location_type &location, const std::string &message)
{
	if (compiler.GetController() != nullptr)
		compiler.GetController()->EmitErrorHandler(location, message);
}

#pragma warning(pop)

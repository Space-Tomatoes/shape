%{
	#pragma warning(push)
	#pragma warning(disable: 4005 4065)

	#include <cstdlib>
	#include <cstdio>
	#include <cerrno>
	#include <string>
	#include <climits>
	#include <locale>

	#include <Shape/ShapeCompiler/ShapeCompiler.hpp>
	#include <Shape/ShapeParser/ShapeParser.hpp>

	#undef yywrap
	#define yywrap() 1
	
	static Shape::location loc;
%}

%option noyywrap nounput batch debug noinput

%{
	#define YY_USER_ACTION	loc.columns(yyleng);

/*	#define YY_INPUT(buf, result, max_size)																\
	{																									\
		result = YY_NULL;																				\
		short c = 0;																					\
																										\
		for (result = YY_NULL; result < max_size && (c = getc(yyin)) != EOF && c != '\n'; ++result)		\
			buf[result] = (char)c;																		\
																										\
		if (c == '\n')																					\
			buf[result++] = (char)c;																	\
	}		*/																							\
%}

%x ShapeState
%x StringState

Any					.*
Digit				[0-9]
Letter				[a-zA-Z]
Integer				{Digit}+
Real				{Digit}+"."{Digit}*
Whitespace			[ \t\r\n]
Identifier			({Letter}|"_")({Digit}|{Letter}|"_"|"\\")*

/* ����������� �������� ������ - ��������� �� ���� ������ Shape-������������������. */
OpenCurlyBrace		"{"

/* ����������� �������� ������ - ��������� �� ���� ����� Shape-������������������. */
CloseCurlyBrace		"}"

OpenBrace			"("
CloseBrace			")"

OpenBracket			"["
CloseBracket		"]"

/* ������� ��������� ���������. */
If					"if"
ElseIf				"elseif"
Else				"else"
EndIf				"/if"

/* �����. */
Foreach				"foreach"
EndForeach			"/foreach"
While				"while"
EndWhile			"/while"
For					"for"
EndFor				"/for"
Do					"do"

True				"true"
False				"false"

/* �������� ��������� � IDataStorage. */
Dollar				"$"
Sharp				"#"
At					"@"

/* ���������� ���������. */
Not					"!"
EQ					"=="
NE					"!="
LE					"<="
GE					">="
GT					">"
LT					"<"

And					"&&"
Or					"||"

Assign				"="

/* �������������� ���������. */
Plus				"+"
Minus				"-"
Multiply			"*"
Division			"/"

PlusAssign			"+="
MinusAssign			"-="
MultiplyAssign		"*="
DivisionAssign		"/="

PlusPlus			"++"
MinusMinus			"--"

Comma				","
Semicolon			";"

Comment				"{*"
EndComment			"*}"

String				"\""
EndString			"\""

Arrow				"->"

Dot					"."

/* ����� ������ ����� {OpenCurlyBrace} ��� �������. */
Content				[^{OpenCurlyBrace}]+

IntegerCast			("(Integer)"|"(integer)"|"(Int)"|"(int)")
RealCast			("(Real)"|"(real)")
BooleanCast			("(Boolean)"|"(boolean)"|"(Bool)"|"(bool)")
StringCast			("(String)"|"(string)")

As					("as"|"ass")

%%

%{
	loc.step();
%}

<INITIAL>{Comment}{Any}{EndComment}			;

<ShapeState>\n								{
												loc.lines(yyleng);
											}
<ShapeState>{IntegerCast}					return Shape::ShapeParser::make_IntegerCast(loc);
<ShapeState>{RealCast}						return Shape::ShapeParser::make_RealCast(loc);
<ShapeState>{BooleanCast}					return Shape::ShapeParser::make_BooleanCast(loc);
<ShapeState>{StringCast}					return Shape::ShapeParser::make_StringCast(loc);

<ShapeState>{If}							return Shape::ShapeParser::make_If(loc);
<ShapeState>{ElseIf}						return Shape::ShapeParser::make_ElseIf(loc);
<ShapeState>{Else}							return Shape::ShapeParser::make_Else(loc);
<ShapeState>{EndIf}							return Shape::ShapeParser::make_EndIf(loc);

<ShapeState>{While}							return Shape::ShapeParser::make_While(loc);
<ShapeState>{EndWhile}						return Shape::ShapeParser::make_EndWhile(loc);

<ShapeState>{Foreach}						return Shape::ShapeParser::make_Foreach(loc);
<ShapeState>{EndForeach}					return Shape::ShapeParser::make_EndForeach(loc);
<ShapeState>{As}							return Shape::ShapeParser::make_As(loc);

<ShapeState>{For}							return Shape::ShapeParser::make_For(loc);
<ShapeState>{EndFor}						return Shape::ShapeParser::make_EndFor(loc);

<ShapeState>{Dollar}						return Shape::ShapeParser::make_Dollar(loc);
<ShapeState>{Sharp}							return Shape::ShapeParser::make_Sharp(loc);
<ShapeState>{At}							return Shape::ShapeParser::make_At(loc);

<ShapeState>{EQ}							return Shape::ShapeParser::make_EQ(loc);

<ShapeState>{Assign}						return Shape::ShapeParser::make_Assign(loc);
<ShapeState>{PlusPlus}						return Shape::ShapeParser::make_PlusPlus(loc);
<ShapeState>{Plus}							return Shape::ShapeParser::make_Plus(loc);
<ShapeState>{MinusMinus}					return Shape::ShapeParser::make_MinusMinus(loc);
<ShapeState>{Minus}							return Shape::ShapeParser::make_Minus(loc);
<ShapeState>{Multiply}						return Shape::ShapeParser::make_Multiply(loc);
<ShapeState>{Division}						return Shape::ShapeParser::make_Division(loc);

<ShapeState>{PlusAssign}					return Shape::ShapeParser::make_PlusAssign(loc);
<ShapeState>{MinusAssign}					return Shape::ShapeParser::make_MinusAssign(loc);
<ShapeState>{MultiplyAssign}				return Shape::ShapeParser::make_MultiplyAssign(loc);
<ShapeState>{DivisionAssign}				return Shape::ShapeParser::make_DivisionAssign(loc);

<ShapeState>{Comma}							return Shape::ShapeParser::make_Comma(loc);
<ShapeState>{Semicolon}						return Shape::ShapeParser::make_Semicolon(loc);

<ShapeState>{Not}							return Shape::ShapeParser::make_Not(loc);

<ShapeState>{NE}							return Shape::ShapeParser::make_NE(loc);
<ShapeState>{LE}							return Shape::ShapeParser::make_LE(loc);
<ShapeState>{GE}							return Shape::ShapeParser::make_GE(loc);
<ShapeState>{GT}							return Shape::ShapeParser::make_GT(loc);
<ShapeState>{LT}							return Shape::ShapeParser::make_LT(loc);
<ShapeState>{And}							return Shape::ShapeParser::make_And(loc);
<ShapeState>{Or}							return Shape::ShapeParser::make_Or(loc);

<ShapeState>{OpenBrace}						return Shape::ShapeParser::make_OpenBrace(loc);
<ShapeState>{CloseBrace}					return Shape::ShapeParser::make_CloseBrace(loc);
<ShapeState>{OpenBracket}					return Shape::ShapeParser::make_OpenBracket(loc);
<ShapeState>{CloseBracket}					return Shape::ShapeParser::make_CloseBracket(loc);

<ShapeState>{Arrow}							return Shape::ShapeParser::make_Arrow(loc);
<ShapeState>{Dot}							return Shape::ShapeParser::make_Dot(loc);

<ShapeState>{Whitespace}					; /* Ignoring whitespaces. */
<ShapeState>{Real}							{ /* Real numbers. */
												errno = 0;

												_locale_t locale = _create_locale(LC_ALL, "en-US");
												double value = _strtod_l(yytext, NULL, locale);
												_free_locale(locale);

												return Shape::ShapeParser::make_Real((float)value, loc);
											}
<ShapeState>{Integer}						{ /* Integer numbers. */
												errno = 0;
												long value = strtol(yytext, NULL, 10);

												if (!(INT_MIN <= value && value <= INT_MAX && errno != ERANGE))
													compiler.Error("Integer is out of range");

												return Shape::ShapeParser::make_Integer(value, loc);
											}
<ShapeState>{True}							{
												return Shape::ShapeParser::make_True(loc);
											}
<ShapeState>{False}							{
												return Shape::ShapeParser::make_False(loc);
											}
<ShapeState>{String}						{ /* Begin of string token. */
												BEGIN(StringState);
												yylval->build<std::string>() = "";																							
											}
<StringState>[^\\\n"]+						{												
												yylval->as<std::string>() += yytext;											
											}
<StringState>\\n							{ 
												yylval->as<std::string>() += '\n'; 
											}
<StringState>\\t							{ 
												yylval->as<std::string>() += '\t';
											}
<StringState>\\r							{ 
												yylval->as<std::string>() += '\r'; 
											}
<StringState>\\[\\]							{ 
												yylval->as<std::string>() += '\\';
											}
<StringState>\\["]							{ 
												yylval->as<std::string>() += '"'; 
											}
<StringState>\\								{ 
												compiler.Error(loc, "Invalid escape sequence in string literal"); 
											}
<StringState>\n								{ 
												compiler.Error(loc, "Newline in string literal"); 
											}
<StringState>{EndString}					{ /* End of string token. */
												BEGIN(ShapeState);

												return Shape::ShapeParser::make_String(yylval->as<std::string>().c_str(), loc);
											}
<ShapeState>{Identifier}					{
												return Shape::ShapeParser::make_Identifier(yytext, loc);
											}
<INITIAL>{OpenCurlyBrace}					{ /* ����������� �������� ������ - ������� � Shape. */
												BEGIN(ShapeState);
												//return Shape::ShapeParser::make_Content(yytext, loc);
												//return Shape::ShapeParser::make_OpenCurlyBrace(loc);
											}
<ShapeState>{CloseCurlyBrace}				{ /* ����������� �������� ������ � Shape - ������� � INITIAL (����� �� Shape). */
												BEGIN(INITIAL);												
												//return Shape::ShapeParser::make_CloseCurlyBrace(loc);
											}
<ShapeState>.								{ 
												compiler.Error(loc, "Invalid character");												
											}
<INITIAL>\n									{
												loc.lines(yyleng);
												return Shape::ShapeParser::make_Content("\n", loc);
											}
<INITIAL>[^{]+								{ /* �������. */
												
												
												//yymore();
												
												std::string data = yytext;

												if (data.size() >= 1 && data.front() == '\n')
													data.erase(0, 1);

												return Shape::ShapeParser::make_Content(data, loc);
												//return Shape::ShapeParser::make_Content(data.size() ? data : yytext, loc);
											}
<<EOF>>										{
												return Shape::ShapeParser::make_End(loc);
											}
%%

namespace Shape
{
	/// \brief OnScanBegin callback.
	void ShapeCompiler::ScanBegin()
	{
		yy_flex_debug = this->traceScanning;		

		if (mode == File)
		{
			if (this->fileName.empty() || this->fileName == "-")
				yyin = stdin;
			else if (!(yyin = fopen(this->fileName.c_str(), "r")))
				Error("Cannot open file: " + this->fileName + ": " + strerror(errno));
		}
		else if (mode == Buffer)
		{
			yy_scan_string(this->buffer.c_str());
		}
		
		loc = location();
	}

	/// \brief OnScanEnd callback.
	void ShapeCompiler::ScanEnd()
	{
		if (mode == File)
			fclose(yyin);

		yylex_destroy();
	}
} // namespace Shape

#pragma warning(pop)
/// \file DefaultStorages.cpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#include <Shape/Handlers/DefaultStorages.hpp>

namespace Shape { namespace DefaultStorages
{
	Value * LocalStorage::operator () (CompilerController *controller, const std::string &key)
	{
		Value *value = nullptr;

		if (key.empty())
			value = &this->memory;
		else
			value = &this->memory[key];

		return value;
	}
} /* namespace DefaultStorages */ } // namespace Shape
/// \file CompilerController.cpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#include <Shape/Handlers/CompilerController.hpp>

namespace Shape
{
	/*
		Statics.
	*/
	ArgumentList CompilerController::EMPTY_ARGS;
} // namespace Shape
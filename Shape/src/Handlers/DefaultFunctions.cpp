#include <Shape/Handlers/DefaultFunctions.hpp>
#include <Shape/ShapeCompiler/ShapeCompiler.hpp>

namespace Shape { namespace DefaultFunctions
{
	Value IncludeFunction::operator () (CompilerController *controller, ArgumentList &args)
	{
		Value value("");

		for (const auto &arg : args)
		{
			ShapeCompiler cmpl;

			cmpl.SetController(controller);

			value += cmpl.ParseFile(arg->Execute().ToString());
		}

		return value;
	}
} /* namespace Shape */ } // namespace Shape
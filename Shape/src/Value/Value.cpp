/// \file Value.cpp
/// \brief Shape-value definition.
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>
/// \copyright Copyright � Wincor Nixdorf Russia, 2013.

#pragma once

#include <Shape/Value/Value.hpp>

namespace Shape
{
	/* 
		Constructors. 
	*/
	/// \brief Constructs a Void value object.
	Value::Value(Void)
	{ 
		FromVoid();
	}

	Value::Value(const Value &value)
	{
		FromValue(value);
	}

	/// \brief Constructs an Integer value object.
	/// \param [in] value The initial value for the object
	Value::Value(const Integer &value)
	{ 
		FromInteger(value);
	}

	/// \brief Constructs a Real value object.
	/// \param [in] value The initial value for the object
	Value::Value(const Real &value)
	{ 
		FromReal(value);
	}

	/// \brief Constructs a String value object.
	/// \param [in] value The initial value for the object
	Value::Value(const char *value)
	{ 
		FromString(value);
	}

	/// \brief Constructs a String value object.
	/// \param [in] value The initial value for the object
	Value::Value(const String &value)
	{ 
		FromString(value);
	}

	/// \brief Constructs a Boolean value object.
	/// \param [in] value The initial value for the object
	Value::Value(const Boolean &value)
	{ 
		FromBoolean(value);
	}

	/// \brief Constructs a Array value object.
	/// \param [in] value The initial value for the object.
	Value::Value(const ArrayPtr &value)
	{
		//FromArray(value);
	}

	Value::Value(const ValueType &type)
	{
		FromVoid();
		this->type = type;
		arrayPtr = std::make_shared<Array>(100, Value::GetHash, Value::EqualTo);
	}

	/*
		Assignment operators.
	*/
	/// \brief Copying assignment operator.
	/// \param [in] value The new value for the object
	/// \return Reference to the left operand.
	Value & Value::operator = (const Value &value)
	{ 
		if (this->ref != nullptr)
			ref->FromValue(value);

		FromValue(value);

		return *this;
	}

	/// \brief Integer assignment operator.
	/// \param [in] value The new value for the object
	/// \return Reference to the left operand.
	Value & Value::operator = (const Integer &value)
	{ 
		FromInteger(value);

		return *this;
	}

	/// \brief Real assignment operator.
	/// \param [in] value The new value for the object
	/// \return Reference to the left operand.
	Value & Value::operator = (const Real &value)
	{ 
		FromReal(value);

		return *this;
	}

	/// \brief String assignment operator.
	/// \param [in] value The new value for the object
	/// \return Reference to the left operand.
	Value & Value::operator = (const char *value)
	{ 
		FromString(value);

		return *this;
	}

	/// \brief String assignment operator.
	/// \param [in] value The new value for the object
	/// \return Reference to the left operand.
	Value & Value::operator = (const String &value)
	{ 
		FromString(value);

		return *this;
	}

	/// \brief Boolean assignment operator.
	/// \param [in] value The new value for the object
	/// \return Reference to the left operand.
	Value & Value::operator = (const Boolean &value)
	{ 
		FromBoolean(value);

		return *this;
	}

	/// \brief Constructs a Array value object.
	/// \param [in] value The initial value for the object.
	Value & Value::operator = (const ArrayPtr &value)
	{
		//FromArray(value);

		return *this;
	}

	/*
		Binary arithmetic operators.
	*/
	/// \brief The addition operator.
	/// \param [in] value The right operand.
	/// \return A new object with the result of operation.
	const Value Value::operator + (const Value &value) const
	{ 
		Value tmp(*this);

		tmp.type = GetImplicitConvertionType(value);

		switch (tmp.type)
		{
			case TInteger:
				tmp.FromInteger(tmp.intValue + value.intValue);
			break;
			case TReal:
				tmp.FromReal(tmp.realValue + value.realValue);
			break;
			case TString:
				tmp.FromString(tmp.stringValue + value.stringValue);
			break;
			case TBoolean:
				tmp.FromBoolean(tmp.booleanValue && value.booleanValue);
			break;
		}

		return tmp;
	}

	/// \brief The difference operator.
	/// \param [in] value The right operand.
	/// \return A new object with the result of operation.
	const Value Value::operator - (const Value &value) const
	{ 
		Value tmp(*this);

		tmp.type = GetImplicitConvertionType(value);

		switch (tmp.type)
		{
			case TInteger:
				tmp.FromInteger(tmp.intValue - value.intValue);
			break;
			case TReal:
				tmp.FromReal(tmp.realValue - value.realValue);
			break;
			case TString:
				throw InvalidOperationException("The difference operator is not permitted for strings.", this->location);
			break;
			case TBoolean:
				throw InvalidOperationException("The difference operator is not permitted for boolean.", this->location);
			break;
		}

		return tmp;
	}

	/// \brief The multiplication operator.
	/// \param [in] value The right operand.
	/// \return A new object with the result of operation.
	const Value Value::operator * (const Value &value) const
	{ 
		Value tmp(*this);

		tmp.type = GetImplicitConvertionType(value);

		switch (tmp.type)
		{
			case TInteger:
				tmp.FromInteger(tmp.intValue * value.intValue);
			break;
			case TReal:
				tmp.FromReal(tmp.realValue * value.realValue);
			break;
			case TString:
				throw InvalidOperationException("The multiplication operator is not permitted for strings.", this->location);
			break;
			case TBoolean:
				throw InvalidOperationException("The multiplication operator is not permitted for boolean.", this->location);
			break;
		}

		return tmp;
	}

	/// \brief The division operator.
	/// \param [in] value The right operand.
	/// \return A new object with the result of operation.
	const Value Value::operator / (const Value &value) const
	{ 
		Value tmp(*this);

		tmp.type = GetImplicitConvertionType(value);

		switch (tmp.type)
		{
			case TInteger:
				if (value.intValue != 0)
					tmp.FromInteger(tmp.intValue / value.intValue);
				else throw InvalidOperationException("Division by zero", this->location);
			break;
			case TReal:
				if (value.realValue != 0.0f)
					tmp.FromReal(tmp.realValue / value.realValue);
				else throw InvalidOperationException("Division by zero", this->location);
			break;
			case TString:
				throw InvalidOperationException("The division operator is not permitted for strings.", this->location);
			break;
			case TBoolean:
				/*if (value.booleanValue != false)
					tmp.FromBoolean(tmp.booleanValue / value.booleanValue);
				else throw InvalidOperationException("Division by zero");*/
				throw InvalidOperationException("The division operator is not permitted for boolean.", this->location);
			break;
		}

		return tmp;
	}

	/*
		Binary arithmetic operators with assignment.
	*/
	/// \brief The addition operator with assignment.
	/// \param [in] value The right operand.
	/// \return Reference to the left operand.
	Value & Value::operator += (const Value &value)
	{ 
		*this = *this + value;

		return *this;
	}

	/// \brief The difference operator with assignment.
	/// \param [in] value The right operand.
	/// \return Reference to the left operand.
	Value & Value::operator -= (const Value &value)
	{ 
		*this = *this - value;
		
		return *this;
	}

	/// \brief The multiplication operator with assignment.
	/// \param [in] value The right operand.
	/// \return Reference to the left operand.
	Value & Value::operator *= (const Value &value)
	{
		*this = *this * value;

		return *this;
	}

	/// \brief The division operator with assignment.
	/// \param [in] The right operand.
	/// \return Reference to the left operand.
	Value & Value::operator /= (const Value &value)
	{ 
		*this = *this / value;

		return *this;	
	}

	/*
		Unary operators.
	*/
	/// \brief The positive operator.
	/// \return A new object with the result of operation.
	const Value Value::operator+() const
	{
		return Value(0) + *this;
	}

	/// \brief The negation operator.
	/// \return A new object with the result of operation.
	const Value Value::operator-() const
	{ 
		return Value(0) - *this;
	}

	const Value Value::operator ! () const
	{
		return !this->ToBoolean();
	}

	Value & Value::operator -- ()
	{
		switch (this->type)
		{
			case TInteger:
				this->FromInteger(--this->intValue);
			break;
			case TReal:
				this->FromReal(--this->realValue);
			break;
			case TString:
				throw InvalidOperationException("The decrement operator is not permitted for strings.", this->location);
			break;
			case TBoolean:
				throw InvalidOperationException("The decrement operator is not permitted for boolean.", this->location);
			break;
		}

		return *this;
	}

	Value & Value::operator ++ ()
	{
		switch (this->type)
		{
			case TInteger:
				this->FromInteger(++this->intValue);
			break;
			case TReal:
				this->FromReal(++this->realValue);
			break;
			case TString:
				throw InvalidOperationException("The increment operator is not permitted for strings.", this->location);
			break;
			case TBoolean:
				throw InvalidOperationException("The increment operator is not permitted for boolean.", this->location);
			break;
		}

		return *this;
	}

	/*
		Cast operations.
	*/
	/// \brief Casts a Void value to the Shape-value.
	void Value::FromVoid(Void)
	{ 
		type = TVoid;
		intValue = 0;
		realValue = 0.0f;
		stringValue = "";
		booleanValue = false;
		ref = nullptr;
		isSystem = false;
	}

	/// \brief Casts an Integer value to the Shape-value.
	/// \param [in] value An Integer value.
	void Value::FromInteger(const Integer &value)
	{ 
		std::stringstream ss;
		type = TInteger;
		intValue = value;
		realValue = (Real)value;
		ss << value;
		stringValue = ss.str();
		booleanValue = (value != 0);
		ref = nullptr;
		isSystem = false;
	}

	/// \brief Casts a Real value to the Shape-value.
	/// \param [in] value A Real value.
	void Value::FromReal(const Real &value)
	{ 
		std::stringstream ss;
		type = TReal;
		intValue = (Integer)value;
		realValue = value;
		ss << value;
		stringValue = ss.str();
		booleanValue = (value != 0);
		ref = nullptr;
		isSystem = false;
	}

	/// \brief Casts a String value to the Shape-value.
	/// \param [in] value A String value.
	void Value::FromString(const String &value)
	{ 
		type = TString;
		stringValue = value;

		intValue = 0;
		realValue = 0;
		booleanValue = false;
		ref = nullptr;
		isSystem = false;

		/*std::stringstream ss;
		ss << value;
		ss >> intValue;
		ss >> realValue;
		ss >> booleanValue;*/
	}

	/// \brief Casts a Boolean value to the Shape-value.
	/// \param [in] value A Boolean value.
	void Value::FromBoolean(const Boolean &value)
	{ 
		std::stringstream ss;
		type = TBoolean;
		intValue = value;
		realValue = value;
		ss << value;
		stringValue = ss.str();
		booleanValue = value;
		ref = nullptr;
		isSystem = false;
	}

	void Value::FromValue(const Value &value)
	{
		type = value.type;
		intValue = value.intValue;
		realValue = value.realValue;
		stringValue = value.stringValue;
		booleanValue = value.booleanValue;
		arrayPtr = value.arrayPtr;
		ref = value.ref;
		this->location = value.location;
		isSystem = false;
	}

	/// \brief Casts the current value to an Integer value.
	/// \return The Integer representation of the current value.
	Value::Integer Value::ToInteger() const
	{ 
		return intValue;
	}

	/// \brief Casts the current value to a Real value.
	/// \return The Real representation of the current value.
	Value::Real Value::ToReal() const
	{ 
		return realValue;
	}

	/// \brief Casts the current value to a String value.
	/// \return The String representation of the current value.
	Value::String Value::ToString() const
	{ 
		return stringValue;
	}

	/// \brief Casts the current value to a Boolean value.
	/// \return The Boolean representation of the current value.
	Value::Boolean Value::ToBoolean() const
	{
		return (intValue != 0);
	}

	bool Value::IsVoid() const
	{ 
		return this->type == Value::TVoid; 
	}

	bool Value::IsInteger() const
	{ 
		return this->type == Value::TInteger; 
	}

	bool Value::IsReal() const
	{ 
		return this->type == Value::TReal; 
	}

	bool Value::IsString() const
	{ 
		return this->type == Value::TString; 
	}

	bool Value::IsBoolean() const
	{ 
		return this->type == Value::TBoolean; 
	}

	bool Value::IsArray() const
	{
		return this->type == Value::TArray;
	}

	/// \brief Prints value.
	void Value::Print() const
	{
		if (type == TString)
		{
			std::string tmp(stringValue);

			ReplaceAll(tmp, "\\", "\\\\");
			ReplaceAll(tmp, "\n", "\\n");
			ReplaceAll(tmp, "\r", "\\r");
			ReplaceAll(tmp, "\t", "\\t");				
			ReplaceAll(tmp, "\"", "\\\"");

			std::cout << "\"" << tmp << "\"";
		}
		else if (type == TBoolean)
			std::cout << (booleanValue ? "true" : "false");
		else if (type == TInteger)
			std::cout << intValue;
		else if (type == TReal)
			std::cout << realValue;
	}

	Value Value::Execute() const
	{
		return *this;
	}

	/// \brief Gets type of current value.
	/// \return Type of current value.
	Value::ValueType Value::GetType() const
	{
		return type;
	}

	Value::ValueType Value::GetImplicitConvertionType(const Value &value) const
	{
		ValueType result = TVoid;

		if (type != TVoid && value.type != TVoid)
		{
			/* Boolean cast. */
			if (type == TBoolean && value.type == TBoolean)
				result = TBoolean;
			/* Numeric casts */
			else if (type != TString && value.type != TString)
			{
				/* Real cast. */
				if (type == TReal || value.type == TReal)
					result = TReal;
				/* Integer cast. */
				else result = TInteger;
			}
			/* String cast. */
			else if (type == TString || value.type != TString)
				result = TString;
		}

		if (result == TVoid && value.type != TVoid)
			result = value.type;		
		else if (result == TVoid && type != TVoid)
			result = type;

		return result;
	}

	/*
		Logical operators.
	*/
	/// \brief The equality operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator == (const Value &left, const Value &right)
	{ 
		bool result = false;

		switch (left.GetImplicitConvertionType(right))
		{
			case Value::TInteger:
				result = left.intValue == right.intValue;
			break;
			case Value::TReal:
				result = left.realValue == right.realValue;
			break;
			case Value::TString:
				result = left.stringValue == right.stringValue;
			break;
			case Value::TBoolean:
				result = left.booleanValue == right.booleanValue;
			break;
		}

		return result;
	}

	bool operator != (const Value &left, const Value &right)
	{
		return !(left == right);
	}

	/// \brief The less than operator
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator < (const Value &left, const Value &right)
	{ 
		bool result = false;

		switch (left.GetImplicitConvertionType(right))
		{
			case Value::TInteger:
				result = left.intValue < right.intValue;
			break;
			case Value::TReal:
				result = left.realValue < right.realValue;
			break;
			case Value::TString:
				result = left.stringValue < right.stringValue;
			break;
			case Value::TBoolean:
				result = left.booleanValue < right.booleanValue;
			break;
		}

		return result;
	}

	/// \brief The greater than operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator > (const Value &left, const Value &right)
	{ 
		bool result = false;

		switch (left.GetImplicitConvertionType(right))
		{
			case Value::TInteger:
				result = left.intValue > right.intValue;
			break;
			case Value::TReal:
				result = left.realValue > right.realValue;
			break;
			case Value::TString:
				result = left.stringValue > right.stringValue;
			break;
			case Value::TBoolean:
				result = left.booleanValue > right.booleanValue;
			break;
		}

		return result;
	}

	/// \brief The less than or equal operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator <= (const Value &left, const Value &right)
	{ 
		bool result = false;

		switch (left.GetImplicitConvertionType(right))
		{
			case Value::TInteger:
				result = left.intValue <= right.intValue;
			break;
			case Value::TReal:
				result = left.realValue <= right.realValue;
			break;
			case Value::TString:
				result = left.stringValue <= right.stringValue;
			break;
			case Value::TBoolean:
				result = left.booleanValue <= right.booleanValue;
			break;
		}

		return result;
	}

	/// \brief The greater than or equal operator.
	/// \param [in] left The left operand.
	/// \param [in] right The right operand.
	/// \return Result of a logical operation.
	bool operator >= (const Value &left, const Value &right)
	{ 
		bool result = false;

		switch (left.GetImplicitConvertionType(right))
		{
			case Value::TInteger:
				result = left.intValue >= right.intValue;
			break;
			case Value::TReal:
				result = left.realValue >= right.realValue;
			break;
			case Value::TString:
				result = left.stringValue >= right.stringValue;
			break;
			case Value::TBoolean:
				result = left.booleanValue >= right.booleanValue;
			break;
		}

		return result;	
	}

	bool operator || (const Value &left, const Value &right)
	{
		bool result = false;

		switch (left.GetImplicitConvertionType(right))
		{
			case Value::TInteger:
				result = left.intValue || right.intValue;
			break;
			case Value::TReal:
				result = left.realValue || right.realValue;
			break;
			case Value::TString:
				result = left.stringValue || right.stringValue;
			break;
			case Value::TBoolean:
				result = left.booleanValue || right.booleanValue;
			break;
		}

		return result;
	}

	bool operator && (const Value &left, const Value &right)
	{
		bool result = false;

		switch (left.GetImplicitConvertionType(right))
		{
			case Value::TInteger:
				result = left.intValue && right.intValue;
			break;
			case Value::TReal:
				result = left.realValue && right.realValue;
			break;
			case Value::TString:
				result = left.stringValue && right.stringValue;
			break;
			case Value::TBoolean:
				result = left.booleanValue && right.booleanValue;
			break;
		}

		return result;
	}

	/*
		Stream I/O.
	*/
	/// \brief The stream input operator.
	/// \param [in] stream Reference to the input stream.
	/// \param [out] value Value object into which the data will be read.
	/// \return Reference to the input stream.
	std::istream & operator >> (std::istream &stream, Value &value)
	{ 
		std::string tmp;

		stream >> tmp;
		value.FromString(tmp);

		return stream;
	}

	/// \brief The stream output operator.
	/// \param [in] stream Reference to the output stream.
	/// \param [in] value Value object from which the data will be read.
	/// \return Reference to the output stream.
	std::ostream & operator << (std::ostream &stream, const Value &value)
	{ 
		stream << value.ToString();
		
		return stream;
	}
} // namespace Shape
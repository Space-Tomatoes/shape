{
	'variables' : {
		'source_files' : [
			'src/Array.cpp',
			'src/Convert.cpp',
			'src/Escaper.cpp',
			'src/IndentCanceller.cpp',
			'src/Indenter.cpp',
			'src/Object.cpp',
			'src/SolidusEscaper.cpp',
			'src/Value.cpp',
		],
		'include_files' : [
			'include/JsonBox.h',
			'include/JsonBox/Array.h',
			'include/JsonBox/Convert.h',
			'include/JsonBox/Escaper.h',
			'include/JsonBox/Grammar.h',
			'include/JsonBox/IndentCanceller.h',
			'include/JsonBox/Indenter.h',
			'include/JsonBox/Object.h',
			'include/JsonBox/OutputFilter.h',
			'include/JsonBox/SolidusEscaper.h',
			'include/JsonBox/Value.h',
		],	
	},
}
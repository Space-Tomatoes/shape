{
	'includes': [
		'../../../../Shared/Common.gypi',
	],
	'targets': [
		{
			'target_name': 'JsonBox',
			'type': 'static_library',
			'includes': [
				'sources.gypi',
			],
			'sources': [
				'<@(source_files)', '<@(include_files)',
			],
			'include_dirs': [
				'include',
			],
			'direct_dependent_settings': {
				'include_dirs': [
					'include',
				],
			},
		},
	],
}
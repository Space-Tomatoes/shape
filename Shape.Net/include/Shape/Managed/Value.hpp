#pragma once

#include <msclr/marshal_cppstd.h>

#include <Shape.hpp>

using namespace System;

namespace Shape { namespace Managed
{
	public ref class Value :
		public IDisposable
	{
	public:
		enum class ValueType : 
			unsigned __int8
		{
			Void,
			Integer,
			Real,
			String,
			Boolean,
			Array,
			Object,
			Reference
		};

	internal:
		Shape::Value *value;

		/// \brief internal use only. maps managed wrapper to native instance of object.
		Value(Shape::Value *value) :
			value(value)
		{ }

	public:
		virtual ~Value()
		{ }

		/* Properties. */
		property Value::ValueType Type
		{
			Value::ValueType get()
			{				
				return (Value::ValueType)this->value->GetType();
			}
		}

		/* Indexers. */
		property Value ^ default[Int32]
		{
			Value ^ get(Int32 index)
			{
				return gcnew Value(&this->value->GetChild(index));
			}
		}

		property Value ^ default[String ^]
		{
			Value ^ get(String ^index)
			{
				return gcnew Value(&this->value->GetChild(msclr::interop::marshal_as<std::string>(index)));
			}
		}

		property Value ^ default[Single]
		{
			Value ^ get(Single index)
			{
				return gcnew Value(&this->value->GetChild(index));
			}
		}

		property Value ^ default[Boolean]
		{
			Value ^ get(Boolean index)
			{
				return gcnew Value(&this->value->GetChild(index));
			}
		}

		Value ^ FromInteger(Int32 value)
		{
			this->value->FromInteger(value);
			return this;
		}

		Value ^ FromString(String ^value)
		{
			this->value->FromString(msclr::interop::marshal_as<std::string>(value));
			return this;
		}

		Value ^ FromSingle(Single value)
		{
			this->value->FromReal(value);
			return this;
		}

		Value ^ FromBoolean(Boolean value)
		{
			this->value->FromBoolean(value);
			return this;
		}

		Value ^ MakeVoid()
		{
			this->value->FromVoid();
			return this;
		}

		Value ^ MakeArray()
		{
			*this->value = Shape::Value(Shape::Value::TArray);
			return this;
		}

		virtual String ^ ToString() override
		{
			return msclr::interop::marshal_as<String ^>(this->value->ToString());
		}

		Int32 ToInt32()
		{
			return this->value->ToInteger();
		}

		Single ToSingle()
		{
			return this->value->ToReal();
		}

		Boolean ToBoolean()
		{
			return this->value->ToBoolean();
		}

	}; // ref class Value
} /* namespace Managed */ } // namespace Shape
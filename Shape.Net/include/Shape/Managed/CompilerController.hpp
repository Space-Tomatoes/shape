#pragma once

#include <msclr/marshal_cppstd.h>

#include <Shape.hpp>
#include <Shape/Managed/IFunctionHandler.hpp>
#include <Shape/Managed/Value.hpp>

using namespace System;
using namespace System::Runtime::InteropServices;

namespace Shape { namespace Managed
{
	public ref class CompilerController :
		public IDisposable
	{
	internal:
		Shape::CompilerController *controller;
		bool owner;

		CompilerController(Shape::CompilerController *controller);

	public:
		enum class StorageType
		{
			AtStorage,
			DollarStorage,
			SharpStorage,
			ConstStorage
		};

	public:
		CompilerController();
		virtual ~CompilerController();	

		bool SetFunctionHandler(String ^functionName, Shape::Managed::IFunctionHandler ^handler);
		//void EmitErrorHandler();
		//Shape::Managed::Value EmitFunctionHandler(String ^functionName);
		Shape::Managed::Value ^ GetStorageRoot(String ^storageName);
		Shape::Managed::Value ^ GetDefaultStorageRoot();
	}; // class CompilerController
} /* namespace Managed */ } // namespace Shape
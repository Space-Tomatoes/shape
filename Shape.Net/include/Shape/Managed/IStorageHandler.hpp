#pragma once

#include <Shape/Managed/CompilerController.hpp>
#include <Shape/Managed/Value.hpp>

using namespace System;

namespace Shape { namespace Managed
{
	public interface class IStorageHandler
	{
	public:
		virtual Value Execute(CompilerController ^controller, String ^key) abstract;
	}; // interface class IStorageHandler
} /* namespace Managed */ } // namespace Shape
#pragma once 

#include <msclr/all.h>

#include <Shape.hpp>
#include <Shape/Managed/IStorageHandler.hpp>
#include <Shape/Managed/CompilerController.hpp>

using namespace System;
using namespace System::Runtime::InteropServices;

namespace Shape { namespace Managed
{
	class StorageHandlerAdapter :
		public Shape::IStorageHandler
	{
	private:
		/// \brief GC's handler to managed implementation of handler.
		msclr::auto_gcroot<Shape::Managed::IStorageHandler ^> handler;

	public:
		StorageHandlerAdapter(Shape::Managed::IStorageHandler ^handler);
		virtual ~StorageHandlerAdapter();

		virtual Shape::Value * operator () (Shape::CompilerController *controller, const std::string &key) override;
	}; // class StorageHandlerAdapter
} /* namespace Managed */ } // namespace Shape
#pragma once

#include <msclr/marshal_cppstd.h>

#include <Shape.hpp>
#include <Shape/Managed/CompilerController.hpp>

using namespace System;
using namespace System::Runtime::InteropServices;

namespace Shape { namespace Managed
{
	public ref class ShapeCompiler :
		public IDisposable
	{
	private:
		Shape::ShapeCompiler *compiler;
		CompilerController ^controller;

	public:
		ShapeCompiler();
		virtual ~ShapeCompiler();
		
	public:			
		String ^ ParseFile(String ^fileName);
		String ^ Parse(String ^templateData);

		property bool TraceScanning
		{
			bool get();
			void set(bool value);
		}
		
		property bool TraceParsing
		{
			bool get();
			void set(bool value);
		}

		property CompilerController ^ Controller
		{
			CompilerController ^ get();		
			void set(CompilerController ^ value);
		}
	}; // class ShapeCompiler
} /* namespace Managed */ } // namespace Shape
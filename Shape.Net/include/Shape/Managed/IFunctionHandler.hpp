#pragma once

using namespace System;

namespace Shape { namespace Managed
{
	ref class CompilerController;

	public interface class IFunctionHandler
	{
	public:
		virtual void Execute(CompilerController ^controller/*, ArgumentList &args*/) abstract;
	}; // interface class IFunctionHandler	
} /* namespace Managed */ } // namespace Shape
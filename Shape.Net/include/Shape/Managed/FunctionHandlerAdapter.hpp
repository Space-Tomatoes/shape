#pragma once

#include <msclr/all.h>

#include <Shape.hpp>
#include <Shape/Managed/IFunctionHandler.hpp>

using namespace System;
using namespace System::Runtime::InteropServices;

namespace Shape { namespace Managed
{
	class FunctionHandlerAdapter :
		public Shape::IFunctionHandler
	{
	private:
		/// \brief GC's handler to managed implementation of handler.
		msclr::auto_gcroot<Shape::Managed::IFunctionHandler ^> handler;

	public:
		FunctionHandlerAdapter(Shape::Managed::IFunctionHandler ^handler);
		virtual ~FunctionHandlerAdapter();

		virtual Shape::Value operator () (Shape::CompilerController *controller, Shape::ArgumentList &args) override;
	}; // class FunctionHandlerAdapter
} /* namespace Managed */ } // namespace Shape
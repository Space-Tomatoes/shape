{
	'variables' : {
		'source_files' : [
			'src/AssemblyInfo.cpp',
			
			# General Shape classes.
			'src/ShapeCompiler.cpp',
			'src/Value.cpp',			
			'src/CompilerController.cpp',
			
			# Handler interfaces.
			'src/IFunctionHandler.cpp',
			'src/IStorageHandler.cpp',
			'src/IErrorHandler.cpp',
			
			# Handler adapters.
			'src/FunctionHandlerAdapter.cpp',
			'src/StorageHandlerAdapter.cpp',
			'src/ErrorHandlerAdapter.cpp',
		],
		'include_files' : [
			# Common Managed Shape header.
			'include/ManagedShape.hpp',
		
			# General Shape classes.
			'include/Shape/Managed/ShapeCompiler.hpp',
			'include/Shape/Managed/Value.hpp',
			'include/Shape/Managed/CompilerController.hpp',
			
			# Handler interfaces.
			'include/Shape/Managed/IFunctionHandler.hpp',
			'include/Shape/Managed/IStorageHandler.hpp',
			'include/Shape/Managed/IErrorHandler.hpp',	

			# Handler adapters.
			'include/Shape/Managed/FunctionHandlerAdapter.hpp',
			'include/Shape/Managed/StorageHandlerAdapter.hpp',
			'include/Shape/Managed/ErrorHandlerAdapter.hpp',		
		],
		'cert_files' : [
			'certificates/Debug.snk',
			'certificates/Release.snk',
		]		
	},
}
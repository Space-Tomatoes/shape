#include <Shape/Managed/ShapeCompiler.hpp>

namespace Shape { namespace Managed
{
		ShapeCompiler::ShapeCompiler() :
			compiler(new Shape::ShapeCompiler()),
			controller(gcnew CompilerController)
		{ 
			this->compiler->SetController(this->Controller->controller);
		}
	
		ShapeCompiler::~ShapeCompiler()
		{ 
			delete this->compiler;
		}
				
		String ^ ShapeCompiler::ParseFile(String ^fileName)
		{
			return msclr::interop::marshal_as<String ^>
			(
				compiler->ParseFile(msclr::interop::marshal_as<std::string>(fileName))
			);
		}

		String ^ ShapeCompiler::Parse(String ^templateData)
		{
			return msclr::interop::marshal_as<String ^>
			(
				compiler->Parse(msclr::interop::marshal_as<std::string>(templateData))
			);		
		}

		bool ShapeCompiler::TraceScanning::get()
		{ 
			return this->compiler->GetTraceScanning(); 
		}

		void ShapeCompiler::TraceScanning::set(bool value)
		{ 
			this->compiler->SetTraceScanning(value); 
		}		

		bool ShapeCompiler::TraceParsing::get()
		{
			return this->compiler->GetTraceParsing(); 
		}

		void ShapeCompiler::TraceParsing::set(bool value)
		{
			this->compiler->SetTraceParsing(value); 
		}
			
		CompilerController ^ ShapeCompiler::Controller::get()
		{ 
			return controller; 
		}
		
		void ShapeCompiler::Controller::set(CompilerController ^value)
		{
			controller = value;
			// invalidate current controller.
			this->compiler->SetController(controller->controller);
		}			
} /* namespace Managed */ } // namespace Shape
#include <Shape/Managed/CompilerController.hpp>
#include <Shape/Managed/FunctionHandlerAdapter.hpp>

namespace Shape { namespace Managed
{
	CompilerController::CompilerController() :
		controller(new Shape::CompilerController),
		owner(true)
	{ }

	// Internal constructor.
	CompilerController::CompilerController(Shape::CompilerController *controller) :
		controller(controller),
		owner(false)
	{ }

	CompilerController::~CompilerController()
	{
		if (owner && this->controller != nullptr)
			delete this->controller;
	}
		
	bool CompilerController::SetFunctionHandler(String ^functionName, Shape::Managed::IFunctionHandler ^handler)
	{ 
		return this->controller->SetFunctionHandler<Shape::Managed::FunctionHandlerAdapter>(msclr::interop::marshal_as<std::string>(functionName), handler);
	}

	/*void CompilerController::EmitErrorHandler()
	{ }*/

	/*void CompilerController::EmitFunctionHandler()
	{ }*/
	
	Shape::Managed::Value ^ CompilerController::GetStorageRoot(String ^storageName)
	{ 
		Shape::Value *ptr = nullptr;
		this->controller->EmitStorageHandler(msclr::interop::marshal_as<std::string>(storageName), "", ptr);
		return gcnew Shape::Managed::Value(ptr);
	}

	Shape::Managed::Value ^ CompilerController::GetDefaultStorageRoot()
	{
		return this->GetStorageRoot("@");
	}
} /* namespace Managed */ } // namespace Shape
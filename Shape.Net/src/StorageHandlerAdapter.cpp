#include <Shape/Managed/StorageHandlerAdapter.hpp>

namespace Shape { namespace Managed
{
	StorageHandlerAdapter::StorageHandlerAdapter(Shape::Managed::IStorageHandler ^handler) :
		handler(handler)
	{ }

	StorageHandlerAdapter::~StorageHandlerAdapter()
	{ }

	Shape::Value * StorageHandlerAdapter::operator () (Shape::CompilerController *controller, const std::string &key)
	{
		//this->handler->Execute(gcnew CompilerController(controller), msclr::interop::marshal_as<System::String>(key);

		return nullptr;
	}
} /* namespace Shape */ } // namespace Managed
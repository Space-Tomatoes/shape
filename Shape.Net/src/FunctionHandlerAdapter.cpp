#include <Shape/Managed/FunctionHandlerAdapter.hpp>
#include <Shape/Managed/CompilerController.hpp>

namespace Shape { namespace Managed
{
	FunctionHandlerAdapter::FunctionHandlerAdapter(Shape::Managed::IFunctionHandler ^handler) :
		handler(handler)
	{ }

	FunctionHandlerAdapter::~FunctionHandlerAdapter()
	{ }

	Shape::Value FunctionHandlerAdapter::operator () (Shape::CompilerController *controller, Shape::ArgumentList &args)
	{
		this->handler->Execute(gcnew CompilerController(controller));

		return Shape::Value();
	}
} /* namespace Managed */ } // namespace Shape
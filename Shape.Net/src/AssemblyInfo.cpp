using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

[assembly:AssemblyTitleAttribute("Shape.Net")];
[assembly:AssemblyDescriptionAttribute("")];
[assembly:AssemblyConfigurationAttribute("")];
[assembly:AssemblyCompanyAttribute("Wincor Nixdorf Russia")];
[assembly:AssemblyProductAttribute("Shape.Net")];
[assembly:AssemblyCopyrightAttribute("Copyright � Wincor Nixdorf Russia, 2014")];
[assembly:AssemblyTrademarkAttribute("")];
[assembly:AssemblyCultureAttribute("")];

[assembly:AssemblyVersionAttribute("1.0.0.4")];
[assembly:AssemblyFileVersionAttribute("1.0.0.4")];

[assembly:ComVisible(false)];

[assembly:CLSCompliantAttribute(true)];

[assembly:SecurityPermission(SecurityAction::RequestMinimum, UnmanagedCode = true)];

[assembly:AssemblyKeyFileAttribute("Shape.Net/certificates/Release.snk")];
//[assembly:AssemblyDelaySignAttribute(true)];
{
	'includes': [
		'../../Shared/Common.gypi',
	],
	'targets': [
		# Shape engine static library
		{			
			'target_name': 'Shape',
			'type': 'static_library',
			'includes': [
				'Shape/sources.gypi',
			],
			'include_dirs': [
				'Shape/include',
				'Shape/include/Shape/ShapeParser', # Due to limitations of the bison.
			],
			'direct_dependent_settings': {
				'include_dirs': [
					'Shape/include',
				],
			},
			'sources': [
				'Shape/sources.gypi',
				'<@(source_files)',
				'<@(include_files)',
			],
			'actions': [
				# GNU flex. Fast Lexical Analyzer generator.
				{
					'action_name': 'flex',
					'variables': {
						'scanner_file': 'Shape/src/ShapeScanner/ShapeScanner.ll',
						'scanner_output_file': 'Shape/src/ShapeScanner/ShapeScanner.cpp',
						'additional_options': '-Cem',					# Default compression.
						#'additional_options': '--full',				# Full compression.
					},
					'inputs': [ '<(scanner_file)', ],
					'outputs': [ '<(scanner_output_file)', ],
					'action': [ 
						'../../Shared/Tools/FlexBison/bin/win_flex', 
						'-o<@(_outputs)', 								# Output file.
						'--wincompat',                                  # Don't use unistd.h
						'<(additional_options)', 						# Additional options. 
						'<@(_inputs)',                                  # Input file.
					],
				},
				# GNU bison. Parser generator.
				{
					'action_name': 'bison',
					'variables': {
						'parser_file': 'Shape/src/ShapeParser/ShapeParser.yy',
						'parser_source_file': 'Shape/src/ShapeParser/ShapeParser.cpp',
						'parser_header_file': 'Shape/include/Shape/ShapeParser/ShapeParser.hpp',
						'skeleton_file': 'Shape/src/ShapeParser/m4/lalr1.m4',
						'additional_options': '--verbose',
						#'additional_options': '',
					},
					'inputs': [ '<(parser_file)', ],
					'outputs': [ '<(parser_source_file)', '<(parser_header_file)', ],
					'action': [
						'../../Shared/Tools/FlexBison/bin/win_bison',
						'--output=<@(parser_source_file)',				# Output file.
						'--defines=<@(parser_header_file)',				# Defines file.
						'--skeleton=<@(skeleton_file)',					# Skeleton file.
						#'<(additional_options)', 						# Additional options.
						'<@(_inputs)',									# Input file.
					],
				},
			],
		},
		# General Shape console interpreter
		{
			'target_name': 'Shape.Interpreter',
			'type': 'executable',
			'dependencies': [
				'Shape',
				'../../Shared/getopt/getopt.gyp:getopt',
				'third_party/JsonBox/JsonBox.gyp:JsonBox',
			],
			'sources': [
				'Shape.Interpreter/src/ShapeInterpreter.cpp',
				'Shape.Interpreter/src/PlaygroundController.hpp',
			],
		},
		# ProTopas Dump utility. todo: move to wnrServiceExtFw.
		{
			'target_name': 'Shape.Dump',
			'type': 'executable',
			'dependencies': [
				'../../Shared/SDKs/ProTopasKit/ProTopasKit.gyp:50.10',
				'../../Shared/WNRusLib/WNRusLib.gyp:ProTopas',
				'../../Shared/getopt/getopt.gyp:getopt',
				'third_party/JsonBox/JsonBox.gyp:JsonBox',
			],
			'sources': [
				'Shape.Dump/src/ShapeDump.cpp',
			],
		},
		# Shape's C++\CLI wrapper for .net platform.
		{
			'target_name': 'Shape.Net',
			'type': 'shared_library',
			'dependencies': [
				'Shape',
			],
			'includes': [
				'Shape.Net/sources.gypi',
			],
			'include_dirs': [
				'Shape.Net/include'
			],
			'sources': [
				'<@(source_files)',
				'<@(include_files)',
			],			
			'msbuild_configuration_attributes': {
				'CharacterSet': 'Unicode',			# C++/CLI use Unicode by default.
				'CLRSupport': 'true',				# Option at general options tab. Doesn't work.
			},
			'msbuild_settings': {
				'ClCompile': {
					'ExceptionHandling': 'false',	# Standart exception handling not supported by CLI.
					'CompileAsManaged': 'true',		# Enables C++/CLI extensions.
				},
			},
		},		
	],
}
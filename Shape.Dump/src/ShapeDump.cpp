/// \file Startup.cpp
/// \author Nikita, Soldatov <nikita.soldatov.ext@wincor-nixdorf.com>

#include <cstdlib>
#include <cstdio>

#include <string>

#include <getopt.h>
#include <JsonBox.h>

#include <CCCinFW.xpp>
#include <CCCdmFW.xpp>
#include <CoinOut.xpp>

#include <WNRusLib/Core.hpp>
#include <WNRusLib/Types.hpp>
#include <WNRusLib/String.hpp>

#define DEF_DUMP_CIN		(true)
#define DEF_DUMP_CDM		(true)
#define DEF_DUMP_COIN		(true)
#define DEF_DATA_FILE		("default.json")

static char szMod[] = "$MOD$ 160414 1000t ShapeDump.exe [Build: " __TIMESTAMP__ "]";

void ParseOptions(int argc, char *argv[]);
void Usage();

struct
{
	bool dumpCin;
	bool dumpCdm;
	bool dumpCoin;
	std::string dataFileName;
} options = { DEF_DUMP_CIN, DEF_DUMP_CDM, DEF_DUMP_COIN, DEF_DATA_FILE };

int main(int argc, char *argv[])
{
	using namespace WNRusLib;
	using namespace WNRusLib::Core;
	
	ErrCode sRc = ~CCFRMW_RC_OK;

	setlocale(LC_ALL, "");

	ParseOptions(argc, argv);

	sRc = ::InitializeFrameWorkEnvironment();
	fprintf(stderr, "InitializeFrameWorkEnvironment() -> %d\n", sRc);
	
	JsonBox::Value data;

	/* CCCashInFW */
	if (options.dumpCin)
	{
		Short sRc = ~CCCINFW_RC_OK;
		CCCashInFW fwCashIn;
		CCCINCASHUNIT cinCashUnit = { 0 };

		UShort usNumberOfNoteTypes = 0u;
		sRc = fwCashIn.ReadBimNoteTypes(&usNumberOfNoteTypes);
		fprintf(stderr, "fwCashIn.ReadBimNoteTypes() -> %d, usNumberOfNoteTypes = %d\n", 
			sRc, usNumberOfNoteTypes);

		if (sRc == CCCINFW_RC_OK)
		{
			// @bim
			data["bim"] = JsonBox::Array(); 
			// @bim.count
			data["bim"]["count"] = (Int32)usNumberOfNoteTypes;
				
			for (UShort usIndex = 0u; usIndex < usNumberOfNoteTypes; ++usIndex)
			{
				CCCINNOTETYPE noteType = { 0 };

				sRc = fwCashIn.GetBimNoteTypeInfo(usIndex, &noteType);
				
				if (sRc == CCCINFW_RC_OK)
				{
					// @bim[<n>]
					data["bim"][(Int32)usIndex] = JsonBox::Array();
					// @bim[<n>].currency
					data["bim"][(Int32)usIndex]["currency"] = noteType.szCurrencyID;
					// @bim[<n>].denomination
					data["bim"][(Int32)usIndex]["denomination"] = (Int32)noteType.ulDenomination;
					// @bim[<n>].release
					data["bim"][(Int32)usIndex]["release"] = (Int32)noteType.usRelease;
					// @bim[<n>].configured
					data["bim"][(Int32)usIndex]["configured"] = (noteType.bConfigured == TRUE);
				}
			}
		}

		/* Main counters. */
		UShort usNumberOfCassettes = 0u, usNumberOfNoteCounters = 0u;
		sRc = fwCashIn.ReadCashUnitInfo(&usNumberOfCassettes, &usNumberOfNoteCounters);
		fprintf(stderr, "fwCashIn.ReadCashUnitInfo() -> %d, usNumberOfCassettes = %d, usNumberOfNoteCounters = %d\n", 
			sRc, usNumberOfCassettes, usNumberOfNoteCounters);

		if (sRc == CCCINFW_RC_OK)
		{
			// @cin
			data["cin"] = JsonBox::Array();
			
			// @cin.count
			data["cin"]["count"] = (Int32)usNumberOfCassettes;				

			for (UShort usCashUnitIndex = 0u; usCashUnitIndex < usNumberOfCassettes; ++usCashUnitIndex)
			{
				sRc = fwCashIn.GetCashUnitInfo(usCashUnitIndex, &cinCashUnit);

				if (sRc == CCCINFW_RC_OK)
				{
					fprintf(stderr, "usNumber = %d, usType = %d, szCurrencyID = `%s`, ulDenomination = %d, ulInitialCount = %d, ulCount = %d\n", 
						cinCashUnit.usNumber, cinCashUnit.usType, cinCashUnit.szCurrencyID, cinCashUnit.ulDenomination, cinCashUnit.ulInitialCount, cinCashUnit.ulCount);

					// @cin[<n>]
					data["cin"][(Int32)usCashUnitIndex] = JsonBox::Array();
					// @cin[<n>].number
					data["cin"][(Int32)usCashUnitIndex]["number"] = (Int32)cinCashUnit.usNumber;
					// @cin[<n>].type
					data["cin"][(Int32)usCashUnitIndex]["type"] = (Int32)cinCashUnit.usType;
					// @cin[<n>].currency
					data["cin"][(Int32)usCashUnitIndex]["currency"] = cinCashUnit.szCurrencyID;
					// @cin[<n>].denomination
					data["cin"][(Int32)usCashUnitIndex]["denomination"] = (Int32)cinCashUnit.ulDenomination;
					// @cin[<n>].initial
					data["cin"][(Int32)usCashUnitIndex]["initial"] = (Int32)cinCashUnit.ulInitialCount;
					// @cin[<n>].count
					data["cin"][(Int32)usCashUnitIndex]["count"] = (Int32)cinCashUnit.ulCount;
					// @cin[<n>].status
					data["cin"][(Int32)usCashUnitIndex]["status"] = (Int32)cinCashUnit.usStatus;

					// @cin[<n>].notes[<m>]
					data["cin"][(Int32)usCashUnitIndex]["notes"] = JsonBox::Array();
					// @cin[<n>].notes.count
					data["cin"][(Int32)usCashUnitIndex]["notes"]["count"] = (Int32)cinCashUnit.usNumberOfNoteTypes;

					for (UShort usNoteTypeIndex = 0u; usNoteTypeIndex < cinCashUnit.usNumberOfNoteTypes; ++usNoteTypeIndex)
					{
						CCCINNOTETYPE noteType = { 0 };

						sRc = fwCashIn.GetCashUnitNoteTypeInfo(usCashUnitIndex, usNoteTypeIndex, &noteType);
						fprintf(stderr, "fwCashIn.GetCashUnitNoteTypeInfo(%d, %d) -> %d\n", 
							usCashUnitIndex, usNoteTypeIndex, sRc);

						if (sRc == CCCINFW_RC_OK)
						{
							fprintf(stderr, "szCurrencyID = `%s`, ulDenomination = %d, ulCount = %d\n", 
								noteType.szCurrencyID, noteType.ulDenomination, noteType.ulCount);

							// @cin[<n>].notes[<m>]
							data["cin"][(Int32)usCashUnitIndex]["notes"][(Int32)usNoteTypeIndex] = JsonBox::Array();
							// @cin[<n>].notes[<m>].currency
							data["cin"][(Int32)usCashUnitIndex]["notes"][(Int32)usNoteTypeIndex]["currency"] = noteType.szCurrencyID;
							// @cin[<n>].notes[<m>].denomination
							data["cin"][(Int32)usCashUnitIndex]["notes"][(Int32)usNoteTypeIndex]["denomination"] = (Int32)noteType.ulDenomination;
							// @cin[<n>].notes[<m>].count
							data["cin"][(Int32)usCashUnitIndex]["notes"][(Int32)usNoteTypeIndex]["count"] = (Int32)noteType.ulCount;
						}
					}
				}
			}

			/* Reject counters. */
			UShort usNumberOfRejectCassettes = 0u;

			sRc = fwCashIn.ReadCashUnitRejectInfo(&usNumberOfRejectCassettes);
			fprintf(stderr, "fwCashIn.ReadCashUnitRejectInfo() -> %d, usNumberOfRejectCassettes = %d\n",
				sRc, usNumberOfRejectCassettes);		

			if (sRc == CCCINFW_RC_OK)
			{
				data["cin"]["count"] = data["cin"]["count"].getInt() + (Int32)usNumberOfRejectCassettes;

				for (UShort usRejectCassIndex = 0u; usRejectCassIndex < usNumberOfRejectCassettes; ++usRejectCassIndex)
				{
					CCCINCASHUNIT rejectCashUnit = { 0 };

					sRc = fwCashIn.GetCashUnitRejectInfo(usRejectCassIndex, &rejectCashUnit);
					fprintf(stderr, "fwCashIn.GetCashUnitRejectInfo(%d) -> %d\n", 
						usRejectCassIndex, sRc);
					
					if (sRc == CCCINFW_RC_OK)
					{
						fprintf(stderr, "usNumber = %d, usType = %d, szCurrencyID = `%s`, ulDenomination = %d, ulInitialCount = %d, ulCount = %d\n", 
								rejectCashUnit.usNumber, rejectCashUnit.usType, rejectCashUnit.szCurrencyID, rejectCashUnit.ulDenomination, rejectCashUnit.ulInitialCount, rejectCashUnit.ulCount);

						// @cin[<n+r>]
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)] = JsonBox::Array();
						// @cin[<n+r>].number
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["number"] = (Int32)rejectCashUnit.usNumber;
						// @cin[<n+r>].type
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["type"] = (Int32)rejectCashUnit.usType;
						// @cin[<n+r>].currency
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["currency"] = rejectCashUnit.szCurrencyID;
						// @cin[<n+r>].denomination
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["denomination"] = (Int32)rejectCashUnit.ulDenomination;
						// @cin[<n+r>].initial
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["initial"] = (Int32)rejectCashUnit.ulInitialCount;
						// @cin[<n+r>].count
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["count"] = (Int32)rejectCashUnit.ulCount;
						// @cin[<n+r>].status
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["status"] = (Int32)rejectCashUnit.usStatus;

						// @cin[<n+r>].notes[<m>]
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["notes"] = JsonBox::Array();
						// @cin[<n+r>].notes.count
						data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["notes"]["count"] = (Int32)rejectCashUnit.usNumberOfNoteTypes;

						for (UShort usRejectNoteTypeIndex = 0u; usRejectNoteTypeIndex < rejectCashUnit.usNumberOfNoteTypes; ++usRejectNoteTypeIndex)
						{
							CCCINNOTETYPE rejectNoteType = { 0 };

							sRc = fwCashIn.GetCashUnitRejectNoteTypeInfo(usRejectCassIndex, usRejectNoteTypeIndex, &rejectNoteType);
							fprintf(stderr, "fwCashIn.GetCashUnitRejectNoteTypeInfo(%d, %d) -> %d\n", 
								usRejectCassIndex, usRejectNoteTypeIndex, sRc);

							if (sRc == CCCINFW_RC_OK)
							{
								fprintf(stderr, "szCurrencyID = `%s`, ulDenomination = %d, ulCount = %d\n", 
									rejectNoteType.szCurrencyID, rejectNoteType.ulDenomination, rejectNoteType.ulCount);
					
								// @cin[<n+r>].notes[<m>]
								data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["notes"][(Int32)usRejectNoteTypeIndex] = JsonBox::Array();
								// @cin[<n+r>].notes[<m>].currency
								data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["notes"][(Int32)usRejectNoteTypeIndex]["currency"] = rejectNoteType.szCurrencyID;
								// @cin[<n+r>].notes[<m>].denomination
								data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["notes"][(Int32)usRejectNoteTypeIndex]["denomination"] = (Int32)rejectNoteType.ulDenomination;
								// @cin[<n+r>].notes[<m>].count
								data["cin"][(Int32)(usNumberOfCassettes + usRejectCassIndex)]["notes"][(Int32)usRejectNoteTypeIndex]["count"] = (Int32)rejectNoteType.ulCount;
							}
						}
					}
				}
			}
		} 
	}

	/* CCCdmFW */
	if (options.dumpCdm)
	{
		Short sRc = ~CCCDMFW_RC_OK;
		CCCdmFW fwCdm;
		CCCDMCASHUNIT_EX cdmCashUnit = { 0 };

		/* Main counters. */
		UShort usNumCashUnits = 0u;
		sRc = fwCdm.ReadCashUnitInfoEx(&usNumCashUnits);
		fprintf(stderr, "fwCdm.ReadCashUnitInfoEx() -> %d, usNumCashUnits = %d\n", sRc, usNumCashUnits);

		if (sRc == CCCDMFW_RC_OK)
		{
			// @cdm
			data["cdm"] = JsonBox::Array();

			// @cdm.count
			data["cdm"]["count"] = (Int32)usNumCashUnits;

			for (UShort usCashUnitIndex = 0u; usCashUnitIndex < usNumCashUnits; ++usCashUnitIndex)
			{
				sRc = fwCdm.GetCashUnitInfoEx(usCashUnitIndex, &cdmCashUnit);

				if (sRc == CCCDMFW_RC_OK)
				{
					fprintf(stderr, "cdmCashUnit.usType = %d, cdmCashUnit.szCurrencyID = `%s`, cdmCashUnit.ulValues = %d, cdmCashUnit.ulInitialCount = %d, cdmCashUnit.ulCount = %d, cdmCashUnit.ulDispensedCount = %d, cdmCashUnit.ulPresentedCount = %d, cdmCashUnit.ulCashInCount = %d\n", 
						cdmCashUnit.usType, cdmCashUnit.szCurrencyID, cdmCashUnit.ulValues, cdmCashUnit.ulInitialCount, cdmCashUnit.ulCount, cdmCashUnit.ulDispensedCount, cdmCashUnit.ulPresentedCount, cdmCashUnit.ulCashInCount);

					// @cdm[<n>]
					data["cdm"][(Int32)usCashUnitIndex] = JsonBox::Array();
					// @cdm[<n>].number
					data["cdm"][(Int32)usCashUnitIndex]["number"] = (Int32)cdmCashUnit.usNumber;
					// @cdm[<n>].type
					data["cdm"][(Int32)usCashUnitIndex]["type"] = (Int32)cdmCashUnit.usType;
					// @cdm[<n>].currency
					data["cdm"][(Int32)usCashUnitIndex]["currency"] = cdmCashUnit.szCurrencyID;
					// @cdm[<n>].denomiation
					data["cdm"][(Int32)usCashUnitIndex]["denomination"] = (Int32)cdmCashUnit.ulValues;
					// @cdm[<n>].initial
					data["cdm"][(Int32)usCashUnitIndex]["initial"] = (Int32)cdmCashUnit.ulInitialCount;
					// @cdm[<n>].count
					data["cdm"][(Int32)usCashUnitIndex]["count"] = (Int32)cdmCashUnit.ulCount;
					// @cdm[<n>].dispensed
					data["cdm"][(Int32)usCashUnitIndex]["dispensed"] = (Int32)cdmCashUnit.ulDispensedCount;
					// @cdm[<n>].presented
					data["cdm"][(Int32)usCashUnitIndex]["presented"] = (Int32)cdmCashUnit.ulPresentedCount;
					// @cdm[<n>].retracted
					data["cdm"][(Int32)usCashUnitIndex]["retracted"] = (Int32)cdmCashUnit.ulRetractedCount;
					// @cdm[<n>].rejected
					data["cdm"][(Int32)usCashUnitIndex]["rejected"] = (Int32)cdmCashUnit.ulRejectCount;
					// @cdm[<n>].cashin
					data["cdm"][(Int32)usCashUnitIndex]["cashin"] = (Int32)cdmCashUnit.ulCashInCount;
					// @cdm[<n>].status
					data["cdm"][(Int32)usCashUnitIndex]["status"] = (Int32)cdmCashUnit.usStatus;
				}
			}
		}
	}	

	/* CCCoinOutFW */
	if (options.dumpCoin)
	{
		Short sRc = ~COINOUT_RC_OK;
		CCCoinOutFW fwCoin;
		COINOUTCASHUNIT coinCashUnit = { 0 };

		UShort usNumberOfCashUnits = 0u;
		sRc = fwCoin.ReadCashUnitInfo(&usNumberOfCashUnits);
		fprintf(stderr, "fwCoin.ReadCashUnitInfo() -> %d, usNumberOfCashUnits = %d\n", sRc, usNumberOfCashUnits);

		if (sRc = COINOUT_RC_OK)
		{
			// @coin
			data["coin"] = JsonBox::Array();
				
			// @coin.count
			data["coin"] = (Int32)usNumberOfCashUnits;

			for (UShort usIndex = 0u; usIndex < usNumberOfCashUnits; ++usIndex)
			{
				sRc = fwCoin.GetCashUnitInfo(usIndex, &coinCashUnit);

				if (sRc == COINOUT_RC_OK)
				{
					// @coin[<n>]
					data["coin"][(Int32)usIndex] = JsonBox::Array();
					// @coin[<n>].number
					data["coin"][(Int32)usIndex]["number"] = (Int32)coinCashUnit.usNumber;
					// @coin[<n>].type
					data["coin"][(Int32)usIndex]["type"] = (Int32)coinCashUnit.usType;
					// @coin[<n>].currency
					data["coin"][(Int32)usIndex]["currency"] = coinCashUnit.szCurrencyID;
					// @coin[<n>].denomiation
					data["coin"][(Int32)usIndex]["denomiation"] = (Int32)coinCashUnit.ulValues;
					// @coin[<n>].initial
					data["coin"][(Int32)usIndex]["initial"] = (Int32)coinCashUnit.ulInitialCount;
					// @coin[<n>].count
					data["coin"][(Int32)usIndex]["count"] = (Int32)coinCashUnit.ulCount;
					// @coin[<n>].status
					data["coin"][(Int32)usIndex]["status"] = (Int32)coinCashUnit.usStatus;
				}
			}
		}
	}

	sRc = ::DeleteFrameWorkEnvironment();
	fprintf(stderr, "DeleteFrameWorkEnvironment() -> %d\n", sRc);

	data.writeToFile(options.dataFileName, true, false);

	return EXIT_SUCCESS;
}

void ParseOptions(int argc, char *argv[])
{
	struct option params[] =
	{
		{ "help",	no_argument,		NULL, 'h' },
		{ "data",	required_argument,	NULL, 'd' },
		{ "cdm",	required_argument,	NULL, 'm' },
		{ "cin",	required_argument,	NULL, 'n' },
		{ "coin",	required_argument,	NULL, 'i' },
		{ NULL, NULL, NULL, NULL }
	};

char cmd = 0;
	int optionIndex = 0;
	extern char *optarg;

	while (cmd != -1)
	{
		switch (cmd = getopt_long(argc, argv, "hd:m:n:i:", params, &optionIndex))
		{
		case 'm': // --cdm			
			options.dumpCdm = (strcmp(optarg, "true") == 0);
		break;
		case 'n': // --cin
			options.dumpCin = (strcmp(optarg, "true") == 0);
		break;
		case 'i': // --coin
			options.dumpCoin = (strcmp(optarg, "true") == 0);
		break;
		case 'd': // --data
			options.dataFileName = optarg;
		break;
		case 'h': // --help
			Usage();
			exit(EXIT_FAILURE);
		break;
		}
	}
}

void Usage()
{
	printf
	(
		"Usage: ShapeDump[.exe] [-h] [-d <file>] [-m] [-n] [-i]\n"
		"\n"
		"-h, --help\t - Displays this help.\n"
		"-m, --cdm\t - Enable dump of CCCdmFW data.\n"
		"-n, --cin\t - Enable dump of CCCashInFW data.\n"
		"-i, --coin\t - Enable dump of CCCoinFW data.\n"
		"\n"
	);
}